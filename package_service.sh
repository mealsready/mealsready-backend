#!/bin/bash

[[ -z $1 ]] && {
	echo "ERROR Argument missing, usage : COMMAND <serviceName>"
	exit 1
} || {
	mvn -pl fr.cnam.mealsready.services:$1 -am clean package
}
