#!/usr/bin/perl
use strict;

# >>> Add service to docker-compose local
inject({
	'file' => 'docker-compose_local.yml',
	'regex' => '(.+?services\:)(.+$)',
	'content' => "\n\n  " . $ARGV[0] . ":\n    build:\n      context: \"./DB/" . $ARGV[0] . "\"\n      " .
	"dockerfile: \"Dockerfile\"\n" . "    volumes:\n    - /tmp/" . $ARGV[0] . ":/var/lib/postgresql/data\n    " .
	"ports:\n    - \"" . $ARGV[1] .	":5432\"\n    networks:\n    - mealsready-local\n"
});

# >>> Add service to docker-compose
inject({
	'file' => 'docker-compose.yml',
	'regex' => '(.+?services\:)(.+$)',
	'content' => "\n\n  " . $ARGV[0] . ":\n    image: registry.gitlab.com/mealsready/mealsready-backend/${MR_ENV:-rc}/" . lc($ARGV[0]) . ":latest\n    " .
    "container_name: " . $ARGV[0] . "\n    restart: unless-stopped\n    " .
    "volumes:\n    - /tmp/" . $ARGV[0] . ":/var/lib/postgresql/data\n    " .
	"ports:\n    - \"" . $ARGV[1] .	":5432\"\n    networks:\n    - mealsready\n"
});

# >>> Add gitlab ci downstream for the service
inject({
	'file' => '.gitlab-ci.yml',
	'regex' => '(.*?)(pipeline_.+?:.*)$',
	'content' => "pipeline_" . $ARGV[0] . ":\n  stage: pipelines\n  trigger:\n    include: DB/" .
	$ARGV[0] . "/.gitlab-ci.yml\n    strategy: depend\n  rules:\n    - changes:\n      - DB/" . $ARGV[0] .
	"/**/*\n\n"
});

# >>> Update maven module path
inject({
	'file' => 'pom.xml',
	'regex' => '(.*?(?:\n|\r\n)\s*?<module>)(' . $ARGV[0] . '</module>.*$)',
	'content' => 'DB/'
});

sub inject {
	my $params = $_[0];
	my $fileContent = '';
	open(my $fd, '<', $params->{'file'}) or return displayError($params->{'file'});
	while (my $row = <$fd>) {
	  $fileContent .= $row;
	}
	close($fd);

	$fileContent =~ qr/$params->{"regex"}/s or return displayError($params->{'file'});
	my $newFileContent = $1 . $params->{'content'} . $2;

	open($fd, '>', $params->{'file'}) or return displayError($params->{'file'});
	print $fd $newFileContent;
	close($fd);
	return 0;
}

sub displayError {
	print "WARNING : the file " . $_[0] . " could not be updated\n";
}
