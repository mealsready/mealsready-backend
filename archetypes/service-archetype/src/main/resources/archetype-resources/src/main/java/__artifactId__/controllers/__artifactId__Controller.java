package ${mainPackage}.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Api(tags = { "/${artifactId}" }, description = "Description of my service")
@CrossOrigin
@RestController
@RequestMapping("/${artifactId}")
public class ${artifactId}Controller {
    @ApiOperation(value = "Description of the endpoint")
    @ApiResponses(value = { //
	    @ApiResponse(code = 404, message = "The given ID does not exist"), //
	    @ApiResponse(code = 501, message = "Service not implemented") //
    })
    @GetMapping(value = "/{id}")
    public ResponseEntity<String> getRecipeById(
	    @ApiParam(name = "id", type = "Integer", value = "Id", example = "640141", required = true) @PathVariable("id") Integer id) {
	// TODO your coding adventure starts here...
	return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}

