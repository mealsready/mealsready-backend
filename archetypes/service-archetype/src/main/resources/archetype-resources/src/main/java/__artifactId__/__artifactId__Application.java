package ${mainPackage};

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@EnableDiscoveryClient
@SpringBootApplication(exclude = ErrorMvcAutoConfiguration.class)
@ComponentScan({"${mainPackage}", "fr.cnam.mealsready.core"})
@EntityScan({"${mainPackage}", "fr.cnam.mealsready.core.domain"})
public class ${artifactId}Application {

    public static void main(String[] args) {
    	SpringApplication.run(${artifactId}Application.class, args);
    }

}
