INSERT INTO ingredient_categories (name) VALUES
  ('Autre'),
  ('Viande'),
  ('Légumes'),
  ('Fruits'),
  ('Laitage-Oeufs'),
  ('Epicerie Sucrée'),
  ('Epicerie Salée'),
  ('Boissons');
  
INSERT INTO ingredients (name, imageURL, unit, id_category) VALUES
  ('Pomme de terre', 'www.pomme_de_terre.com/pomme_de_terre.jpg', 6, 3),
  ('Lardons', 'www.lardon.com/lardon.jpg', 0, 2),
  ('Oignon rouge', 'www.oignon_rouge.com/oignon_rouge.jpg', 6, 3),
  ('Reblochon', 'www.roblochon.com/roblochon.jpg', 6, 5);

INSERT INTO recipe_types (name) VALUES
  ('Autre'),
  ('Entrée'),
  ('Plat'),
  ('Dessert');

INSERT INTO qualities (name) VALUES
  ('Autre'),
  ('Gourmant'),
  ('Equilibré');

INSERT INTO diets (name) VALUES
  ('Autre'),
  ('Végatarien'),
  ('Végan'),
  ('Sans Gluten'),
  ('Sans Sel'),
  ('Sans Sucre'),
  ('Sportif');
  
INSERT INTO recipes (name, imageURL, servings, duration, description, id_recipe_type, id_quality, id_diet) VALUES
  ('Tartiflette','image-tartiflette', 4, '30 min', 'La meilleure tartiflette', 3, 2, 1),
  ('Salade pomme-de-terre et oignons','image-salade-pomme-de-terre', 2, '5 min', 'La meilleure salade de pomme-de-terre', 2, 3, 2),
  ('Fondue de pomme-de-terre','image-fondue-pomme-de-terre', 4, '30 min', 'La meilleure fondue de pomme-de-terre', 3, 2, 1);

INSERT INTO ingredients_quantities (id_recipe, id_ingredient, quantity) VALUES
  (1, 1, 4),
  (1, 2, 200),
  (1, 3, 2),
  (1, 4, 2),
  (2, 1, 2),
  (2, 3, 1),
  (3, 1, 4),
  (3, 4, 1);

INSERT INTO instructions (id_recipe, number, text) VALUES
  (1, '1', 'Eplucher et couper en tranches les pommes de terre'),
  (1, '2', 'Eplucher et couper en rondelles les oignons'),
  (1, '3', 'Dans un plan, disposer les pommes de terre, les oignons et les lardons par couches'),
  (1, '4', 'Couper les roblochons en 2 disques de même hauteur et les poser sur les dessus (croute vers le haut)'),
  (1, '5', 'Laisser cuire 25 min'),
  (2, '1', 'Eplucher et couper en tranches les pommes de terre'),
  (2, '2', 'Eplucher et couper en rondelles les oignons'),
  (2, '3', 'Mélanger les pommes de terre et les oignons dans un saladier'),
  (2, '4', 'Assaisonner avec la vinaigrette de votre choix'),
  (3, '1', 'Eplucher les pomme-de-terre et les faire bouillir 20 min'),
  (3, '2', 'Creuser les pomme-de-terre'),
  (3, '3', 'Couper le roblochon en 4 quarts et placer 1 quart dans le creux de chaque pomme-de-terre'),
  (3, '4', 'Mettre au four 3 min à 200°C pour faire fondre le fromage');