CREATE TABLE ingredient_categories (
  id    serial       PRIMARY KEY,
  name  varchar(50)  NOT NULL,
  UNIQUE (name)
);

CREATE TABLE ingredients (
  id    serial       PRIMARY KEY,
  name  varchar(50)  NOT NULL,
  UNIQUE (name),
  imageURL varchar(500),
  unit int NOT NULL,
  id_category int NOT NULL
);

CREATE TABLE recipe_types (
  id    serial       PRIMARY KEY,
  name  varchar(50)  NOT NULL,
  UNIQUE (name)
);

CREATE TABLE qualities (
  id    serial       PRIMARY KEY,
  name  varchar(50)  NOT NULL,
  UNIQUE (name)
);

CREATE TABLE diets (
  id    serial       PRIMARY KEY,
  name  varchar(50)  NOT NULL,
  UNIQUE (name)
);

CREATE TABLE recipes (
  id serial PRIMARY KEY,
  name varchar(50) NOT NULL,
  imageURL varchar(500),
  servings int,
  duration varchar(50),
  description varchar(500),
  id_recipe_type int NOT NULL,
  id_quality int,
  id_diet int
);

CREATE TABLE ingredients_quantities (
  id serial PRIMARY KEY,
  id_recipe int NOT NULL,
  id_ingredient int NOT NULL,
  quantity int
);

CREATE TABLE instructions (
  id serial PRIMARY KEY,
  number varchar(50) NOT NULL,
  text varchar(500) NOT NULL,
  id_recipe int NOT NULL
);
  
ALTER TABLE ingredients
  ADD CONSTRAINT FK_id_category
  FOREIGN KEY (id_category)
  REFERENCES ingredient_categories(id);

ALTER TABLE ingredients_quantities
  ADD CONSTRAINT FK_id_recipe_on_ingredients_list
  FOREIGN KEY (id_recipe)
  REFERENCES recipes(id);

ALTER TABLE ingredients_quantities
  ADD CONSTRAINT FK_id_ingredient
  FOREIGN KEY (id_ingredient)
  REFERENCES ingredients(id);
  
ALTER TABLE instructions
  ADD CONSTRAINT FK_id_recipe_on_instruction
  FOREIGN KEY (id_recipe)
  REFERENCES recipes(id);

ALTER TABLE recipes
  ADD CONSTRAINT FK_id_recipe_type
  FOREIGN KEY (id_recipe_type)
  REFERENCES recipe_types(id);
  
ALTER TABLE recipes
  ADD CONSTRAINT FK_id_quality
  FOREIGN KEY (id_quality)
  REFERENCES qualities(id);
  
ALTER TABLE recipes
  ADD CONSTRAINT FK_id_diet
  FOREIGN KEY (id_diet)
  REFERENCES diets(id);
