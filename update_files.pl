#!/usr/bin/perl
use strict;

# >>> Add gitlab ci downstream for the service
inject({
	'file' => '.gitlab-ci.yml',
	'regex' => '(.*?)(pipeline_.+?:.*$)',
	'content' => "pipeline_" . $ARGV[0] . ":\n  stage: pipelines\n  trigger:\n    include: " .
	$ARGV[0] . "/.gitlab-ci.yml\n    strategy: depend\n  rules:\n    - changes:\n      - " . $ARGV[0] .
	"/**/*\n\n"
});

# >>> Add service to docker-compose_local
inject({
	'file' => 'docker-compose_local.yml',
	'regex' => '(.+?services\:)(.+$)',
	'content' => "\n\n  " . $ARGV[0] . ":\n    build:\n      context: \"./" . $ARGV[0] . "\"\n      " .
	"dockerfile: \"DockerFile_dev_instance_builder\"\n      args:\n      - EUREKA_LOCAL_IP=127.0.0.1\n" .
	"      - GATEWAY_LOCAL_IP=127.0.0.1\n    depends_on:\n    - service-discovery\n    ports:\n    - \"" . $ARGV[1] .
	":8080\"\n    networks:\n    - mealsready-local\n"
});

# >>> Add service to docker-compose
inject({
	'file' => 'docker-compose.yml',
	'regex' => '(.+?services\:)(.+$)',
	'content' => "\n\n  " . $ARGV[0] . ":\n    " .
	'image: registry.gitlab.com/mealsready/mealsready-backend/${MR_ENV:-rc}/' . lc($ARGV[0]) . ":latest\n    " .
    "container_name: " . $ARGV[0] . "\n    " .
    "restart: unless-stopped\n    " .
    "environment:\n    " .
    "- EUREKA_IP\n    " .
    "- GATEWAY_IP\n    " .
    "depends_on:\n    " .
    "- gateway\n    " .
    "ports:\n    " .
    "- \"8080\"\n    " .
    "networks:\n    " .
    "- mealsready\n"
});

sub inject {
	my $params = $_[0];
	my $fileContent = '';
	open(my $fd, '<', $params->{'file'}) or return displayError($params->{'file'}, "Could not open the file. ERRNO = " . $!);
	while (my $row = <$fd>) {
	  $fileContent .= $row;
	}
	close($fd);

	$fileContent =~ qr/$params->{"regex"}/s or return displayError($params->{'file'}, "Could not apply regex");
	my $newFileContent = $1 . $params->{'content'} . $2;

	open($fd, '>', $params->{'file'}) or return displayError($params->{'file'}, "Could not write file. ERRNO = " . $!);
	print $fd $newFileContent;
	close($fd);
	return 0;
}

sub displayError {
	print "WARNING : the file " . $_[0] . " could not be updated\ncause = " . $_[1] . "\n\n";
}
