#!/bin/bash

docker-compose --version && {
	ifconfig && {
		EUREKA_SVR=$(ifconfig |grep -E 'inet\s+?192\.168\.[0-9]{1,3}\.[0-9]{1,3}' |awk '{print $2}')
	} || {
		EUREKA_SVR=$(ipconfig |grep -E "IPv4.+?192\.168\.[0-9]{1,3}\.[0-9]{1,3}" |awk '{print $NF}')
	}

	[[ -z $EUREKA_SVR ]] && {
		read -p "Local IP could not be guessed. Please enter the local IP : " EUREKA_SVR
	}
	docker-compose -f docker-compose_local.yml down && \
	docker-compose -f docker-compose_local.yml build --build-arg EUREKA_LOCAL_IP=$EUREKA_SVR --build-arg GATEWAY_LOCAL_IP=$EUREKA_SVR --build-arg BDD_IP=$EUREKA_SVR && \
	docker-compose -f docker-compose_local.yml up
	echo 'Clearing...'
	docker image prune -f
} || {
	echo "Error : docker-compose could not be found or something went wrong while trying to build images. Did you forget to run mvn clean package ?"
	exit 1
}
