package fr.cnam.mealsready.serviceRegister.services.exceptions;

public class InstanceNotFoundException extends ServiceException {
    private static final long serialVersionUID = 1L;
}
