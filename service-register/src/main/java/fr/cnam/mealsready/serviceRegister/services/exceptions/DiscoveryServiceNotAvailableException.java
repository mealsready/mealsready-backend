package fr.cnam.mealsready.serviceRegister.services.exceptions;

public class DiscoveryServiceNotAvailableException extends ServiceException {
    private static final long serialVersionUID = 1L;
}
