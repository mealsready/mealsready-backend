package fr.cnam.mealsready.serviceRegister;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@EnableDiscoveryClient
@SpringBootApplication(exclude = ErrorMvcAutoConfiguration.class)
@ComponentScan({"fr.cnam.mealsready.serviceRegister", "fr.cnam.mealsready.core"})
public class serviceRegisterApplication {

    public static void main(String[] args) {
    	SpringApplication.run(serviceRegisterApplication.class, args);
    }

}
