package fr.cnam.mealsready.serviceRegister.services.exceptions;

public class ServiceNotFoundException extends ServiceException {
    private static final long serialVersionUID = 1L;
}
