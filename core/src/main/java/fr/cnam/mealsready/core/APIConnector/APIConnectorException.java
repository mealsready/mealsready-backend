package fr.cnam.mealsready.core.APIConnector;

import org.springframework.http.HttpStatus;

public class APIConnectorException extends Exception {

    private static final long serialVersionUID = 1L;
    private HttpStatus statusCode;

    public APIConnectorException(HttpStatus statusCode) {
	super("API respond with an error HTTP status code. HTTP status = " + statusCode.value());
	this.statusCode = statusCode;
    }

    public APIConnectorException(String cause) {
	super(cause);
	this.statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    public HttpStatus getStatusCode() {
	return statusCode;
    }
}
