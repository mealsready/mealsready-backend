package fr.cnam.mealsready.core.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.google.gson.annotations.SerializedName;

public enum Unit {
    @SerializedName("0") //
    g("g"), //
    @SerializedName("1") //
    mg("mg"), //
    @SerializedName("2") //
    kg("kg"), //
    @SerializedName("3") //
    L("L"), //
    @SerializedName("4") //
    mL("mL"), //
    @SerializedName("5") //
    cL("cL"), //
    @SerializedName("6") //
    dL("dL"), //
    @SerializedName("7") //
    piece(""), //
    @SerializedName("8") //
    teaSpoon("CaC"), //
    @SerializedName("9") //
    tableSpoon("CaS"), //
    @SerializedName("10") //
    pinch("pincée");//

    private String text;

    Unit(String text) {
	this.text = text;
    }

    public String getText() {
	return text;
    }

    private static Unit unit[] = { g, mg, kg, L, mL, cL, dL, piece, teaSpoon, tableSpoon, pinch };

    @JsonCreator
    public static Unit forValue(int value) {
	return unit[value];
    }

    @JsonValue
    public Integer toValue() {
	for (int i = 0; i < unit.length; ++i) {
	    if (unit[i] == this) {
		return i;
	    }
	}
	return null;
    }
}
