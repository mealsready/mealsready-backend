package fr.cnam.mealsready.core;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import fr.cnam.mealsready.core.services.JWTService;

@Component
@Order(1)
public class CheckJWTFilter implements Filter {
    @Autowired
    JWTService jwtService;
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
	HttpServletRequest req = (HttpServletRequest) request;
	HttpServletResponse resp = (HttpServletResponse) response;
	
	String token = req.getHeader("Authorization_internal");
	
	if (token == null) {
	    resp.setStatus(HttpStatus.UNAUTHORIZED.value());
	} else {
	    if (jwtService.checkJWT(token)) {		
		chain.doFilter(request, response);
	    } else {		
		resp.setStatus(HttpStatus.FORBIDDEN.value());
	    }
	}
    }
}
