package fr.cnam.mealsready.core.domain;

public class Ingredient {

    private Long id;
    private String name;
    private String imageURL;
    private Unit unit;
    private double quantity;

    public double getQuantity() {
	return quantity;
    }

    public void setQuantity(double quantity) {
	this.quantity = quantity;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getImageURL() {
	return imageURL;
    }

    public void setImageURL(String imageURL) {
	this.imageURL = imageURL;
    }

    public Unit getUnit() {
	return unit;
    }

    public void setUnit(Unit unit) {
	this.unit = unit;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }
    
    @Override
    public boolean equals(Object other) {
	if (!other.getClass().getName().equals(this.getClass().getName())) {
	    return false;
	}
	return this.name.equals(((Ingredient) other).getName());
    }
}
