package fr.cnam.mealsready.core.services;

import org.springframework.http.HttpStatus;

public class ServiceCallerException extends Exception {
    private static final long serialVersionUID = 1L;
    private HttpStatus statusCode;

    public ServiceCallerException() {
	this(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    public ServiceCallerException(HttpStatus status) {
	super("API respond with an error HTTP status code. HTTP status = " + status.value());
	this.statusCode = status;
    }

    public ServiceCallerException(String cause) {
	super(cause);
	this.statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    public HttpStatus getStatusCode() {
	return statusCode;
    }
}
