package fr.cnam.mealsready.core.services;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import fr.cnam.mealsready.core.APIConnector.SilentRestTemplateResponseErrorHandler;
import fr.cnam.mealsready.core.config.CoreConfigFactory;
import fr.cnam.mealsready.core.config.GatewayConfiguration;
import fr.cnam.mealsready.core.domain.ServicesEnum;

/**
 * Inter service communication
 * 
 * @author Christopher MILAZZO
 * @since V2.0
 *
 * @param <T> the type of the DTO
 */
@Component
public class ServiceCaller<T> {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String DEFAULT_PORT = "8080";
    
    private GatewayConfiguration gatewayConfiguration;
    private RestTemplate restTemplate;
    private String APIBaseUrl;

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;
    
    @Autowired
    private CoreConfigFactory coreConfigFactory;

    @Autowired
    Gson gson;
    
    @PostConstruct
    private void init() {
	this.gatewayConfiguration = this.coreConfigFactory.getGatewayConfiguration();
	this.restTemplate = restTemplateBuilder.errorHandler(new SilentRestTemplateResponseErrorHandler()).build();

	String port = (this.gatewayConfiguration.getPort() != null)? this.gatewayConfiguration.getPort() : DEFAULT_PORT;
	this.APIBaseUrl = this.gatewayConfiguration.getServer() + ":" + port;
	if (this.APIBaseUrl.lastIndexOf("/") < this.APIBaseUrl.length() - 1) {
	    this.APIBaseUrl += "/";
	}
    }

    /**
     * Perform HTTP request execution using GET method on the given service. You may use
     * <b>this.gatewayConfiguration</b> to retrieve configurations about
     * the gateway
     * 
     * @param service  the service you want to contact
     * @param url      the relative URL of the endpoint
     * @param DTOClass The class representation of the expected object to be
     *                 returned by the API
     * @param params   (optional) all parameters. See parameters interpolation
     *                 example below
     * @return A valued DTO object
     * 
     * @example <br>
     *          String url = "/theEndPoint/{pathParam}?anotherParam={queryParam}";
     *          <br>
     *          String pathParam = "param1"; <br>
     *          String queryParam = "param2"; <br>
     *          MyDTO theDTO = get&#60;MyDTO&#62;(url, MyDTO.class, pathParam,
     *          queryParam);
     */
    public T get(ServicesEnum service, String url, Class<T> DTOClass, Object... params) throws ServiceCallerException {
	String fullURL = this.APIBaseUrl + service.getRoute() + "/" + url;

	logger.info("HTTP GET request made => " + fullURL);
	ResponseEntity<String> responseEntity = restTemplate.getForEntity(fullURL, String.class, params);

	if (responseEntity.getStatusCode().isError()) {
	    throw new ServiceCallerException(responseEntity.getStatusCode());
	}

	return gson.fromJson(responseEntity.getBody(), DTOClass);
    }
    
    /**
     * Works like ServiceCaller::get method but the return is a List of T
     * 
     * @param service
     * @param url
     * @param clazz the class of T[]
     * @param params
     * @return
     * @throws ServiceCallerException
     * 
     * @see ServiceCaller::get
     */
    public List<T> getList(ServicesEnum service, String url, Class<T[]> clazz, Object... params) throws ServiceCallerException {
	String fullURL = this.APIBaseUrl + service.getRoute() + "/" + url;

	logger.info("HTTP GET request made => " + fullURL);
	ResponseEntity<String> responseEntity = restTemplate.getForEntity(fullURL, String.class, params);

	if (responseEntity.getStatusCode().isError()) {
	    throw new ServiceCallerException(responseEntity.getStatusCode());
	}

	T[] collection = gson.fromJson(responseEntity.getBody(), clazz);
	
	return Arrays.asList(collection);
    }
    
    /**
     * Perform HTTP request execution using POST method on the given service. You may use
     * <b>this.gatewayConfiguration</b> to retrieve configurations about
     * the gateway
     * 
     * @param service  the service you want to contact
     * @param url      the relative URL of the endpoint
     * @param DTOClass The class representation of the expected object to be
     *                 returned by the API
     * @param body     The request body
     * @param params   (optional) all parameters. See parameters interpolation
     *                 example below
     * @return A ServiceCallerResponse containing the given body
     * 
     * @example <br>
     *          String url = "/theEndPoint/{pathParam}?anotherParam={queryParam}";
     *          <br>
     *          String pathParam = "param1"; <br>
     *          String queryParam = "param2"; <br>
     *          ServiceCallerResponse response = post&#60;MyDTO&#62;(url, MyDTO.class, myDTOObject,
     *          queryParam);
     */
    public ServiceCallerResponse<T> post(ServicesEnum service, String url, Class<T> DTOClass, T body, Object... params) throws ServiceCallerException {
	String fullURL = this.APIBaseUrl + service.getRoute() + "/" + url;

	logger.info("HTTP POST request made => " + fullURL);
	HttpEntity<T> request = new HttpEntity<>(body);
	ResponseEntity<T> responseEntity = restTemplate.postForEntity(fullURL, request, DTOClass, params);

	if (responseEntity.getStatusCode().isError()) {
	    throw new ServiceCallerException(responseEntity.getStatusCode());
	}
	return new ServiceCallerResponse<T>(responseEntity.getBody(), responseEntity.getStatusCodeValue());
    }
    
    public void put(ServicesEnum service, String url, T body, Object... params) throws ServiceCallerException {
   	String fullURL = this.APIBaseUrl + service.getRoute() + "/" + url;

   	logger.info("HTTP PUT request made => " + fullURL);
   	HttpEntity<T> request = new HttpEntity<>(body);
   	restTemplate.put(fullURL, request, params);
    }
    
    public void delete(ServicesEnum service, String url, Object... params) throws ServiceCallerException {
	String fullURL = this.APIBaseUrl + service.getRoute() + "/" + url;

	logger.info("HTTP DELETE request made => " + fullURL);
	restTemplate.delete(fullURL, params);
    }
}
