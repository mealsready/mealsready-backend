package fr.cnam.mealsready.core.services;

public class ServiceCallerResponse<T> {
    public ServiceCallerResponse(T responseBody, int statusCode) {
	this.responseBody = responseBody;
	this.statusCode = statusCode;
    }
    
    private T responseBody;
    private int statusCode;
    
    public T getResponseBody() {
	return responseBody;
    }
    public int getStatusCode() {
	return statusCode;
    }
    
    
}
