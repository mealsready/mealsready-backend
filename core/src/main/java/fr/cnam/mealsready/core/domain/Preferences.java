package fr.cnam.mealsready.core.domain;

public class Preferences {
    private Integer nbDay;
    private Integer nbPerson;
    private boolean withStarters;
    private boolean withMainCourses;
    private boolean withDessert;

	public Preferences() {
	this.withStarters = true;
	this.withMainCourses = true;
	this.withDessert = true;
    }
	
    public Integer getNbDay() {
		return nbDay;
	}
	public void setNbDay(Integer nbDay) {
		this.nbDay = nbDay;
	}
	public Integer getNbPerson() {
		return nbPerson;
	}
	public void setNbPerson(Integer nbPerson) {
		this.nbPerson = nbPerson;
	}
	public boolean isWithStarters() {
		return withStarters;
	}
	public void setWithStarters(boolean withStarters) {
		this.withStarters = withStarters;
	}
	public boolean isWithMainCourses() {
		return withMainCourses;
	}
	public void setWithMainCourses(boolean withMainCourses) {
		this.withMainCourses = withMainCourses;
	}
	public boolean isWithDessert() {
		return withDessert;
	}
	public void setWithDessert(boolean withDessert) {
		this.withDessert = withDessert;
	}
}
