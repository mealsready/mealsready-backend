package fr.cnam.mealsready.core.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.google.gson.annotations.SerializedName;

public enum RecipeType {
    @SerializedName("0") //
    STARTER, 
    @SerializedName("1") //
    MAINCOURSE, 
    @SerializedName("2") //
    DESSERT,
    @SerializedName("3") //
    OTHER;

    private static RecipeType unit[] = { STARTER, MAINCOURSE, DESSERT, OTHER };

    @JsonCreator
    public static RecipeType forValue(int value) {
	return unit[value];
    }

    @JsonValue
    public Integer toValue() {
	for (int i = 0; i < unit.length; ++i) {
	    if (unit[i] == this) {
		return i;
	    }
	}
	return null;
    }
}
