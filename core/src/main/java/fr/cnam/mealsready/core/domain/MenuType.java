package fr.cnam.mealsready.core.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.google.gson.annotations.SerializedName;

public enum MenuType {
    @SerializedName("0") //
    BREAKFAST, //
    @SerializedName("1") //
    LUNCH, //
    @SerializedName("2") //
    DINNER;//

    private static MenuType unit[] = { BREAKFAST, LUNCH, DINNER };

    @JsonCreator
    public static MenuType forValue(int value) {
	return unit[value];
    }

    @JsonValue
    public Integer toValue() {
	for (int i = 0; i < unit.length; ++i) {
	    if (unit[i] == this) {
		return i;
	    }
	}
	return null;
    }
}
