package fr.cnam.mealsready.core.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:global-${spring.profiles.active}.properties")
public class CoreConfigFactory {
    private ExternalDataSourceCoreConfiguration externalDataSourceCoreConfiguration;
    private GatewayConfiguration gatewayConfiguration;

    @Autowired
    public CoreConfigFactory(ExternalDataSourceCoreConfiguration externalDataSourceCoreConfiguration, GatewayConfiguration gatewayConfiguration) {
	this.externalDataSourceCoreConfiguration = externalDataSourceCoreConfiguration;
	this.gatewayConfiguration = gatewayConfiguration;
    }

    public ExternalDataSourceCoreConfiguration getExternalDataSourceConfiguration() {
	return this.externalDataSourceCoreConfiguration;
    }
    
    public GatewayConfiguration getGatewayConfiguration() {
	return this.gatewayConfiguration;
    }
}
