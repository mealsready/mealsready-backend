package fr.cnam.mealsready.core.APIConnector;

import java.io.IOException;

import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

/**
 * Allow to keep the control of error handling for Spring restTemplate module
 * 
 * @author christopher MILAZZO
 *
 */
@Component
public class SilentRestTemplateResponseErrorHandler implements ResponseErrorHandler {
    @Override
    public boolean hasError(ClientHttpResponse httpResponse) 
      throws IOException {
        return false;
    }
 
    @Override
    public void handleError(ClientHttpResponse httpResponse) 
      throws IOException {
    	return;
    }
}
