package fr.cnam.mealsready.core;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import fr.cnam.mealsready.core.domain.MealsReadyRolesEnum;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface NeedsAuth {
    public MealsReadyRolesEnum role () default MealsReadyRolesEnum.USER;
}
