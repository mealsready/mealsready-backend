package fr.cnam.mealsready.core.config;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class GatewayConfiguration {
    
    @Autowired
    private Environment env;
    
    @PostConstruct
    private void init() {
	Map<String, String> sysEnv = System.getenv();
	this.setPort(this.env.getProperty("gateway.port"));
	
	if (sysEnv.containsKey("GATEWAY_IP")) {
	    this.setServer(sysEnv.get("GATEWAY_IP"));
	} else {	    
	    this.setServer(this.env.getProperty("gateway.server"));
	}
    }
    
    private String server;
    private String port;
    
    public String getServer() {
        return this.server;
    }
    public void setServer(String server) {
        this.server = server;
    }
    public String getPort() {
        return this.port;
    }
    public void setPort(String port) {
        this.port = port;
    }

}
