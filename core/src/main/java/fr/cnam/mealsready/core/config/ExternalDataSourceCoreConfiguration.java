package fr.cnam.mealsready.core.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class ExternalDataSourceCoreConfiguration {
    @Autowired
    private Environment env;
    
    @PostConstruct
    private void init() {
	this.setAPIBaseURL(this.env.getProperty("external-datasource.APIBaseURL"));
	this.setAPIToken(this.env.getProperty("external-datasource.APIToken"));
	this.setImageSpoonacularURL(this.env.getProperty("external-datasource.ImageSpoonacularURL"));
    }
    
    private String APIBaseURL;
    private String APIToken;
    private String ImageSpoonacularURL;

    public String getAPIBaseURL() {
	return APIBaseURL;
    }

    public void setAPIBaseURL(String aPIBaseURL) {
	APIBaseURL = aPIBaseURL;
    }

    public String getAPIToken() {
	return APIToken;
    }

    public void setAPIToken(String aPIToken) {
	APIToken = aPIToken;
    }

    public String getImageSpoonacularURL() {
	return ImageSpoonacularURL;
    }

    public void setImageSpoonacularURL(String imageSpoonacularURL) {
	ImageSpoonacularURL = imageSpoonacularURL;
    }
}
