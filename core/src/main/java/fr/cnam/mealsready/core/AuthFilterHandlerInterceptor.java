package fr.cnam.mealsready.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.AsyncHandlerInterceptor;

import fr.cnam.mealsready.core.domain.MealsReadyRolesEnum;
import fr.cnam.mealsready.core.domain.ServicesEnum;
import fr.cnam.mealsready.core.services.ServiceCaller;
import fr.cnam.mealsready.core.services.ServiceCallerException;

@Configuration
public class AuthFilterHandlerInterceptor implements AsyncHandlerInterceptor {

    @Autowired
    ServiceCaller<String> authCaller;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
	    throws Exception {
	HttpServletRequest req = (HttpServletRequest) request;
	HttpServletResponse resp = (HttpServletResponse) response;

	if (handler instanceof HandlerMethod) {
	    HandlerMethod handlerMethod = (HandlerMethod) handler;
	    // Test if the controller-method is annotated with @NeedsAuth
	    NeedsAuth filter = handlerMethod.getMethod().getAnnotation(NeedsAuth.class);
	    if (filter != null) {
		MealsReadyRolesEnum requiredRole = filter.role();
		// TODO Retrieve user role from user JWT
		MealsReadyRolesEnum userRole = MealsReadyRolesEnum.USER;
		String token = req.getHeader("Authorization");

		if (token == null) {
		    resp.setStatus(HttpStatus.UNAUTHORIZED.value());
		    return false;
		}

		try {
		    if (isAuthenticated(token) && isRoleAuthorized(requiredRole, userRole)) {
			return true;
		    } else {
			resp.setStatus(HttpStatus.FORBIDDEN.value());
			return false;
		    }
		} catch (Exception e) {
		    resp.setStatus(HttpStatus.SERVICE_UNAVAILABLE.value());
		    return false;
		}
	    }
	}

	return true;
    }

    private boolean isAuthenticated(String token) throws Exception {
	try {
	    this.authCaller.post(ServicesEnum.AUTHENTIFICATION_SERVICE, "/token/validate", String.class, token);
	} catch (ServiceCallerException e) {
	    if (e.getStatusCode() == HttpStatus.SERVICE_UNAVAILABLE) {
		throw new Exception();
	    }
	    return false;
	}
	return true;
    }

    private boolean isRoleAuthorized(MealsReadyRolesEnum requiredRole, MealsReadyRolesEnum userRole) {
	if (userRole == null) {
	    return false;
	} else {
	    return requiredRole.ordinal() <= userRole.ordinal();
	}
    }
}
