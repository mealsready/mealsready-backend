package fr.cnam.mealsready.core.services;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;

@Service
public class JWTService {
    private static final String JWT_ISSUER = "meals-ready-internal";
    private final JWTVerifier jwtVerifier;
    private final Algorithm algorithm;
    
    public JWTService() {
	this.algorithm = Algorithm.HMAC256("secret");
	this.jwtVerifier = JWT.require(this.algorithm)
	        .withIssuer(JWTService.JWT_ISSUER)
	        .build();
    }
    
    public String generateJWT() {
	try {
	    Calendar date = Calendar.getInstance();
	    long timeInMillis = date.getTimeInMillis();
	    Date expirationDate = new Date(timeInMillis + (60 * 1000));
	    return JWT.create()
	        .withIssuer(JWTService.JWT_ISSUER)
	        .withExpiresAt(expirationDate)
	        .sign(this.algorithm);
	} catch (JWTCreationException exception){
	    throw new RuntimeException();
	}
    }
    
    public boolean checkJWT(String token) {
	try {
	    DecodedJWT jwt = this.jwtVerifier.verify(token);
	    System.out.println(jwt.getExpiresAt());
	    return true;
	} catch (JWTVerificationException exception){
	    return false;
	}
    }
}
