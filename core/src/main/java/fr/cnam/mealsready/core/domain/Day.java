package fr.cnam.mealsready.core.domain;

import java.util.ArrayList;

public class Day {
	private String name;
	private Integer number;
	private ArrayList<Menu> menusList;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public ArrayList<Menu> getMenusList() {
		return menusList;
	}
	public void setMenusList(ArrayList<Menu> menusList) {
		this.menusList = menusList;
	}
}
