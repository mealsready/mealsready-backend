package fr.cnam.mealsready.core.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

/**
 * This service is a wrapper for the native Spring's resource loader. It is
 * meant to support jar's file system.
 * 
 * @author christopher MILAZZO
 * @since V1.0
 */
@Service
public class ResourceLoaderService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ResourceLoader resourceLoader;

    public String getResourceAbsolutePathString(String location) throws Exception {
	Resource resource = resourceLoader.getResource(location);

	String absolutePathString = "/";

	try {
	    if (resource.getURL().getProtocol().equals("jar")) {
		logger.debug("Jar file system activated");

		File tempFile = Files.createTempFile("Mealsready_backend_", null).toFile();
		resource.getInputStream().transferTo(new FileOutputStream(tempFile));

		absolutePathString = tempFile.getAbsolutePath();
	    } else {
		absolutePathString = resource.getFile().getAbsolutePath();
	    }
	} catch (IOException e) {
	    logger.error("Error while trying to retrieve a resource : " + e.getMessage());
	    // TO DELETE Remplacer par un ServiceException
	    throw new Exception();
	}

	return absolutePathString;
    }
}
