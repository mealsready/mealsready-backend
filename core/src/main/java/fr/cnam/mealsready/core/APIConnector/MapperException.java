package fr.cnam.mealsready.core.APIConnector;

public class MapperException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public MapperException() {
	super();
    }

    public MapperException(String message) {
	super(message);
    }
}
