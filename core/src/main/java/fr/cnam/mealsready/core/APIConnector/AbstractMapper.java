package fr.cnam.mealsready.core.APIConnector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author christopher MILAZZO
 * @since v1.0
 *
 * @param <T> The DTO object type
 * @param <U> The model object type
 */
public abstract class AbstractMapper<T extends APIConnectorDTO, U extends Object> {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Creates and returns an object built from DTO
     * 
     * @param sourceDTO
     * @return it should return an object defined in the domain package
     */
    abstract public U makeObjectFromDTO(T sourceDTO);

    /**
     * Creates and returns a DTO build from the given object.
     * 
     * @param sourceObject It should be an object from the domain package
     * @return a DTO
     */
    abstract public T makeDTOFromObject(U sourceObject);

    /**
     * Map all similar fields from source object to dest object
     * 
     * @param source The ref
     * @param dest   The object to be valued
     * @return the amount of fields mapped
     * 
     * @Throws MapperException when auto mapping failed
     */
    protected final int autoMap(Object source, Object dest) throws MapperException {
	java.lang.reflect.Field[] fields = this.getAllFieldsFromObject(source);
	Class<?> destClass = dest.getClass();
	int fieldsMappedQuantity = 0;

	for (int i = 0; i < fields.length; i++) {
	    java.lang.reflect.Field destField;
	    try {
		destField = destClass.getDeclaredField(fields[i].getName());
		try {
		    if (areSameType(destField, fields[i])) {
			destField.setAccessible(true);
			fields[i].setAccessible(true);
			destField.set(dest, fields[i].get(source));
			destField.setAccessible(false);
			fields[i].setAccessible(false);
			fieldsMappedQuantity++;
		    } else {
			logger.warn("Fields " + destField.getName() + " and " + fields[i].getName()
				+ " can't be mapped because their types are differents.");
		    }
		} catch (IllegalArgumentException | IllegalAccessException e) {
		    e.printStackTrace();
		    throw new MapperException(
			    "Something wrong happened when trying to map fields " + destField.getName() + " from "
				    + source.getClass().getName() + " to " + dest.getClass().getName());
		}
	    } catch (NoSuchFieldException | SecurityException e) {
		// No obvious mapping for this field
	    }
	}
	return fieldsMappedQuantity;
    }

    private java.lang.reflect.Field[] getAllFieldsFromObject(Object object) {
	Class<?> clazz = object.getClass();
	return clazz.getDeclaredFields();
    }

    private boolean areSameType(java.lang.reflect.Field field1, java.lang.reflect.Field field2) {
	return field1.getType() == field2.getType();
    }
}
