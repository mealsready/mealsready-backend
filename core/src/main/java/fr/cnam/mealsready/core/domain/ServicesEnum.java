package fr.cnam.mealsready.core.domain;

/**
 * If you want to expose a micoservice throught the gateway add a
 * new entry to this enum like so :<br>
 * 
 * MY_SERVICE("myServiceName", "my-service")<br>
 * With the previous entry, the micro service called "myServiceName" (application name)
 * will be accessible using the URL : <b>http://&#60;gateway&#62;/my-service/</b>
 * 
 * 
 * @author Christopher MILAZZO
 *
 */
public enum ServicesEnum {
    RECIPES_SERVICE("recipes-service", "recipes"),
    PDF_SERVICE("pdf-service", "pdf"),
    AUTHENTIFICATION_SERVICE("authentification-service", "auth"),
    SERVICE_REGISTER("serviceRegister-service", "service-register"),
    MANAGE_RECIPES("manageRecipes-service", "manage-recipes");
    
    private String serviceName;
    private String serviceRoute;
    
    private ServicesEnum(String serviceName, String serviceRoute) {
	this.serviceName = serviceName;
	this.serviceRoute = serviceRoute;
    }
    
    public String getRoute() {
	return this.serviceRoute;
    }
    
    public String getServiceName() {
	return this.serviceName;
    }
}
