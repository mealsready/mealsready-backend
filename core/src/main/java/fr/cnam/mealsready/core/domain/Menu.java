package fr.cnam.mealsready.core.domain;

public class Menu {
    private int number;
    private String name;
    private MenuType menuType;
    private Recipe starter;
    private Recipe mainCourse;
    private Recipe dessert;
    
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public MenuType getMenuType() {
		return menuType;
	}
	public void setMenuType(MenuType menuType) {
		this.menuType = menuType;
	}
	public Recipe getStarter() {
		return starter;
	}
	public void setStarter(Recipe starter) {
		this.starter = starter;
	}
	public Recipe getMainCourse() {
		return mainCourse;
	}
	public void setMainCourse(Recipe mainCourse) {
		this.mainCourse = mainCourse;
	}
	public Recipe getDessert() {
		return dessert;
	}
	public void setDessert(Recipe dessert) {
		this.dessert = dessert;
	}
}
