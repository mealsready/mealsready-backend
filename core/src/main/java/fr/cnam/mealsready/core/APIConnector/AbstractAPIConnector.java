package fr.cnam.mealsready.core.APIConnector;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import fr.cnam.mealsready.core.config.CoreConfigFactory;
import fr.cnam.mealsready.core.config.ExternalDataSourceCoreConfiguration;

/**
 * Extend from this class to make a new APIConnector without having to configure
 * rest client
 * 
 * @author christopher MILAZZO
 * @since v1.0
 *
 * @param <T> The type of the DTO object
 */
@Component
public abstract class AbstractAPIConnector<T extends APIConnectorDTO> {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Autowired
    private CoreConfigFactory coreConfigFactory;

    @Autowired
    Gson gson;

    private RestTemplate restTemplate;
    private String APIBaseUrl;

    protected ExternalDataSourceCoreConfiguration externalDataSourceCoreConfiguration;

    @PostConstruct
    private void init() {
	this.externalDataSourceCoreConfiguration = this.coreConfigFactory.getExternalDataSourceConfiguration();
	this.restTemplate = restTemplateBuilder.errorHandler(new SilentRestTemplateResponseErrorHandler()).build();

	this.APIBaseUrl = this.externalDataSourceCoreConfiguration.getAPIBaseURL();
	if (this.APIBaseUrl.lastIndexOf("/") < this.APIBaseUrl.length() - 1) {
	    this.APIBaseUrl += "/";
	}
    }

    /**
     * Perform HTTP request execution using GET method. You may use
     * <b>this.externalDataSourceConfiguration</b> to retrieve configurations about
     * external APIs
     * 
     * @param url      the relative URL of the endpoint
     * @param DTOClass The class representation of the expected object to be
     *                 returned by the API
     * @param params   (optional) all parameters. See parameters interpolation
     *                 example below
     * @return A valued APIConnectorDTO
     * 
     * @example <br>
     *          String url = "/theEndPoint/{pathParam}?anotherParam={queryParam}";
     *          <br>
     *          String pathParam = "param1"; <br>
     *          String queryParam = "param2"; <br>
     *          MyDTO theDTO = get&#60;MyDTO&#62;(url, MyDTO.class, pathParam,
     *          queryParam);
     */
    public T getAPI(String url, Class<T> DTOClass, Object... params) throws APIConnectorException {
	String fullURL = this.APIBaseUrl + url;

	// Remove tailing slash
	fullURL = fullURL.replaceFirst("\\/$", "");

	// APIToken injection
	if (fullURL.indexOf("?") != -1) {
	    fullURL += "&apiKey=" + this.externalDataSourceCoreConfiguration.getAPIToken();
	} else {
	    fullURL += "?apiKey=" + this.externalDataSourceCoreConfiguration.getAPIToken();
	}

	return this.get(fullURL, DTOClass, params);
    }

    /**
     * Perform HTTP request execution using GET method.
     * 
     * @param fullURL  The absolute URL
     * @param DTOClass The class representation of the expected object to be
     *                 returned by the API
     * @param params   (optional) all parameters. See parameters interpolation
     *                 example below
     * @return A valued APIConnectorDTO
     * 
     * @example <br>
     *          String url =
     *          "http://host/theEndPoint/{pathParam}?anotherParam={queryParam}";
     *          <br>
     *          String pathParam = "param1"; <br>
     *          String queryParam = "param2"; <br>
     *          MyDTO theDTO = get&#60;MyDTO&#62;(url, MyDTO.class, pathParam,
     *          queryParam);
     */
    public T get(String fullURL, Class<T> DTOClass, Object... params) throws APIConnectorException {
	logger.info("HTTP GET request made => " + fullURL);
	ResponseEntity<String> response = restTemplate.getForEntity(fullURL, String.class, params);

	if (response.getStatusCode().isError()) {
	    throw new APIConnectorException(response.getStatusCode());
	}

	return gson.fromJson(response.getBody(), DTOClass);
    }
    public Object get(String fullURL,Map<String, String> headerMap, Object... params) throws APIConnectorException {
	logger.info("HTTP GET request made => " + fullURL);
	HttpHeaders httpHeaders = createHeadersFromMap(headerMap);
	// build the request
	HttpEntity<Object> request = new HttpEntity<Object>(httpHeaders);
	ResponseEntity<String> response = restTemplate.exchange(
		fullURL,
		HttpMethod.GET,
		request,
		String.class,
		params
		);

	if (response.getStatusCode().isError()) {
	    throw new APIConnectorException(response.getStatusCode());
	}

	return response.getBody();
    }

    /**
     * 
     * @param fullURL
     * @param headerMap
     * @param DTOClass
     * @param params
     * @return
     * @throws APIConnectorException
     */
    public T get(String fullURL, Map<String, String> headerMap, Class<T> DTOClass, Object... params) throws APIConnectorException {
	logger.info("HTTP GET request made => " + fullURL);

	HttpHeaders httpHeaders = createHeadersFromMap(headerMap);
	// build the request
	HttpEntity<Object> request = new HttpEntity<Object>(httpHeaders);
	ResponseEntity<String> response = restTemplate.exchange(
		fullURL,
		HttpMethod.GET,
		request,
		String.class,
		params
		);
	if (response.getStatusCode().isError()) {
	    throw new APIConnectorException(response.getStatusCode());
	}

	return gson.fromJson(response.getBody(), DTOClass);
    }

    /**
     * 
     * @param fullURL
     * @param body
     * @param headers
     * @param params
     * @return
     * @throws APIConnectorException
     */
    public Integer put(String fullURL, Map<String, String> headerMap, Object body, Object... params) throws APIConnectorException {
	HttpHeaders httpHeaders = createHeadersFromMap(headerMap);
	// build the request
	HttpEntity<Object> request = new HttpEntity<Object>(body, httpHeaders);
	ResponseEntity<String> responseEntity = restTemplate.exchange(
		fullURL,
		HttpMethod.PUT,
		request,
		String.class,
		params
		);
	if (responseEntity.getStatusCode().isError()) {
	    throw new APIConnectorException(responseEntity.getStatusCode());
	}
	return responseEntity.getStatusCodeValue();
    }
    
    /**
     * 
     * @param fullURL
     * @param headerMap
     * @param DTOClass
     * @param body
     * @param params
     * @return
     * @throws APIConnectorException
     */
    public T put(String fullURL, Map<String, String> headerMap, Class<T> DTOClass, Object body, Object... params) throws APIConnectorException {
	HttpHeaders httpHeaders = createHeadersFromMap(headerMap);
	// build the request
	HttpEntity<Object> request = new HttpEntity<Object>(body, httpHeaders);
	ResponseEntity<String> responseEntity = restTemplate.exchange(
		fullURL,
		HttpMethod.PUT,
		request,
		String.class,
		params
		);
	if (responseEntity.getStatusCode().isError()) {
	    throw new APIConnectorException(responseEntity.getStatusCode());
	}
	return gson.fromJson(responseEntity.getBody(), DTOClass);
    }
    
    /**
     * 
     * @param fullURL
     * @param headerMap
     * @param DTOClass
     * @param body
     * @param params
     * @return
     * @throws APIConnectorException
     */
    public T post(String fullURL, Map<String, String> headerMap, Class<T> DTOClass, Object body, Object... params) throws APIConnectorException {
	HttpHeaders httpHeaders = createHeadersFromMap(headerMap);
	// build the request
	HttpEntity<Object> request = new HttpEntity<Object>(body, httpHeaders);
	ResponseEntity<String> responseEntity = restTemplate.postForEntity(fullURL, request, String.class, params);
	if (responseEntity.getStatusCode().isError()) {
	    throw new APIConnectorException(responseEntity.getStatusCode());
	}
	return gson.fromJson(responseEntity.getBody(), DTOClass);
    }


    /**
     * Perform POST HTTP Request on fullUrl and return the success HTTP Status code or throw APIConnectorException
     * that contains the HTTP Status code and an error message
     * 
     * @param fullURL
     * @param body the data to put in the request body
     * @param params URL parameters. Works like for get method
     * @return HTTP Status code
     * @throws APIConnectorException
     */
    public Integer post(String fullURL, Object body, Object... params) throws APIConnectorException {
	return this.post(fullURL, body, new HashMap<String, String>(), params);
    }

    /**
     * Perform POST HTTP Request on fullUrl and return the success HTTP Status code or throw APIConnectorException
     * that contains the HTTP Status code and an error message
     * 
     * @param fullURL
     * @param body the data to put in the request body
     * @param headers a Map of HTTP header.
     * @param params URL parameters. Works like for get method
     * @return HTTP Status code
     * @throws APIConnectorException
     */
    public Integer post(String fullURL, Object body, Map<String, String> headers, Object... params) throws APIConnectorException {
	ResponseEntity<String> responseEntity = this.doPost(fullURL, body, headers, params);

	if (responseEntity.getStatusCode().isError()) {
	    throw new APIConnectorException(responseEntity.getStatusCode());
	}
	return responseEntity.getStatusCodeValue();
    }

    /**
     * Perform a post HTTP request 
     * 
     * @param fullURL
     * @param DTOClass
     * @param body
     * @param params
     * @return
     * @throws APIConnectorException
     */
    public T post(String fullURL, Class<T> DTOClass, Object body, Object... params) throws APIConnectorException {
	return this.post(fullURL, DTOClass, body, new HashMap<String, String>(), params);
    }

    /**
     * Perform a post HTTP request. Will return an object of type T.
     * Throws ApiConnectorException with the error status HTTP code. 
     * 
     * @param fullURL
     * @param DTOClass
     * @param body
     * @param headers
     * @param params
     * @return
     * @throws APIConnectorException
     */
    public T post(String fullURL, Class<T> DTOClass, Object body, Map<String, String> headers, Object... params)
	    throws APIConnectorException {

	ResponseEntity<String> responseEntity = this.doPost(fullURL, body, headers, params);

	if (responseEntity.getStatusCode().isError()) {
	    throw new APIConnectorException(responseEntity.getStatusCode());
	}
	return gson.fromJson(responseEntity.getBody(), DTOClass);
    }

    /**
     * Perform the post HTTP Request
     */
    private ResponseEntity<String> doPost(String fullURL, Object body, Map<String, String> headers,
	    Object... params) throws APIConnectorException {
	logger.info("HTTP POST request made => " + fullURL);

	HttpHeaders httpHeaders = createHeadersFromMap(headers);

	if (httpHeaders.getContentType().toString().equals(MediaType.APPLICATION_FORM_URLENCODED.toString())) {
	    return this.doPostAppFormUrlEncoded(fullURL, body, httpHeaders, params);
	} else { // JSON body
	    HttpEntity<Object> request = new HttpEntity<>(body, httpHeaders);
	    return restTemplate.postForEntity(fullURL, request, String.class, params);
	}
    }

    /**
     * Perform the post HTTP request with body of type x-www-form-urlencoded
     */
    private ResponseEntity<String> doPostAppFormUrlEncoded(String fullURL, Object body, HttpHeaders httpHeaders,
	    Object... params) throws APIConnectorException {
	MultiValueMap<String, Object> postParameters = this.parseBody(body);
	HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(postParameters, httpHeaders);
	return restTemplate.postForEntity(fullURL, request, String.class, params);
    }

    /**
     * Transform a map to HttpHeaders object
     * 
     * @param headerMap
     * @return
     */
    private HttpHeaders createHeadersFromMap(Map<String, String> headerMap) {
	HttpHeaders headers = new HttpHeaders();

	Iterator<Entry<String, String>> iterator = headerMap.entrySet().iterator();

	while (iterator.hasNext()) {
	    Entry<String, String> entry = iterator.next();
	    headers.add(entry.getKey(), entry.getValue());
	}
	return headers;
    }

    /**
     * Construct a map for a post request using application/x-www-form-urlencoded
     * 
     * @param body
     * @return
     * @throws APIConnectorException
     */
    private MultiValueMap<String, Object> parseBody(Object body) throws APIConnectorException {
	MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();

	Class<? extends Object> clazz = body.getClass();
	Field[] fields = clazz.getDeclaredFields();

	for (int i = 0; i < fields.length; i++) {
	    try {
		fields[i].setAccessible(true);
		bodyMap.add(fields[i].getName(), fields[i].get(body) != null ? fields[i].get(body).toString() : "");
		fields[i].setAccessible(false);
	    } catch (IllegalArgumentException e) {
		e.printStackTrace();
		throw new APIConnectorException("Unable to parse body");
	    } catch (IllegalAccessException e) {
		e.printStackTrace();
		throw new APIConnectorException("Unable to parse body");
	    }
	}
	return bodyMap;
    }
}
