package fr.cnam.mealsready.core.domain;

import java.util.ArrayList;

public class Recipe {
    private Long id;
    private String name;
    private String imageURL;
    private int servings;
    private String duration;
    private String description;
    private RecipeType recipeType;
    private ArrayList<InstructionStep> instructions = new ArrayList<InstructionStep>();
    private ArrayList<Ingredient> ingredientsList = new ArrayList<Ingredient>();
    private Quality quality;
    private Diet diet; 
    
    public Quality getQuality() {
        return quality;
    }

    public void setQuality(Quality quality) {
        this.quality = quality;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public RecipeType getRecipeType() {
	return recipeType;
    }

    public void setRecipeType(RecipeType recipeType) {
	this.recipeType = recipeType;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getImageURL() {
	return imageURL;
    }

    public void setImageURL(String imageURL) {
	this.imageURL = imageURL;
    }

    public int getServings() {
	return servings;
    }

    public void setServings(int servings) {
	this.servings = servings;
    }

    public String getDuration() {
	return duration;
    }

    public void setDuration(String duration) {
	this.duration = duration;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public ArrayList<InstructionStep> getInstructions() {
	return instructions;
    }

    public void setInstructions(ArrayList<InstructionStep> analyzedInstructions) {
	this.instructions = analyzedInstructions;
    }

    public ArrayList<Ingredient> getIngredientsList() {
	return ingredientsList;
    }

    public void setIngredientsList(ArrayList<Ingredient> ingredientsList) {
	this.ingredientsList = ingredientsList;
    }

    public Diet getDiet() {
	return diet;
    }

    public void setDiet(Diet diet) {
	this.diet = diet;
    }
}
