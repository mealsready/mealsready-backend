package fr.cnam.mealsready.core.domain;

public class InstructionStep {
    	private	Long id; 
	private String number;
	private String instruction;
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getInstruction() {
		return instruction;
	}
	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}
	public Long getId() {
	    return id;
	}
	public void setId(Long id) {
	    this.id = id;
	}
}
