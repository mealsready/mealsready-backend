package fr.cnam.mealsready.core.domain;

public enum ExternalAPIEnum {
    SPOONACULAR("spoonacular"),
    KEYCLOAK("keycloak");

    private String APIName;

    private ExternalAPIEnum(String serviceName) {
	this.APIName = serviceName;
    }

    public String getAPIName() {
	return this.APIName;
    }
}
