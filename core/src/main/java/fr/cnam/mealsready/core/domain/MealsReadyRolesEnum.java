package fr.cnam.mealsready.core.domain;

public enum MealsReadyRolesEnum {
    USER,
    ADMIN
}
