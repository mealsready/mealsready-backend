package fr.cnam.mealsready.manage_recipes.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.cnam.mealsready.manage_recipes.entities.IngredientQuantityEntity;

@Repository

public interface IngredientQuantityDAO extends JpaRepository<IngredientQuantityEntity, Long> {

	@Query(value = "DELETE FROM ingredients_quantities WHERE id_recipe IN (?)", nativeQuery = true)
	void deleteByRecipe(Long id_recipe);

	@Query(value = "SELECT * FROM ingredients_quantities WHERE id_recipe IN (?)", nativeQuery = true)
	List<IngredientQuantityEntity> selectByRecipe(Long id_recipe);
}
