package fr.cnam.mealsready.manage_recipes.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.cnam.mealsready.manage_recipes.entities.UnitEntity;
import fr.cnam.mealsready.manage_recipes.exceptions.ItemALreadyExistsException;
import fr.cnam.mealsready.manage_recipes.exceptions.NotFoundHandlerException;
import fr.cnam.mealsready.manage_recipes.handlers.UnitHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Api(tags = { "/unit" }, description = "Units of measure (Ex : g, mg, L, piece)")
@CrossOrigin
@RestController
@RequestMapping("/unit")
public class UnitController {

	@Autowired
	UnitHandler unitHandler;

	@PostMapping(value = "/create")
	@ApiOperation(value = "Create a unit type Ex : vegetarian")
	@ApiResponses(value = { //
			@ApiResponse(code = 422, message = "The Unit name already exists and can not be used"), //
	})
	public ResponseEntity<String> create(
			@ApiParam(name = "name", type = "String", value = "Unit name", example = "vegan", required = true) @RequestBody String name) {
		try {
			UnitEntity unitEntity = unitHandler.create(name);
			String reponse = "Unit created with  value : " + unitEntity.getName();
			return new ResponseEntity<>(reponse, HttpStatus.OK);
		} catch (ItemALreadyExistsException e) {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@PutMapping(value = "/update")
	@ApiOperation(value = "Change a Unit name")
	@ApiResponses(value = { //
			@ApiResponse(code = 422, message = "The Unit name already exists and can not be used"), //
			@ApiResponse(code = 404, message = "Unit does not exist"), //
	})
	public ResponseEntity<String> update(
			@ApiParam(name = "unit", type = "Unit", value = "unit", example = "vegan", required = true) @RequestBody UnitEntity unitEntity) {
		try {
			unitHandler.update(unitEntity);
			String response = "Unit updated with new value : " + unitEntity.getName();
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (NotFoundHandlerException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (ItemALreadyExistsException e) {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@DeleteMapping(value = "/delete/{id}")
	@ApiOperation(value = "Delete a Unit from database")
	@ApiResponses(value = { //
			@ApiResponse(code = 404, message = "Unit does not exist"), //
			@ApiResponse(code = 409, message = "Request is impossible") //
	})
	public ResponseEntity<String> delete(
			@ApiParam(name = "id", type = "Long", value = "id", example = "001", required = true) @PathVariable("id") Long id) {
		try {
			unitHandler.delete(id);
			String response = "Unit with id : " + id + " has been deleted";
			return new ResponseEntity<String>(response, HttpStatus.OK);
		} catch (NotFoundHandlerException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (DataIntegrityViolationException e) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
	}

	@GetMapping(value = "/get/{id}")
	@ApiOperation(value = "Get a Unit by id")
	@ApiResponses(value = { //
			@ApiResponse(code = 404, message = "Unit does not exist"), //
	})
	public ResponseEntity<UnitEntity> get(
			@ApiParam(name = "id", type = "Long", value = "id", example = "001", required = true) @PathVariable("id") Long id) {
		try {
			return new ResponseEntity<UnitEntity>(unitHandler.get(id), HttpStatus.OK);
		} catch (NotFoundHandlerException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(value = "/get_all")
	@ApiOperation(value = "Get a all Units from database")
	@ApiResponses(value = { //
			@ApiResponse(code = 503, message = "Service unavailable"), //
	})
	public ResponseEntity<List<UnitEntity>> getAll() {
		try {
			return new ResponseEntity<List<UnitEntity>>(unitHandler.getAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
		}
	}
}
