package fr.cnam.mealsready.manage_recipes.exceptions;

public class NotFoundHandlerException extends HandlerException {

    private static final long serialVersionUID = 1L;

    public NotFoundHandlerException() {
	this("The resource you are looking for could not be found");
    }

    public NotFoundHandlerException(String message) {
	super(message);
    }

}
