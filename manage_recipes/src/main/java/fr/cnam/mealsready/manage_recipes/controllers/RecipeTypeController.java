package fr.cnam.mealsready.manage_recipes.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.cnam.mealsready.manage_recipes.entities.RecipeTypeEntity;
import fr.cnam.mealsready.manage_recipes.exceptions.ItemALreadyExistsException;
import fr.cnam.mealsready.manage_recipes.exceptions.NotFoundHandlerException;
import fr.cnam.mealsready.manage_recipes.handlers.RecipeTypeHandler;
import fr.cnam.mealsready.manage_recipes.tools.Tools;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Api(tags = { "/recipe_type" }, description = "Type of food that makes up a meal (Ex : starter, maincourse, dessert)")
@CrossOrigin
@RestController
@RequestMapping("/recipe_type")
public class RecipeTypeController {

	@Autowired
	RecipeTypeHandler recipeTypeHandler;

	@PostMapping(value = "/create")
	@ApiOperation(value = "Create a recipeType type Ex : vegetarian")
	@ApiResponses(value = { //
			@ApiResponse(code = 422, message = "The RecipeType name already exists and can not be used"), //
	})
	public ResponseEntity<String> create(
			@ApiParam(name = "name", type = "String", value = "RecipeType name", example = "vegan", required = true) @RequestBody String name) {
		try {
			RecipeTypeEntity recipeTypeEntity = recipeTypeHandler.create(Tools.upperCaseFirst(name));
			String reponse = "RecipeType created with  value : " + recipeTypeEntity.getName();
			return new ResponseEntity<>(reponse, HttpStatus.OK);
		} catch (ItemALreadyExistsException e) {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@PutMapping(value = "/update")
	@ApiOperation(value = "Change a RecipeType name")
	@ApiResponses(value = { //
			@ApiResponse(code = 422, message = "The RecipeType name already exists and can not be used"), //
			@ApiResponse(code = 404, message = "RecipeType does not exist"), //
	})
	public ResponseEntity<String> update(
			@ApiParam(name = "recipeType", type = "RecipeType", value = "recipeType", example = "vegan", required = true) @RequestBody RecipeTypeEntity recipeTypeEntity) {
		try {
			recipeTypeEntity.setName(Tools.upperCaseFirst(recipeTypeEntity.getName()));
			recipeTypeHandler.update(recipeTypeEntity);
			String response = "RecipeType updated with new value : " + recipeTypeEntity.getName();
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (NotFoundHandlerException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (ItemALreadyExistsException e) {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@DeleteMapping(value = "/delete/{id}")
	@ApiOperation(value = "Delete a RecipeType from database")
	@ApiResponses(value = { //
			@ApiResponse(code = 404, message = "RecipeType does not exist"), //
			@ApiResponse(code = 409, message = "Request is impossible") //
	})
	public ResponseEntity<String> delete(
			@ApiParam(name = "id", type = "Long", value = "id", example = "001", required = true) @PathVariable("id") Long id) {
		try {
			recipeTypeHandler.delete(id);
			String response = "RecipeType with id : " + id + " has been deleted";
			return new ResponseEntity<String>(response, HttpStatus.OK);
		} catch (NotFoundHandlerException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (DataIntegrityViolationException e) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
	}

	@GetMapping(value = "/get/{id}")
	@ApiOperation(value = "Get a RecipeType by id")
	@ApiResponses(value = { //
			@ApiResponse(code = 404, message = "RecipeType does not exist"), //
	})
	public ResponseEntity<RecipeTypeEntity> get(
			@ApiParam(name = "id", type = "Long", value = "id", example = "001", required = true) @PathVariable("id") Long id) {
		try {
			return new ResponseEntity<RecipeTypeEntity>(recipeTypeHandler.get(id), HttpStatus.OK);
		} catch (NotFoundHandlerException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(value = "/get_all")
	@ApiOperation(value = "Get a all RecipeTypes from database")
	@ApiResponses(value = { //
			@ApiResponse(code = 503, message = "Service unavailable"), //
	})
	public ResponseEntity<List<RecipeTypeEntity>> getAll() {
		try {
			return new ResponseEntity<List<RecipeTypeEntity>>(recipeTypeHandler.getAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
		}
	}
}
