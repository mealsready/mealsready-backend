package fr.cnam.mealsready.manage_recipes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@EnableDiscoveryClient
@SpringBootApplication(exclude = ErrorMvcAutoConfiguration.class)
@ComponentScan({"fr.cnam.mealsready.manage_recipes", "fr.cnam.mealsready.core"})
@EntityScan({"fr.cnam.mealsready.manage_recipes.entities"})
public class manage_recipesApplication {

    public static void main(String[] args) {
    	SpringApplication.run(manage_recipesApplication.class, args);
    }

}
