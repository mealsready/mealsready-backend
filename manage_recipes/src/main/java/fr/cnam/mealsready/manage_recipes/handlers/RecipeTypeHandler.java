package fr.cnam.mealsready.manage_recipes.handlers;

import org.springframework.stereotype.Service;

import fr.cnam.mealsready.manage_recipes.DAO.RecipeTypeDAO;
import fr.cnam.mealsready.manage_recipes.entities.RecipeTypeEntity;
import fr.cnam.mealsready.manage_recipes.exceptions.ItemALreadyExistsException;
import fr.cnam.mealsready.manage_recipes.exceptions.NotFoundHandlerException;

@Service
public class RecipeTypeHandler extends CrudHandler<RecipeTypeEntity, RecipeTypeDAO> {

	public RecipeTypeEntity create(String name) {
		if (this.existsByName(name)) {
			throw new ItemALreadyExistsException("RecipeType with name : " + name + " already exists");
		}
		RecipeTypeEntity recipeTypeEntity = new RecipeTypeEntity();
		recipeTypeEntity.setName(name);
		return this.dao.save(recipeTypeEntity);
	}

	@Override
	public RecipeTypeEntity update(RecipeTypeEntity recipeTypeEntity) {
		if (!this.dao.existsById(recipeTypeEntity.getId())) {
			throw new NotFoundHandlerException("RecipeType does not exist");
		}
		if (this.existsByName(recipeTypeEntity.getName())) {
			throw new ItemALreadyExistsException("RecipeType with name : " + recipeTypeEntity.getName() + " already exists");
		}
		return this.dao.save(recipeTypeEntity);
	}

	@Override
	public void delete(Long id) {
		if (!this.dao.existsById(id)) {
			throw new NotFoundHandlerException("RecipeType does not exist");
		}
		this.dao.deleteById(id);
	}

	public boolean existsByName(String name) {
		return this.dao.existsByName(name);
	}

}
