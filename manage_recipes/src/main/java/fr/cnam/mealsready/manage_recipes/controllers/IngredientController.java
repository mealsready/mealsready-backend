package fr.cnam.mealsready.manage_recipes.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

import fr.cnam.mealsready.core.domain.Ingredient;
import fr.cnam.mealsready.manage_recipes.entities.IngredientEntity;
import fr.cnam.mealsready.manage_recipes.exceptions.ItemALreadyExistsException;
import fr.cnam.mealsready.manage_recipes.exceptions.NotFoundHandlerException;
import fr.cnam.mealsready.manage_recipes.handlers.IngredientHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Api(tags = { "/ingredient" }, description = "Product that goes into the composition of a recipe")
@CrossOrigin
@RestController
@RequestMapping("/ingredient")
public class IngredientController {

	@Autowired
	IngredientHandler ingredientHandler;

	
	@PostMapping(value = "/create")
	@ApiOperation(value = "Create an Ingredient Ex : pomme de terre")
	@ApiResponses(value = { //
			@ApiResponse(code = 404, message = "The given argument does not exist"), //
			@ApiResponse(code = 422, message = "The Ingredient name already exists and can not be used") //
	})
	public ResponseEntity<String> create(
			@ApiParam(name = "ingredient", type = "IngredientCreateRequest", value = "ingredient", example = "pomme de terre", required = true) @RequestBody Ingredient ingredientCreateRequest) {
		try {
			IngredientEntity ingredientEntity = ingredientHandler.create(ingredientCreateRequest);
			String reponse = "Ingredient created with  value : " + ingredientEntity.getName();
			return new ResponseEntity<>(reponse, HttpStatus.OK);
		} catch (NotFoundHandlerException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (ItemALreadyExistsException e) {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@PutMapping(value = "/update")
	@ApiOperation(value = "Update an Ingredient Ex : pomme de terre")
	@ApiResponses(value = { //
			@ApiResponse(code = 404, message = "The given argument does not exist"), //
			@ApiResponse(code = 422, message = "The Ingredient name already exists and can not be used") //
	})
	public ResponseEntity<String> update(
			@ApiParam(name = "ingredient", type = "IngredientUpdateRequest", value = "ingredient", example = "pomme de terre", required = true) @RequestBody Ingredient ingredientUpdateRequest) {
		try {
			ingredientHandler.update(ingredientUpdateRequest);
			String response = "Ingredient updated with new value : " + ingredientUpdateRequest.getName();
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (NotFoundHandlerException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (ItemALreadyExistsException e) {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@DeleteMapping(value = "/delete/{id}")
	@ApiOperation(value = "Delete a Ingredient from database")
	@ApiResponses(value = { //
			@ApiResponse(code = 404, message = "Ingredient does not exist"), //
			@ApiResponse(code = 409, message = "Request is impossible") //
	})
	public ResponseEntity<String> delete(
			@ApiParam(name = "id", type = "Long", value = "id", example = "001", required = true) @PathVariable("id") Long id) {
		try {
			ingredientHandler.delete(id);
			String response = "Ingredient with id : " + id + " has been deleted";
			return new ResponseEntity<String>(response, HttpStatus.OK);
		} catch (NotFoundHandlerException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (DataIntegrityViolationException e) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
	}

	@GetMapping(value = "/get/{id}")
	@ApiOperation(value = "Get a Ingredient by id")
	@ApiResponses(value = { //
			@ApiResponse(code = 404, message = "Ingredient does not exist"), //
	})
	public ResponseEntity<IngredientEntity> get(
			@ApiParam(name = "id", type = "Long", value = "id", example = "001", required = true) @PathVariable("id") Long id) {
		try {
			return new ResponseEntity<IngredientEntity>(ingredientHandler.get(id), HttpStatus.OK);
		} catch (NotFoundHandlerException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(value = "/get_all")
	@ApiOperation(value = "Get a all Ingredient from database")
	@ApiResponses(value = { //
			@ApiResponse(code = 503, message = "Service unavailable"), //
	})
	public ResponseEntity<List<IngredientEntity>> getAll() {
		try {
			return new ResponseEntity<List<IngredientEntity>>(ingredientHandler.getAll(), HttpStatus.OK);
		} catch (HttpServerErrorException e) {
			return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
		}
	}
}
