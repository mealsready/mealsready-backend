package fr.cnam.mealsready.manage_recipes.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.cnam.mealsready.manage_recipes.entities.DietEntity;
import fr.cnam.mealsready.manage_recipes.exceptions.ItemALreadyExistsException;
import fr.cnam.mealsready.manage_recipes.exceptions.NotFoundHandlerException;
import fr.cnam.mealsready.manage_recipes.handlers.DietHandler;
import fr.cnam.mealsready.manage_recipes.tools.Tools;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Api(tags = {
		"/diet" }, description = "Restrictive condition (Ex : vegetarian, vegan, gluten free, without salt, without sugar, athletic)")
@CrossOrigin
@RestController
@RequestMapping("/diet")
public class DietController {

	@Autowired
	DietHandler dietHandler;

	@PostMapping(value = "/create")
	@ApiOperation(value = "Create a Diet type Ex : vegetarian")
	@ApiResponses(value = { //
			@ApiResponse(code = 422, message = "The Diet name already exists and can not be used"), //
	})
	public ResponseEntity<String> create(
			@ApiParam(name = "name", type = "String", value = "Diet name", example = "vegan", required = true) @RequestBody String name) {
		try {
			DietEntity dietEntity = dietHandler.create(Tools.upperCaseFirst(name));
			String reponse = "Diet created with  value : " + dietEntity.getName();
			return new ResponseEntity<>(reponse, HttpStatus.OK);
		} catch (ItemALreadyExistsException e) {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@PutMapping(value = "/update")
	@ApiOperation(value = "Change a Diet name")
	@ApiResponses(value = { //
			@ApiResponse(code = 422, message = "The Diet name already exists and can not be used"), //
			@ApiResponse(code = 404, message = "Diet does not exist"), //
	})
	public ResponseEntity<String> update(
			@ApiParam(name = "diet", type = "Diet", value = "diet", example = "vegan", required = true) @RequestBody DietEntity dietEntity) {
		try {
			dietEntity.setName(Tools.upperCaseFirst(dietEntity.getName()));
			dietHandler.update(dietEntity);
			String response = "Diet updated with new value : " + dietEntity.getName();
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (NotFoundHandlerException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (ItemALreadyExistsException e) {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@DeleteMapping(value = "/delete/{id}")
	@ApiOperation(value = "Delete a Diet from database")
	@ApiResponses(value = { //
			@ApiResponse(code = 404, message = "Diet does not exist"), //
			@ApiResponse(code = 409, message = "Request is impossible") //
	})
	public ResponseEntity<String> delete(
			@ApiParam(name = "id", type = "Long", value = "id", example = "001", required = true) @PathVariable("id") Long id) {
		try {
			dietHandler.delete(id);
			String response = "Diet with id : " + id + " has been deleted";
			return new ResponseEntity<String>(response, HttpStatus.OK);
		} catch (NotFoundHandlerException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (DataIntegrityViolationException e) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
	}

	@GetMapping(value = "/get/{id}")
	@ApiOperation(value = "Get a Diet by id")
	@ApiResponses(value = { //
			@ApiResponse(code = 404, message = "Diet does not exist"), //
	})
	public ResponseEntity<DietEntity> get(
			@ApiParam(name = "id", type = "Long", value = "id", example = "001", required = true) @PathVariable("id") Long id) {
		try {
			return new ResponseEntity<DietEntity>(dietHandler.get(id), HttpStatus.OK);
		} catch (NotFoundHandlerException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(value = "/get_all")
	@ApiOperation(value = "Get a all Diets from database")
	@ApiResponses(value = { //
			@ApiResponse(code = 503, message = "Service unavailable"), //
	})
	public ResponseEntity<List<DietEntity>> getAll() {
		try {
			return new ResponseEntity<List<DietEntity>>(dietHandler.getAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
		}
	}
}
