package fr.cnam.mealsready.manage_recipes.handlers;

import java.util.List;

import org.postgresql.util.PSQLException;
import org.springframework.stereotype.Service;

import fr.cnam.mealsready.manage_recipes.DAO.IngredientQuantityDAO;
import fr.cnam.mealsready.manage_recipes.entities.IngredientQuantityEntity;

@Service
public class IngredientQuantityHandler extends CrudHandler<IngredientQuantityEntity, IngredientQuantityDAO> {

	public void deleteByRecipe(Long id_recipe) throws PSQLException {
//		if (this.dao.selectByRecipe(id_recipe).isEmpty()) {
//			throw new NotFoundHandlerException("Recipe does not exist");
//		}
//		this.dao.deleteByRecipe(id_recipe);
		List<IngredientQuantityEntity> ingredientQuantityList = this.dao.selectByRecipe(id_recipe);
		for (IngredientQuantityEntity ingredientQuantityEntity : ingredientQuantityList) {
			this.dao.delete(ingredientQuantityEntity);
		}
	}
}
