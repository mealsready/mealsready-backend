package fr.cnam.mealsready.manage_recipes.exceptions;

public class ItemALreadyExistsException  extends RuntimeException {

    private static final long serialVersionUID = 989857920161781391L;

    public ItemALreadyExistsException() {
	this("The resource does already exist");
    }

    public ItemALreadyExistsException(String message) {
	super(message);
    }

}
