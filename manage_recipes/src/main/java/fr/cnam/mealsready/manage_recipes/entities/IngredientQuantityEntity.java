package fr.cnam.mealsready.manage_recipes.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import fr.cnam.mealsready.core.APIConnector.APIConnectorDTO;

@Entity
@Table(name = "ingredients_quantities")
public class IngredientQuantityEntity implements APIConnectorDTO {

	@Id
	@JsonIgnore
	IngredientQuantityIdEntity pk = new IngredientQuantityIdEntity();

	@Column(name = "quantity")
	private int quantity;

	public IngredientQuantityIdEntity getPk() {
		return pk;
	}

	public void setPk(IngredientQuantityIdEntity pk) {
		this.pk = pk;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public RecipeEntity getRecipe() {
		return getPk().getRecipeEntity();
	}

	public void setRecipe(RecipeEntity recipeEntity) {
		getPk().setRecipeEntity(recipeEntity);
	}

	public IngredientEntity getIngredient() {
		return getPk().getIngredientEntity();
	}

	public void setIngredient(IngredientEntity ingredientEntity) {
		getPk().setIngredientEntity(ingredientEntity);
	}
}
