package fr.cnam.mealsready.manage_recipes.controllers;

import java.util.ArrayList;
import java.util.List;

import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

import fr.cnam.mealsready.core.domain.Recipe;
import fr.cnam.mealsready.manage_recipes.entities.RecipeEntity;
import fr.cnam.mealsready.manage_recipes.exceptions.NotFoundHandlerException;
import fr.cnam.mealsready.manage_recipes.handlers.RecipeHandler;
import fr.cnam.mealsready.manage_recipes.mappers.RecipeMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Api(tags = { "/recipe" }, description = "Process for carrying out the preparation of a dish")
@CrossOrigin
@RestController
@RequestMapping("/recipe")
public class RecipeController {

    @Autowired
    RecipeHandler recipeHandler;

    @Autowired
    RecipeMapper recipeMapper;

    @PostMapping(value = "/create")
    @ApiOperation(value = "Create a Recipe (Ex : tartiflette)")
    @ApiResponses(value = { //
	    @ApiResponse(code = 404, message = "The given argument does not exist"), //
    })
    public ResponseEntity<String> create(
	    @ApiParam(name = "recipe", type = "RecipeCreateRequest", value = "recipe", example = "tartiflette", required = true) @RequestBody Recipe recipeCreateRequest) {
	try {
	    recipeHandler.create(recipeCreateRequest);
	    String reponse = "Recipe created with  value : " + recipeCreateRequest.getName();
	    return new ResponseEntity<>(reponse, HttpStatus.OK);
	} catch (NotFoundHandlerException e) {
	    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
    }

    @PutMapping(value = "/update")
    @ApiOperation(value = "Update an Recipe (Ex : tartiflette)")
    @ApiResponses(value = { //
	    @ApiResponse(code = 404, message = "The given argument does not exist"), //
	    @ApiResponse(code = 409, message = "Request error"), //
    })
    public ResponseEntity<String> update(
	    @ApiParam(name = "recipe", type = "RecipeUpdateRequest", value = "recipe", example = "tartiflette", required = true) @RequestBody Recipe recipeUpdateRequest) {
	try {
	    // Recipe
	    recipeHandler.update(recipeUpdateRequest);
	    return new ResponseEntity<>(HttpStatus.OK);
	} catch (NotFoundHandlerException e) {
	    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	} catch (PSQLException e) {
	    return new ResponseEntity<>(HttpStatus.CONFLICT);
	}

    }

    @DeleteMapping(value = "/delete/{id}")
    @ApiOperation(value = "Delete a Recipe from database")
    @ApiResponses(value = { //
	    @ApiResponse(code = 404, message = "Recipe does not exist"), //
	    @ApiResponse(code = 409, message = "Request is impossible") //
    })
    public ResponseEntity<String> delete(
	    @ApiParam(name = "id", type = "Long", value = "id", example = "001", required = true) @PathVariable("id") Long id) {
	try {
	    recipeHandler.delete(id);
	    return new ResponseEntity<String>(HttpStatus.OK);
	} catch (NotFoundHandlerException e) {
	    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	} catch (DataIntegrityViolationException e) {
	    return new ResponseEntity<>(HttpStatus.CONFLICT);
	}
    }

    @GetMapping(value = "/get/{id}")
    @ApiOperation(value = "Get a Recipe by id")
    @ApiResponses(value = { //
	    @ApiResponse(code = 404, message = "Recipe does not exist"), //
    })
    public ResponseEntity<Recipe> get(
	    @ApiParam(name = "id", type = "Long", value = "id", example = "001", required = true) @PathVariable("id") Long id) {
	try {
	    return new ResponseEntity<Recipe>(recipeMapper.makeObjectFromDTO(recipeHandler.get(id)), HttpStatus.OK);
	} catch (NotFoundHandlerException e) {
	    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
    }

    @GetMapping(value = "/get_all")
    @ApiOperation(value = "Get a all Recipe from database")
    @ApiResponses(value = { //
	    @ApiResponse(code = 503, message = "Service unavailable"), //
    })
    public ResponseEntity<List<Recipe>> getAll() {
	try {
	    List<RecipeEntity> listRE = new ArrayList<RecipeEntity>();
	    listRE.addAll(recipeHandler.getAll());
	    List<Recipe> listR = new ArrayList<Recipe>();
	    for (RecipeEntity re : listRE) {
		listR.add(recipeMapper.makeObjectFromDTO(re));
	    }
	    return new ResponseEntity<List<Recipe>>(listR, HttpStatus.OK);
	} catch (HttpServerErrorException e) {
	    return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
	}
    }

    @GetMapping(value = "/select/{key}")
    @ApiOperation(value = "Get a Recipe by key words from database")
    @ApiResponses(value = { //
	    @ApiResponse(code = 503, message = "Service unavailable"), //
    })
    public ResponseEntity<List<Recipe>> select(
	    @ApiParam(name = "key", type = "string", value = "key", example = "Tartiflette", required = true) @PathVariable("key") String key) {
	try {
	    return new ResponseEntity<List<Recipe>>(recipeHandler.selectByKeyWords(key), HttpStatus.OK);
	} catch (HttpServerErrorException e) {
	    return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
	}
    }

    @GetMapping(value = "/getRandomRecipes/{days}")
    @ApiOperation(value = "Get Recipes for a number of day")
    @ApiResponses(value = { //
	    @ApiResponse(code = 503, message = "Service unavailable"), //
    })
    public ResponseEntity<List<Recipe>> selectRandomRecipes(
	    @ApiParam(name = "days", type = "int", value = "days", example = "5", required = true) @PathVariable("days") int days) {
	try {
	    return new ResponseEntity<List<Recipe>>(recipeHandler.selectRandomRecipes(days), HttpStatus.OK);
	} catch (HttpServerErrorException e) {
	    return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
	}
    }

    @GetMapping(value = "/changeRecipe/{id}")
    @ApiOperation(value = "Change Recipe")
    @ApiResponses(value = { //
	    @ApiResponse(code = 404, message = "No recipe available"), //
	    @ApiResponse(code = 503, message = "Service unavailable"), //
    })
    public ResponseEntity<Recipe> selectChangeRecipe(
	    @ApiParam(name = "id", type = "int", value = "id", example = "5", required = true) @PathVariable("id") int id) {
	try {
	    return new ResponseEntity<Recipe>(recipeHandler.selectChangeRecipe(id), HttpStatus.OK);
	} catch (NotFoundHandlerException e) {
	    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	} catch (HttpServerErrorException e) {
	    return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
	}
    }
}
