package fr.cnam.mealsready.manage_recipes.handlers;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.cnam.mealsready.manage_recipes.DAO.InstructionDAO;
import fr.cnam.mealsready.manage_recipes.entities.InstructionEntity;
import fr.cnam.mealsready.manage_recipes.exceptions.NotFoundHandlerException;

@Service
public class InstructionHandler extends CrudHandler<InstructionEntity, InstructionDAO> {

	@Override
	public InstructionEntity update(InstructionEntity instructionEntity) {
		if (!this.dao.existsById(instructionEntity.getId())) {
			throw new NotFoundHandlerException("Instruction does not exist");
		}
		return this.dao.save(instructionEntity);
	}

	@Override
	public void delete(Long id) {
		if (!this.dao.existsById(id)) {
			throw new NotFoundHandlerException("Instruction does not exist");
		}
		this.dao.deleteById(id);
	}

	public void deleteByRecipe(Long id_recipe) {
//		if (this.dao.selectByRecipe(id_recipe).isEmpty()) {
//			throw new NotFoundHandlerException("Recipe does not exist");
//		}
//		this.dao.deleteByRecipe(id_recipe);
		List<InstructionEntity> instructionList = this.dao.selectByRecipe(id_recipe);
		for (InstructionEntity instructionEntity : instructionList) {
			this.dao.delete(instructionEntity);
		}
	}
}
