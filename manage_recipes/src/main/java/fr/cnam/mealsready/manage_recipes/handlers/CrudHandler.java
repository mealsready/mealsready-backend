package fr.cnam.mealsready.manage_recipes.handlers;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import fr.cnam.mealsready.manage_recipes.exceptions.NotFoundHandlerException;

public class CrudHandler<T, U extends JpaRepository<T, Long>> {

    @Autowired
    protected U dao;

    public T create(T o) {
	return this.dao.save(o);
    }

    public T update(T o) {
	return this.dao.save(o);
    }

    public void delete(Long id) {
	this.dao.deleteById(id);
    }

    public T get(Long id) {
	Optional<T> result = this.dao.findById(id);
	if (result.isEmpty()) {
	    String className = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]
		    .toString();
	    throw new NotFoundHandlerException("Id " + id + " not found for the entity " + className);
	}
	return result.get();
    }

    public Boolean existsById(Long id) {
	return this.dao.existsById(id);
    }

    public List<T> getAll() {
	return this.dao.findAll();
    }

    public long count() {
	return this.dao.count();
    }
}
