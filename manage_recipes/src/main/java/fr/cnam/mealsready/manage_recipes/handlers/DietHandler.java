package fr.cnam.mealsready.manage_recipes.handlers;

import org.springframework.stereotype.Service;

import fr.cnam.mealsready.manage_recipes.DAO.DietDAO;
import fr.cnam.mealsready.manage_recipes.entities.DietEntity;
import fr.cnam.mealsready.manage_recipes.exceptions.ItemALreadyExistsException;
import fr.cnam.mealsready.manage_recipes.exceptions.NotFoundHandlerException;

@Service
public class DietHandler extends CrudHandler<DietEntity, DietDAO> {

	public DietEntity create(String name) {		
		if (this.existsByName(name)) {
			throw new ItemALreadyExistsException("Diet with name : " + name + " already exists");
		}
		DietEntity dietEntity = new DietEntity();
		dietEntity.setName(name);
		return this.dao.save(dietEntity);
	}

	@Override
	public DietEntity update(DietEntity dietEntity) {
		if (!this.dao.existsById(dietEntity.getId())) {
			throw new NotFoundHandlerException("Diet does not exist");
		}
		if (this.existsByName(dietEntity.getName())) {
			throw new ItemALreadyExistsException("Diet with name : " + dietEntity.getName() + " already exists");
		}
		return this.dao.save(dietEntity);
	}

	@Override
	public void delete(Long id) {
		if (!this.dao.existsById(id)) {
			throw new NotFoundHandlerException("Diet does not exist");
		}
		this.dao.deleteById(id);
	}

	public boolean existsByName(String name) {
		return this.dao.existsByName(name);
	}
	
}
