package fr.cnam.mealsready.manage_recipes.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.cnam.mealsready.manage_recipes.entities.InstructionEntity;

@Repository

public interface InstructionDAO extends JpaRepository<InstructionEntity, Long> {

	@Query(value = "DELETE FROM instructions WHERE id_recipe = ?1", nativeQuery = true)
	void deleteByRecipe(Long id_recipe);

	@Query(value = "SELECT * FROM instructions WHERE id_recipe = ?1", nativeQuery = true)
	List<InstructionEntity> selectByRecipe(Long id_recipe);
}
