package fr.cnam.mealsready.manage_recipes.tools;

import org.springframework.stereotype.Service;

@Service
public class Tools {
	
	public static String upperCaseFirst(String val) {
		char[] arr = (val.toLowerCase()).toCharArray();
		arr[0] = Character.toUpperCase(arr[0]);
		return new String(arr);
	}
}
