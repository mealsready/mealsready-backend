package fr.cnam.mealsready.manage_recipes.mappers;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.cnam.mealsready.core.APIConnector.AbstractMapper;
import fr.cnam.mealsready.core.domain.Ingredient;
import fr.cnam.mealsready.core.domain.InstructionStep;
import fr.cnam.mealsready.core.domain.Recipe;
import fr.cnam.mealsready.core.domain.RecipeType;
import fr.cnam.mealsready.manage_recipes.entities.DietEntity;
import fr.cnam.mealsready.manage_recipes.entities.IngredientQuantityEntity;
import fr.cnam.mealsready.manage_recipes.entities.InstructionEntity;
import fr.cnam.mealsready.manage_recipes.entities.QualityEntity;
import fr.cnam.mealsready.manage_recipes.entities.RecipeEntity;
import fr.cnam.mealsready.manage_recipes.entities.RecipeTypeEntity;

@Service
public class RecipeMapper extends AbstractMapper<RecipeEntity, Recipe> {

	@Autowired
	InstructionMapper instructionMapper;
	@Autowired
	IngredientMapper ingredientMapper;

	@Override
	public Recipe makeObjectFromDTO(RecipeEntity sourceDTO) {
	    Recipe recipe = new Recipe();
	    autoMap(sourceDTO, recipe);
	    
	    switch (sourceDTO.getRecipeType().getId().intValue()) {
	    case 2:
		recipe.setRecipeType(RecipeType.STARTER);
		break;
	    case 3:
		recipe.setRecipeType(RecipeType.MAINCOURSE);
		break;
	    case 4:
		recipe.setRecipeType(RecipeType.DESSERT);
		break;
	    default:
		recipe.setRecipeType(RecipeType.OTHER);
		break;
	    }
	    
	    recipe.setDiet(new fr.cnam.mealsready.core.domain.Diet());
	    recipe.getDiet().setName(sourceDTO.getDiet().getName());
	    recipe.getDiet().setId(sourceDTO.getDiet().getId());
	    
	    recipe.setQuality(new fr.cnam.mealsready.core.domain.Quality());
	    recipe.getQuality().setName(sourceDTO.getQuality().getName());
	    recipe.getQuality().setId(sourceDTO.getQuality().getId());

	    ArrayList<InstructionStep> instructionSteps = new ArrayList<InstructionStep>();
	    for (InstructionEntity instructionDTO : sourceDTO.getInstructions()) {
		instructionSteps.add(this.instructionMapper.makeObjectFromDTO(instructionDTO));
	    }
	    recipe.setInstructions(instructionSteps);
	    
	    ArrayList<fr.cnam.mealsready.core.domain.Ingredient> ingredientsList = new ArrayList<>();
	    for (IngredientQuantityEntity ingredientQuantityEntity : sourceDTO.getIngredientQuantity()) {
		ingredientsList.add(this.ingredientMapper.makeObjectFromDTO(ingredientQuantityEntity));
	    }
	    recipe.setIngredientsList(ingredientsList);
	    
	    return recipe;
	}

	@Override
	public RecipeEntity makeDTOFromObject(Recipe sourceObject) {
	    RecipeEntity recipeEntity = new RecipeEntity();
	    
	    autoMap(sourceObject, recipeEntity);
	    RecipeTypeEntity recipeTypeEntity = new RecipeTypeEntity();
	    switch (sourceObject.getRecipeType().toValue()) {
	    case 0:
		recipeTypeEntity.setId(2L);
		break;
	    case 1:
		recipeTypeEntity.setId(3L);
		break;
	    case 2:
		recipeTypeEntity.setId(4L);
		break;
	    default:
		recipeTypeEntity.setId(0L);
		break;
	    }
	    recipeEntity.setRecipeType(recipeTypeEntity);
	   
	    recipeEntity.setDiet(new DietEntity());
	    recipeEntity.getDiet().setName(sourceObject.getDiet().getName());
	    recipeEntity.getDiet().setId(sourceObject.getDiet().getId());
	    
	    recipeEntity.setQuality(new QualityEntity());
	    recipeEntity.getQuality().setName(sourceObject.getQuality().getName());
	    recipeEntity.getQuality().setId(sourceObject.getQuality().getId());

	    ArrayList<InstructionEntity> instructions = new ArrayList<InstructionEntity>();
	    for (InstructionStep instructionStep : sourceObject.getInstructions()) {
		instructions.add(this.instructionMapper.makeDTOFromObject(instructionStep));
	    }
	    recipeEntity.setInstructions(instructions);
	    
	    ArrayList<IngredientQuantityEntity> ingredientQuantity = new ArrayList<>();
	    for (Ingredient ingredient : sourceObject.getIngredientsList()) {
		ingredientQuantity.add(this.ingredientMapper.makeDTOFromObject(ingredient));
	    }
	    recipeEntity.setIngredientQuantity(ingredientQuantity);
	    
	    return recipeEntity;
	}

}
