package fr.cnam.mealsready.manage_recipes.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.cnam.mealsready.manage_recipes.entities.IngredientEntity;

@Repository

public interface IngredientDAO extends JpaRepository<IngredientEntity, Long> {

	boolean existsByName(String name);

}
