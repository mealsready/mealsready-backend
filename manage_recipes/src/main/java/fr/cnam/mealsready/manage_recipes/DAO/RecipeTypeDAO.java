package fr.cnam.mealsready.manage_recipes.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.cnam.mealsready.manage_recipes.entities.RecipeTypeEntity;

@Repository

public interface RecipeTypeDAO extends JpaRepository<RecipeTypeEntity, Long> {

	boolean existsByName(String name);

}
