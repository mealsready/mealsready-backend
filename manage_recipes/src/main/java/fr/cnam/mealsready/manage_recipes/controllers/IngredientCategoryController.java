package fr.cnam.mealsready.manage_recipes.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.cnam.mealsready.manage_recipes.entities.IngredientCategoryEntity;
import fr.cnam.mealsready.manage_recipes.exceptions.ItemALreadyExistsException;
import fr.cnam.mealsready.manage_recipes.exceptions.NotFoundHandlerException;
import fr.cnam.mealsready.manage_recipes.handlers.IngredientCategoryHandler;
import fr.cnam.mealsready.manage_recipes.tools.Tools;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Api(tags = {
		"/ingredient_category" }, description = "Ingredients groups (Ex : meat, vegetables, fruits, milk-eggs...)")
@CrossOrigin
@RestController
@RequestMapping("/ingredient_category")
public class IngredientCategoryController {

	@Autowired
	IngredientCategoryHandler ingredientCategoryHandler;

	@PostMapping(value = "/create")
	@ApiOperation(value = "Create a ingredientCategory type Ex : meat")
	@ApiResponses(value = { //
			@ApiResponse(code = 422, message = "The IngredientCategory name already exists and can not be used"), //
	})
	public ResponseEntity<String> create(
			@ApiParam(name = "name", type = "String", value = "IngredientCategory name", example = "vegan", required = true) @RequestBody String name) {
		try {
			IngredientCategoryEntity ingredientCategoryEntity = ingredientCategoryHandler.create(Tools.upperCaseFirst(name));
			String reponse = "IngredientCategory created with  value : " + ingredientCategoryEntity.getName();
			return new ResponseEntity<>(reponse, HttpStatus.OK);
		} catch (ItemALreadyExistsException e) {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@PutMapping(value = "/update")
	@ApiOperation(value = "Change a IngredientCategory name")
	@ApiResponses(value = { //
			@ApiResponse(code = 422, message = "The IngredientCategory name already exists and can not be used"), //
			@ApiResponse(code = 404, message = "IngredientCategory does not exist"), //
	})
	public ResponseEntity<String> update(
			@ApiParam(name = "ingredientCategory", type = "IngredientCategory", value = "ingredientCategory", example = "vegan", required = true) @RequestBody IngredientCategoryEntity ingredientCategoryEntity) {
		try {
			ingredientCategoryEntity.setName(Tools.upperCaseFirst(ingredientCategoryEntity.getName()));
			ingredientCategoryHandler.update(ingredientCategoryEntity);
			String response = "IngredientCategory updated with new value : " + ingredientCategoryEntity.getName();
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (NotFoundHandlerException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (ItemALreadyExistsException e) {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@DeleteMapping(value = "/delete/{id}")
	@ApiOperation(value = "Delete a IngredientCategory from database")
	@ApiResponses(value = { //
			@ApiResponse(code = 404, message = "IngredientCategory does not exist"), //
			@ApiResponse(code = 409, message = "Request is impossible") //
	})
	public ResponseEntity<String> delete(
			@ApiParam(name = "id", type = "Long", value = "id", example = "001", required = true) @PathVariable("id") Long id) {
		try {
			ingredientCategoryHandler.delete(id);
			String response = "IngredientCategory with id : " + id + " has been deleted";
			return new ResponseEntity<String>(response, HttpStatus.OK);
		} catch (NotFoundHandlerException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (DataIntegrityViolationException e) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
	}

	@GetMapping(value = "/get/{id}")
	@ApiOperation(value = "Get a IngredientCategory by id")
	@ApiResponses(value = { //
			@ApiResponse(code = 404, message = "IngredientCategory does not exist"), //
	})
	public ResponseEntity<IngredientCategoryEntity> get(
			@ApiParam(name = "id", type = "Long", value = "id", example = "001", required = true) @PathVariable("id") Long id) {
		try {
			return new ResponseEntity<IngredientCategoryEntity>(ingredientCategoryHandler.get(id), HttpStatus.OK);
		} catch (NotFoundHandlerException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(value = "/get_all")
	@ApiOperation(value = "Get a all IngredientCategorys from database")
	@ApiResponses(value = { //
			@ApiResponse(code = 503, message = "Service unavailable"), //
	})
	public ResponseEntity<List<IngredientCategoryEntity>> getAll() {
		try {
			return new ResponseEntity<List<IngredientCategoryEntity>>(ingredientCategoryHandler.getAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
		}
	}
}
