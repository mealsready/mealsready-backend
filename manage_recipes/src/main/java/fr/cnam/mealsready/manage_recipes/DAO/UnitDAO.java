package fr.cnam.mealsready.manage_recipes.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.cnam.mealsready.manage_recipes.entities.UnitEntity;

@Repository

public interface UnitDAO extends JpaRepository<UnitEntity, Long> {

	boolean existsByName(String name);

}
