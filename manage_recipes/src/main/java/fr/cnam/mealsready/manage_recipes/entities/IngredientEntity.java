package fr.cnam.mealsready.manage_recipes.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import fr.cnam.mealsready.core.APIConnector.APIConnectorDTO;

@Entity
@Table(name = "ingredients")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class IngredientEntity implements APIConnectorDTO {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String name;

	@Column
	private String imageURL;

	@Column
	private Integer unit;

	@ManyToOne
	@JoinColumn(name = "id_category")
	private IngredientCategoryEntity category;

	@OneToMany(mappedBy = "pk.ingredientEntity")
	@JsonIgnore
	private List<IngredientQuantityEntity> ingredientQuantityEntity = new ArrayList<IngredientQuantityEntity>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public Integer getUnit() {
		return unit;
	}

	public void setUnit(Integer unit) {
		this.unit = unit;
	}

	public IngredientCategoryEntity getCategory() {
		return category;
	}

	public void setCategory(IngredientCategoryEntity category) {
		this.category = category;
	}

	public List<IngredientQuantityEntity> getIngredientQuantity() {
		return ingredientQuantityEntity;
	}

	public void setIngredientQuantity(List<IngredientQuantityEntity> ingredientQuantityEntity) {
		this.ingredientQuantityEntity = ingredientQuantityEntity;
	}

}
