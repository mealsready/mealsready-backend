package fr.cnam.mealsready.manage_recipes.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.cnam.mealsready.manage_recipes.entities.QualityEntity;
import fr.cnam.mealsready.manage_recipes.exceptions.ItemALreadyExistsException;
import fr.cnam.mealsready.manage_recipes.exceptions.NotFoundHandlerException;
import fr.cnam.mealsready.manage_recipes.handlers.QualityHandler;
import fr.cnam.mealsready.manage_recipes.tools.Tools;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Api(tags = { "/quality" }, description = "Appreciation of the recipe (Ex : gourmand, balanced...)")
@CrossOrigin
@RestController
@RequestMapping("/quality")
public class QualityController {

	@Autowired
	QualityHandler qualityHandler;

	@PostMapping(value = "/create")
	@ApiOperation(value = "Create a quality type Ex : vegetarian")
	@ApiResponses(value = { //
			@ApiResponse(code = 422, message = "The Quality name already exists and can not be used"), //
	})
	public ResponseEntity<String> create(
			@ApiParam(name = "name", type = "String", value = "Quality name", example = "vegan", required = true) @RequestBody String name) {
		try {
			QualityEntity qualityEntity = qualityHandler.create(Tools.upperCaseFirst(name));
			String reponse = "Quality created with  value : " + qualityEntity.getName();
			return new ResponseEntity<>(reponse, HttpStatus.OK);
		} catch (ItemALreadyExistsException e) {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@PutMapping(value = "/update")
	@ApiOperation(value = "Change a Quality name")
	@ApiResponses(value = { //
			@ApiResponse(code = 422, message = "The Quality name already exists and can not be used"), //
			@ApiResponse(code = 404, message = "Quality does not exist"), //
	})
	public ResponseEntity<String> update(
			@ApiParam(name = "quality", type = "Quality", value = "quality", example = "vegan", required = true) @RequestBody QualityEntity qualityEntity) {
		try {
			qualityEntity.setName(Tools.upperCaseFirst(qualityEntity.getName()));
			qualityHandler.update(qualityEntity);
			String response = "Quality updated with new value : " + qualityEntity.getName();
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (NotFoundHandlerException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (ItemALreadyExistsException e) {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@DeleteMapping(value = "/delete/{id}")
	@ApiOperation(value = "Delete a Quality from database")
	@ApiResponses(value = { //
			@ApiResponse(code = 404, message = "Quality does not exist"), //
			@ApiResponse(code = 409, message = "Request is impossible") //
	})
	public ResponseEntity<String> delete(
			@ApiParam(name = "id", type = "Long", value = "id", example = "001", required = true) @PathVariable("id") Long id) {
		try {
			qualityHandler.delete(id);
			String response = "Quality with id : " + id + " has been deleted";
			return new ResponseEntity<String>(response, HttpStatus.OK);
		} catch (NotFoundHandlerException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (DataIntegrityViolationException e) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
	}

	@GetMapping(value = "/get/{id}")
	@ApiOperation(value = "Get a Quality by id")
	@ApiResponses(value = { //
			@ApiResponse(code = 404, message = "Quality does not exist"), //
	})
	public ResponseEntity<QualityEntity> get(
			@ApiParam(name = "id", type = "Long", value = "id", example = "001", required = true) @PathVariable("id") Long id) {
		try {
			return new ResponseEntity<QualityEntity>(qualityHandler.get(id), HttpStatus.OK);
		} catch (NotFoundHandlerException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(value = "/get_all")
	@ApiOperation(value = "Get a all Qualitys from database")
	@ApiResponses(value = { //
			@ApiResponse(code = 503, message = "Service unavailable"), //
	})
	public ResponseEntity<List<QualityEntity>> getAll() {
		try {
			return new ResponseEntity<List<QualityEntity>>(qualityHandler.getAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
		}
	}
}
