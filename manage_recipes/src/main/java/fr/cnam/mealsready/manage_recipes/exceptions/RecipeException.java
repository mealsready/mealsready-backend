package fr.cnam.mealsready.manage_recipes.exceptions;

public class RecipeException extends Exception {
	private static final long serialVersionUID = 1L;

	public RecipeException() {
		super("Recipe not found");
	}

	public RecipeException(String message) {
		super(message);
	}
}
