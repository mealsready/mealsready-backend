package fr.cnam.mealsready.manage_recipes.handlers;

import org.springframework.stereotype.Service;

import fr.cnam.mealsready.manage_recipes.DAO.UnitDAO;
import fr.cnam.mealsready.manage_recipes.entities.UnitEntity;
import fr.cnam.mealsready.manage_recipes.exceptions.ItemALreadyExistsException;
import fr.cnam.mealsready.manage_recipes.exceptions.NotFoundHandlerException;

@Service
public class UnitHandler extends CrudHandler<UnitEntity, UnitDAO> {

	public UnitEntity create(String name) {
		if (this.existsByName(name)) {
			throw new ItemALreadyExistsException("Unit with name : " + name + " already exists");
		}
		UnitEntity unitEntity = new UnitEntity();
		unitEntity.setName(name);
		return this.dao.save(unitEntity);
	}

	@Override
	public UnitEntity update(UnitEntity unitEntity) {
		if (!this.dao.existsById(unitEntity.getId())) {
			throw new NotFoundHandlerException("Unit does not exist");
		}
		if (this.existsByName(unitEntity.getName())) {
			throw new ItemALreadyExistsException("Unit with name : " + unitEntity.getName() + " already exists");
		}
		return this.dao.save(unitEntity);
	}

	@Override
	public void delete(Long id) {
		if (!this.dao.existsById(id)) {
			throw new NotFoundHandlerException("Unit does not exist");
		}
		this.dao.deleteById(id);
	}

	public boolean existsByName(String name) {
		return this.dao.existsByName(name);
	}

}
