package fr.cnam.mealsready.manage_recipes.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import fr.cnam.mealsready.core.APIConnector.APIConnectorDTO;

@Entity
@Table(name = "instructions")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class InstructionEntity implements APIConnectorDTO {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column
	private int number;
	@Column
	private String text;
	@ManyToOne
	@JoinColumn(name = "id_recipe")
	@JsonIgnore
	private RecipeEntity recipeEntity;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public RecipeEntity getRecipeEntity() {
		return recipeEntity;
	}

	public void setRecipeEntity(RecipeEntity recipeEntity) {
		this.recipeEntity = recipeEntity;
	}
}
