package fr.cnam.mealsready.manage_recipes.handlers;

import org.springframework.stereotype.Service;

import fr.cnam.mealsready.manage_recipes.DAO.IngredientCategoryDAO;
import fr.cnam.mealsready.manage_recipes.entities.IngredientCategoryEntity;
import fr.cnam.mealsready.manage_recipes.exceptions.ItemALreadyExistsException;
import fr.cnam.mealsready.manage_recipes.exceptions.NotFoundHandlerException;

@Service
public class IngredientCategoryHandler extends CrudHandler<IngredientCategoryEntity, IngredientCategoryDAO> {

	public IngredientCategoryEntity create(String name) {
		if (this.existsByName(name)) {
			throw new ItemALreadyExistsException("IngredientCategory with name : " + name + " already exists");
		}
		IngredientCategoryEntity ingredientCategoryEntity = new IngredientCategoryEntity();
		ingredientCategoryEntity.setName(name);
		return this.dao.save(ingredientCategoryEntity);
	}

	@Override
	public IngredientCategoryEntity update(IngredientCategoryEntity ingredientCategoryEntity) {
		if (!this.dao.existsById(ingredientCategoryEntity.getId())) {
			throw new NotFoundHandlerException("IngredientCategory does not exist");
		}
		if (this.existsByName(ingredientCategoryEntity.getName())) {
			throw new ItemALreadyExistsException(
					"IngredientCategory with name : " + ingredientCategoryEntity.getName() + " already exists");
		}
		return this.dao.save(ingredientCategoryEntity);
	}

	@Override
	public void delete(Long id) {
		if (!this.dao.existsById(id)) {
			throw new NotFoundHandlerException("IngredientCategory does not exist");
		}
		this.dao.deleteById(id);
	}

	public boolean existsByName(String name) {
		return this.dao.existsByName(name);
	}

}
