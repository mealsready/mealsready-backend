package fr.cnam.mealsready.manage_recipes.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.cnam.mealsready.manage_recipes.entities.QualityEntity;

@Repository

public interface QualityDAO extends JpaRepository<QualityEntity, Long> {

	boolean existsByName(String name);

}
