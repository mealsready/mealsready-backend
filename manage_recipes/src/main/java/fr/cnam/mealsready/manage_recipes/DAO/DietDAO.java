package fr.cnam.mealsready.manage_recipes.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.cnam.mealsready.manage_recipes.entities.DietEntity;

@Repository

public interface DietDAO extends JpaRepository<DietEntity, Long> {

	boolean existsByName(String name);

}
