package fr.cnam.mealsready.manage_recipes.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import fr.cnam.mealsready.core.APIConnector.APIConnectorDTO;

@Entity
@Table(name = "recipes")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class RecipeEntity implements APIConnectorDTO {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column
	private String name;

	@Column
	private String imageURL;

	@Column
	private int servings; // Nb of guests

	@Column
	private String duration; // Time to make recipe

	@Column
	private String description;

	@ManyToOne
	@JoinColumn(name = "id_recipe_type")
	private RecipeTypeEntity recipeTypeEntity; // Starter, MainCourse or Dessert

	@ManyToOne
	@JoinColumn(name = "id_quality")
	private QualityEntity qualityEntity; // Gourmand, Balanced...

	@ManyToOne
	@JoinColumn(name = "id_diet")
	private DietEntity dietEntity; // Vegetarian, vegan, salt free, sugar free, gluten free

	@OneToMany(mappedBy = "pk.recipeEntity")
	private List<IngredientQuantityEntity> ingredientQuantityEntity = new ArrayList<IngredientQuantityEntity>();

	@OneToMany(targetEntity = InstructionEntity.class, mappedBy = "recipeEntity")
	private List<InstructionEntity> instructionEntities;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public int getServings() {
		return servings;
	}

	public void setServings(int servings) {
		this.servings = servings;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public RecipeTypeEntity getRecipeType() {
		return recipeTypeEntity;
	}

	public void setRecipeType(RecipeTypeEntity recipeTypeEntity) {
		this.recipeTypeEntity = recipeTypeEntity;
	}

	public QualityEntity getQuality() {
		return qualityEntity;
	}

	public void setQuality(QualityEntity qualityEntity) {
		this.qualityEntity = qualityEntity;
	}

	public DietEntity getDiet() {
		return dietEntity;
	}

	public void setDiet(DietEntity dietEntity) {
		this.dietEntity = dietEntity;
	}

	public List<IngredientQuantityEntity> getIngredientQuantity() {
		return ingredientQuantityEntity;
	}

	public void setIngredientQuantity(List<IngredientQuantityEntity> ingredientQuantityEntity) {
		this.ingredientQuantityEntity = ingredientQuantityEntity;
	}

	public List<InstructionEntity> getInstructions() {
		return instructionEntities;
	}

	public void setInstructions(List<InstructionEntity> instructionEntities) {
		this.instructionEntities = instructionEntities;
	}

}
