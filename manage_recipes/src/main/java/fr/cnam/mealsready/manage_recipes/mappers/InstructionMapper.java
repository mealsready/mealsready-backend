package fr.cnam.mealsready.manage_recipes.mappers;

import org.springframework.stereotype.Service;

import fr.cnam.mealsready.core.APIConnector.AbstractMapper;
import fr.cnam.mealsready.core.domain.InstructionStep;
import fr.cnam.mealsready.manage_recipes.entities.InstructionEntity;

@Service
public class InstructionMapper extends AbstractMapper<InstructionEntity, InstructionStep> {

    @Override
    public InstructionStep makeObjectFromDTO(InstructionEntity sourceDTO) {
	InstructionStep instructionStep = new InstructionStep();
	instructionStep.setId(sourceDTO.getId());
	instructionStep.setInstruction(sourceDTO.getText());
	instructionStep.setNumber(""+sourceDTO.getNumber());
	return instructionStep;
    }

    @Override
    public InstructionEntity makeDTOFromObject(InstructionStep sourceObject) {
	InstructionEntity instructionEntity = new InstructionEntity();
	instructionEntity.setId(null);
	instructionEntity.setNumber(Integer.parseInt(sourceObject.getNumber()));
	instructionEntity.setText(sourceObject.getInstruction());
	return instructionEntity;
    }

}
