package fr.cnam.mealsready.manage_recipes.exceptions;

public class HandlerException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public HandlerException() {
	this("Something bad occure using the handler");
    }

    public HandlerException(String message) {
	super(message);
    }

}
