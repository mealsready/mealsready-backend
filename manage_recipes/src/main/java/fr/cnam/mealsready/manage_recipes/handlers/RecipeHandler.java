package fr.cnam.mealsready.manage_recipes.handlers;

import java.util.ArrayList;
import java.util.List;

import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

import fr.cnam.mealsready.core.domain.InstructionStep;
import fr.cnam.mealsready.core.domain.Recipe;
import fr.cnam.mealsready.manage_recipes.DAO.RecipeDAO;
import fr.cnam.mealsready.manage_recipes.entities.IngredientEntity;
import fr.cnam.mealsready.manage_recipes.entities.IngredientQuantityEntity;
import fr.cnam.mealsready.manage_recipes.entities.InstructionEntity;
import fr.cnam.mealsready.manage_recipes.entities.RecipeEntity;
import fr.cnam.mealsready.manage_recipes.exceptions.NotFoundHandlerException;
import fr.cnam.mealsready.manage_recipes.mappers.RecipeMapper;

@Service
public class RecipeHandler extends CrudHandler<RecipeEntity, RecipeDAO> {

	@Autowired
	RecipeMapper recipeMapper;
	@Autowired
	IngredientHandler ingredientHandler;
	@Autowired
	IngredientQuantityHandler ingredientQuantityHandler;
	@Autowired
	InstructionHandler instructionHandler;

	public RecipeEntity create(Recipe recipeCreateRequest) {
		// Set recipe
		RecipeEntity recipeEntity = this.dao.save(recipeMapper.makeDTOFromObject(recipeCreateRequest));
		// Set ingredientQuantity
		if (recipeCreateRequest.getIngredientsList() != null) {
			// Set
			for (fr.cnam.mealsready.core.domain.Ingredient ingredientInRecipeRequest : recipeCreateRequest.getIngredientsList()) {
				setIngredientQuantity(ingredientInRecipeRequest.getId(), (int) ingredientInRecipeRequest.getQuantity(),
						recipeEntity);
			}
		}
		// Set instructions
		if (!recipeCreateRequest.getInstructions().isEmpty()) {
			int count = 0;
			for (InstructionStep step : recipeCreateRequest.getInstructions()) {
			    setInstruction(++count, step.getInstruction(), recipeEntity);
			}
		}
		return recipeEntity;
	}

	public RecipeEntity update(Recipe recipeUpdateRequest) throws PSQLException {
		// Check exist
		if (!this.dao.existsById(recipeUpdateRequest.getId())) {
		    throw new NotFoundHandlerException("Recipe does not exist");
		}
		// Set recipe
		RecipeEntity recipeEntity = this.dao.save(recipeMapper.makeDTOFromObject(recipeUpdateRequest));
		// Set ingredientQuantity
		if (recipeUpdateRequest.getIngredientsList() != null) {
			// Reset
			recipeEntity.setIngredientQuantity(new ArrayList<IngredientQuantityEntity>());
			ingredientQuantityHandler.deleteByRecipe(recipeUpdateRequest.getId());
			// Set
			for (fr.cnam.mealsready.core.domain.Ingredient ingredientInRecipeRequest : recipeUpdateRequest.getIngredientsList()) {
				setIngredientQuantity(ingredientInRecipeRequest.getId(), (int) ingredientInRecipeRequest.getQuantity(),
						recipeEntity);
			}
		}
		// Set instructions
		if (!recipeUpdateRequest.getInstructions().isEmpty()) {
			// Reset
			recipeEntity.setInstructions(new ArrayList<InstructionEntity>());
			instructionHandler.deleteByRecipe(recipeEntity.getId());
			// Set
			int count = 0;
			for (InstructionStep step : recipeUpdateRequest.getInstructions()) {
				setInstruction(++count, step.getInstruction(), recipeEntity);
			}
		}
		return recipeEntity;
	}

	private void setIngredientQuantity(Long id, int quantity, RecipeEntity recipeEntity) {
		if (ingredientHandler.get(id) != null) {
			IngredientQuantityEntity ingredientQuantityEntity = new IngredientQuantityEntity();
			IngredientEntity ingredientEntity = ingredientHandler.get(id);
			ingredientQuantityEntity.setIngredient(ingredientEntity);
			ingredientQuantityEntity.setRecipe(recipeEntity);
			ingredientQuantityEntity.setQuantity(quantity);
			recipeEntity.getIngredientQuantity().add(ingredientQuantityEntity);
			ingredientQuantityHandler.create(ingredientQuantityEntity);
		} else {
			throw new NotFoundHandlerException();
		}
	}

	private void setInstruction(int count, String text, RecipeEntity recipeEntity) {
		InstructionEntity instructionEntity = new InstructionEntity();
		instructionEntity.setNumber(count);
		instructionEntity.setText(text);
		instructionEntity.setRecipeEntity(recipeEntity);
		recipeEntity.getInstructions().add(instructionEntity);
		instructionHandler.create(instructionEntity);
	}

	public boolean existsByName(String name) {
		return this.dao.existsByName(name);
	}

	public List<Recipe> selectByKeyWords(String key) {
		String[] keys = key.split(" ");
		List<RecipeEntity> listRE = new ArrayList<RecipeEntity>();
		switch (keys.length) {
		case 1:
		    listRE.addAll(this.dao.selectByKeyWords(keys[0].toLowerCase(), keys[0].toLowerCase(), keys[0].toLowerCase(),
					keys[0].toLowerCase()));
		    break;
		case 2:
		    listRE.addAll(this.dao.selectByKeyWords(keys[0].toLowerCase(), keys[1].toLowerCase(), keys[0].toLowerCase(),
					keys[0].toLowerCase()));
		    break;
		case 3:
		    listRE.addAll(this.dao.selectByKeyWords(keys[0].toLowerCase(), keys[1].toLowerCase(), keys[2].toLowerCase(),
					keys[0].toLowerCase()));
		    break;
		case 4:
		    listRE.addAll(this.dao.selectByKeyWords(keys[0].toLowerCase(), keys[1].toLowerCase(), keys[2].toLowerCase(),
					keys[3].toLowerCase()));
		    break;
		default:
			throw new HttpServerErrorException(HttpStatus.SERVICE_UNAVAILABLE);
		}
		List<Recipe> listR = new ArrayList<Recipe>();
		for (RecipeEntity re : listRE) {
			listR.add(recipeMapper.makeObjectFromDTO(re));
		}
		return listR;
	}
	
	public List<Recipe> selectRandomRecipes(int number) {
		List<RecipeEntity> listRE = new ArrayList<RecipeEntity>();
		// Starter
		listRE.addAll(this.dao.selectRandomRecipesByType(2, number*2));
		// Main course
		listRE.addAll(this.dao.selectRandomRecipesByType(3, number*2));
		// Dessert
		listRE.addAll(this.dao.selectRandomRecipesByType(4, number*2));
		List<Recipe> listR = new ArrayList<Recipe>();
		for (RecipeEntity re : listRE) {
			listR.add(recipeMapper.makeObjectFromDTO(re));
		}
		return listR;
	}
	
	public Recipe selectChangeRecipe(int id) {
		int typeId = this.dao.getRecipeType(id);
		RecipeEntity recipeEntity = this.dao.selectChangeRecipe(typeId, id);
		if (recipeEntity == null) {
			throw new NotFoundHandlerException("No recipe found for the ID = " + id);
		}
		return recipeMapper.makeObjectFromDTO(recipeEntity);
	}
}
