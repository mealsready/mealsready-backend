package fr.cnam.mealsready.manage_recipes.handlers;

import org.springframework.stereotype.Service;

import fr.cnam.mealsready.manage_recipes.DAO.QualityDAO;
import fr.cnam.mealsready.manage_recipes.entities.QualityEntity;
import fr.cnam.mealsready.manage_recipes.exceptions.ItemALreadyExistsException;
import fr.cnam.mealsready.manage_recipes.exceptions.NotFoundHandlerException;

@Service
public class QualityHandler extends CrudHandler<QualityEntity, QualityDAO> {

	public QualityEntity create(String name) {
		if (this.existsByName(name)) {
			throw new ItemALreadyExistsException("Quality with name : " + name + " already exists");
		}
		QualityEntity qualityEntity = new QualityEntity();
		qualityEntity.setName(name);
		return this.dao.save(qualityEntity);
	}

	@Override
	public QualityEntity update(QualityEntity qualityEntity) {
		if (!this.dao.existsById(qualityEntity.getId())) {
			throw new NotFoundHandlerException("Quality does not exist");
		}
		if (this.existsByName(qualityEntity.getName())) {
			throw new ItemALreadyExistsException("Quality with name : " + qualityEntity.getName() + " already exists");
		}
		return this.dao.save(qualityEntity);
	}

	@Override
	public void delete(Long id) {
		if (!this.dao.existsById(id)) {
			throw new NotFoundHandlerException("Quality does not exist");
		}
		this.dao.deleteById(id);
	}

	public boolean existsByName(String name) {
		return this.dao.existsByName(name);
	}

}
