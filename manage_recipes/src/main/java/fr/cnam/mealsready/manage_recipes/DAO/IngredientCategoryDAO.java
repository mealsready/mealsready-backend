package fr.cnam.mealsready.manage_recipes.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.cnam.mealsready.manage_recipes.entities.IngredientCategoryEntity;

@Repository

public interface IngredientCategoryDAO extends JpaRepository<IngredientCategoryEntity, Long> {

	boolean existsByName(String name);

}
