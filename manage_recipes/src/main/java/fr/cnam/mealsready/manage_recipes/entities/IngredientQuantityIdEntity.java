package fr.cnam.mealsready.manage_recipes.entities;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class IngredientQuantityIdEntity implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "id_ingredient")
	private IngredientEntity ingredientEntity;

	@ManyToOne
	@JoinColumn(name = "id_recipe")
	private RecipeEntity recipeEntity;

	public IngredientEntity getIngredientEntity() {
		return ingredientEntity;
	}

	public void setIngredientEntity(IngredientEntity ingredientEntity) {
		this.ingredientEntity = ingredientEntity;
	}

	public RecipeEntity getRecipeEntity() {
		return recipeEntity;
	}

	public void setRecipeEntity(RecipeEntity recipeEntity) {
		this.recipeEntity = recipeEntity;
	}
}
