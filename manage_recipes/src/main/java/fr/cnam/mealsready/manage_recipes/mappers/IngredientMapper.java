package fr.cnam.mealsready.manage_recipes.mappers;

import org.springframework.stereotype.Service;

import fr.cnam.mealsready.core.APIConnector.AbstractMapper;
import fr.cnam.mealsready.manage_recipes.entities.IngredientEntity;
import fr.cnam.mealsready.manage_recipes.entities.IngredientQuantityEntity;

@Service
public class IngredientMapper extends AbstractMapper<IngredientQuantityEntity, fr.cnam.mealsready.core.domain.Ingredient>{

	@Override
	public fr.cnam.mealsready.core.domain.Ingredient makeObjectFromDTO(IngredientQuantityEntity sourceDTO) {
	    fr.cnam.mealsready.core.domain.Ingredient ingredient = new fr.cnam.mealsready.core.domain.Ingredient();
	    
	    autoMap(sourceDTO.getIngredient(), ingredient);
	    ingredient.setQuantity(sourceDTO.getQuantity());
	    ingredient.setUnit(fr.cnam.mealsready.core.domain.Unit.forValue(sourceDTO.getIngredient().getUnit()));
	    
	    return ingredient;
	}

	@Override
	public IngredientQuantityEntity makeDTOFromObject(fr.cnam.mealsready.core.domain.Ingredient sourceObject) {
	    IngredientQuantityEntity ingredientQuantityEntity = new IngredientQuantityEntity();
	    
	    IngredientEntity ingredientEntity = new IngredientEntity();
	    autoMap(sourceObject, ingredientEntity);
	    ingredientQuantityEntity.setQuantity((int) sourceObject.getQuantity());	    
	    ingredientQuantityEntity.setIngredient(ingredientEntity);
	    
	    return ingredientQuantityEntity;
	}

}
