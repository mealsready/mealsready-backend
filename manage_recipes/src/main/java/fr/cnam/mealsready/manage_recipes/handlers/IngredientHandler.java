package fr.cnam.mealsready.manage_recipes.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.cnam.mealsready.core.domain.Ingredient;
import fr.cnam.mealsready.manage_recipes.DAO.IngredientDAO;
import fr.cnam.mealsready.manage_recipes.entities.IngredientEntity;
import fr.cnam.mealsready.manage_recipes.exceptions.ItemALreadyExistsException;
import fr.cnam.mealsready.manage_recipes.exceptions.NotFoundHandlerException;
import fr.cnam.mealsready.manage_recipes.mappers.IngredientMapper;

@Service
public class IngredientHandler extends CrudHandler<IngredientEntity, IngredientDAO> {

	@Autowired
	IngredientMapper ingredientMapper;

	public IngredientEntity create(Ingredient ingredientCreateRequest) {
		if (this.existsByName(ingredientCreateRequest.getName())) {
			throw new ItemALreadyExistsException(
					"Ingredient with name : " + ingredientCreateRequest.getName() + " already exists");
		}
		return this.dao.save(ingredientMapper.makeDTOFromObject(ingredientCreateRequest).getIngredient());
	}

	public IngredientEntity update(Ingredient ingredientUpdateRequest) {
		if (!this.dao.existsById(ingredientUpdateRequest.getId())) {
			throw new NotFoundHandlerException("Ingredient does not exist");
		}
		if (this.existsByName(ingredientUpdateRequest.getName())) {
			throw new ItemALreadyExistsException(
					"Ingredient with name : " + ingredientUpdateRequest.getName() + " already exists");
		}
		return this.dao.save(ingredientMapper.makeDTOFromObject(ingredientUpdateRequest).getIngredient());
	}

	@Override
	public void delete(Long id) {
		if (!this.dao.existsById(id)) {
			throw new NotFoundHandlerException("Ingredient does not exist");
		}
		this.dao.deleteById(id);
	}

	public boolean existsByName(String name) {
		return this.dao.existsByName(name);
	}

}
