package fr.cnam.mealsready.manage_recipes.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.cnam.mealsready.manage_recipes.entities.RecipeEntity;

@Repository

public interface RecipeDAO extends JpaRepository<RecipeEntity, Long> {

	boolean existsByName(String name);

	@Query(value = "SELECT * FROM recipes r "
			+ "INNER JOIN ingredients_quantities q ON r.id = q.id_recipe "
			+ "INNER JOIN ingredients i ON i.id = q.id_ingredient "
			+ "WHERE (lower(r.name) LIKE (%?1%) "
			+ "OR lower(i.name) LIKE (%?1%)) "
			+ "AND r.id IN ("
			+	"SELECT r.id FROM recipes r "
			+	"INNER JOIN ingredients_quantities q ON r.id = q.id_recipe "
			+	"INNER JOIN ingredients i ON i.id = q.id_ingredient "
			+	"WHERE (lower(r.name) LIKE (%?2%) "
			+	"OR lower(i.name) LIKE (%?2%)) "
			+ 	"AND r.id IN ("
			+   	"SELECT r.id FROM recipes r "
			+		"INNER JOIN ingredients_quantities q ON r.id = q.id_recipe "
			+		"INNER JOIN ingredients i ON i.id = q.id_ingredient "
			+		"WHERE (lower(r.name) LIKE (%?3%) "
			+		"OR lower(i.name) LIKE (%?3%)) "
			+		"AND r.id IN ("
			+   		"SELECT r.id FROM recipes r "
			+			"INNER JOIN ingredients_quantities q ON r.id = q.id_recipe "
			+			"INNER JOIN ingredients i ON i.id = q.id_ingredient "
			+			"WHERE (lower(r.name) LIKE (%?4%) "
			+			"OR lower(i.name) LIKE (%?4%)) "
			+		")"
			+ 	")"
			+ ") ", nativeQuery = true)
	List<RecipeEntity> selectByKeyWords(String key1, String key2, String key3, String key4);
	
	@Query(value = "SELECT * FROM recipes WHERE id_recipe_type = ?1 ORDER BY RANDOM() LIMIT ?2", nativeQuery = true)
	List<RecipeEntity> selectRandomRecipesByType(int type, int number);
	
	@Query(value = "SELECT * FROM recipes WHERE id_recipe_type = ?1 AND id != ?2 ORDER BY RANDOM() LIMIT 1", nativeQuery = true)
	RecipeEntity selectChangeRecipe(int type, int id);
	
	@Query(value = "SELECT id_recipe_type FROM recipes WHERE id = ?1", nativeQuery = true)
	int getRecipeType(int id);
}
