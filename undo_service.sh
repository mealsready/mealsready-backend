#!/bin/bash

previousLoc=$PWD
serviceName=$1

goPreviousLoc() {
	cd "$previousLoc"
}

checkServiceExists() {
	[[ -d $serviceName && -e $serviceName/pom.xml ]] || {
		echo "ERROR : Service $serviceName does not exist"
		echo "Abort"
		exit 1
	}
}

askConfirmation() {
	while [ -z $response ] || [[ $response != 'y' && $response != 'Y' && $response != 'n' && $response != 'N' ]]; do
		echo "Do you really want to remove service $serviceName ?"
		read -p "(y | n) : " response
	done
	[[ $response == 'n' || $response == 'N' ]] && {
		echo "Abort"
		exit 0
	}
}

[[ -z $1 ]] && {
	echo "ERROR Argument missing, usage : COMMAND <serviceName>"
	exit 1
} || {
	[[ $1 = '-h' ]] && {
		echo
		echo '////// HELP \\\\\\'
		echo "Ce script permet de supprimer un service ou une BDD qui vient d'être créée."
		echo "PREREQUIS, aucun commit ne doit être fait entre le moment de la création du service/BDD et l'utilisation de ce script"
		echo "sinon, il ne fonctionnera pas."
		echo "ATTENTION : Ce script est bête, il ne faut pas avoir fait de modifications des fichiers pom.xml, .gitlab-ci.yml ou docker-compose_local.yml,"
		echo "car ce script remet ces 3 fichiers à l'etat du dernier commit de la branche courante."
		echo
		echo "SYNOPSIS :"
		echo "./undo_service.sh <serviceName>"
		echo
		echo "PARAMS :"
		echo "serviceName = le nom du microservice/BDD à supprimer."
		echo
		
		goPreviousLoc
		exit 0
	}

	rootPath=$(dirname $0)
	cd "$rootPath" && {
		checkServiceExists
		askConfirmation
		
		echo "removing service $serviceName..."
		git checkout -- pom.xml && \
		git checkout -- .gitlab-ci.yml && \
		git checkout -- docker-compose_local.yml && \
		git checkout -- docker-compose.yml && \
		rm -rf "./$serviceName/" && \
		echo "Done"
		goPreviousLoc
	}
}
