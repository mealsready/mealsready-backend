#!/bin/bash

previousLoc=$PWD
goPreviousLoc() {
	cd "$previousLoc"
}

[[ $1 = '-h' ]] && {
	echo
	echo '////// HELP \\\\\\'
	echo "Ce script permet de créer une base de donnée permettant d'être dockerisable."
	echo "A l'interieur du dossier généré, se trouve 2 fichiers sql permettant d'initialiser la BDD."
	echo
	echo "SYNOPSIS :"
	echo "./create_db.sh <dbName> <portNumber>"
	echo
	echo "PARAMS :"
	echo "* dbName = le nom de la base de donnée à créer."
	echo
	echo "* portNumber = Le numéo de port à utiliser pour les démarrages en local de la BDD."
	echo
	
	goPreviousLoc
	exit 0
}

[[ -z $1 || -z $2 ]] && {
	echo "ERROR Argument missing, usage : COMMAND <dbName> <portNumber>"
	exit 1
} || {
	rootPath=$(dirname $0)
	cd "$rootPath/archetypes/db-archetype" || {
		echo "ERROR Maven db-archetype not found !"
		goPreviousLoc
		exit 1
	}
	
	mvn --version && {
		mvn clean install && \
		goPreviousLoc && \
		cd "$rootPath" && \
		mvn archetype:generate -B \
			-DarchetypeGroupId=fr.cnam.mealsready.archetypes \
			-DarchetypeArtifactId=db-archetype \
			-DarchetypeVersion=latest \
			-DgroupId=fr.cnam.mealsready \
			-DartifactId=$1 \
			-DportNumber=$2 \
			-DdbName=$1 \
			-DuserName=mealsready \
			-DuserPassword=mealsready \
			-Dversion=1.0-SNAPSHOT && {
			    # Update files
			    perl -v && {
			    	perl add_postgres.pl $1 $2
			    	[[ -d DB/ ]] || mkdir DB
			    	mv -f $1 DB/
			    } || {
			    	echo "WARNING perl not found. You have to update .gitlab-ci, docker-comose_local and pom.xml manually."
				}
			}
	} || {
		echo "ERROR Maven can not be found !"		
	}
	goPreviousLoc
	[[ $? -eq 0 ]] && echo 'Done' || echo 'Error'
}
