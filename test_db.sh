#!/bin/bash

previousLoc=$PWD
goPreviousLoc() {
	cd "$previousLoc"
}

[[ -z $1 ]] && {
	echo "ERROR Argument missing, usage : COMMAND <dbName> [<bindingPort>]"
	exit 1
} || {

	[[ $1 = '-h' ]] && {
		echo
		echo '////// HELP \\\\\\'
		echo "Ce script permet de tester une base de données en buildant l'image puis en démarrant un conteneur docker temporaire."
		echo "SYNOPSIS :"
		echo "./test_db.sh <dbName> [<bindingPort>]"
		echo
		echo "PARAMS :"
		echo "* dbName = le nom de la base de donnée à tester. C'est le même nom que le dossier."
		echo
		echo "* bindingPort (optionnel) = Si non définit docker allouera un port aléatoire pour connecter le port d'écoute 5432 de la BDD."
		echo "Si ce paramètre est définit, c'est le numéro de port fourni qui sera utilisé pour binder le port d'écoute de la BDD."
		echo
		
		goPreviousLoc
		exit 0
	}

	rootPath=$(dirname $0)
	cd "$rootPath/DB/$1" || {
		echo "ERROR database $1 not found !"
		goPreviousLoc
		exit 1
	}
	
	# preparing port binding
	[[ -z $2 ]] && {
		publish='-P'
	} || {
		publish="-p $2:5432"
	}
	
	#preparing database name
	database=$(echo $1 | sed 's/\///g')
	imageName=$(echo "autotest_mealsready_db_$database" |awk '{print tolower($0)}')
	
	docker build -t $imageName ./ && \
	eval "docker run -d --name $imageName $publish $imageName" && \
	echo 'connecting to database...' && {
		x=1
		while [ $x -le 5 ] && [ $? -ne 0 ] || [ $x -eq 1 ]
		do
		  sleep 5
		  com_error=0
		  echo "Connection attempt $x/5"
		  which winpty && {
		    # this line for windows compatibility
		    winpty docker exec -it $imageName psql -U mealsready -d $database || com_error=1
		  } || {
		  	docker exec -it $imageName psql -U mealsready -d $database || com_error=1
		  }
		  x=$(( $x + 1 ))
		done
		[[ $? -ne 0 ]] && echo 'ERROR : not able to connect to db container !' && docker logs $imageName
	} || echo 'ERROR while trying to start up database. Try to start container manually.' && docker logs $imageName
	echo 'Removing container...'
	docker stop $imageName || echo
	docker rm $imageName || echo
	echo
	echo 'removing image...'
	sleep 3
	docker image rm $imageName:latest
	echo
	echo 'Done'
	goPreviousLoc
}
