# Meals-ready-backend
Bienvenue développeur, ci dessous tu trouveras ton guide spirituel.

## Process de développement
Merci de consulter le [WIKI](https://gitlab.com/mealsready/mealsready-backend/-/wikis/home) de ce repository pour avoir le process de développement du projet.

# Get started
### Installation de l'environnement de DEV
Afin de garantir une bonne collaboration il est préférable d'utiliser le même IDE.
L'IDE utilisé est Eclipse.

Les étapes de mise en place de l'environnement de développement se trouvent [ICI](https://gitlab.com/mealsready/mealsready-backend/-/wikis/MealsReady-WIKI/SetUp-dev-env).
 
### Démarrer le projet en local

Le projet utilise Maven comme gestionnaire de dépendance et gestionnaire de build. La configuration de Maven pour le projet se fait dans le fichier  __pom.xml__ . Pour plus d'information sur Maven voir le lien vers la reference de Maven dans la rubrique  __Documentation de référence__  plus bas.

Pour démarrer le projet en local il suffit de passer par le starter de spring boot intégrer à Eclipse grace au plugin précédemment installé :
 * Cliquer sur  __Run->Run configurations...__ , Double-cliquer sur  **Spring Boot App** et choisir le projet dans la liste déroulante.
 * Cliquer sur le bouton search et choisir la seule classe proposée.
 * Cliquer sur  **Apply** puis sur **Run** 

 * NOTE : La manip ci-dessus est à faire qu'une seule fois, pour les prochain lancement l'option de démarrage est disponibles directement via les petites flèches noire près des boutons de lancement.

 OU

 * utiliser le **boot_dashboard** installé lors de la procedure d'installation de l'environnement.
 

# L'arborescence des fichiers

Ci dessous une proposition d'arborescence qu'il convient de suivre. Aucune obligation mais c'est toujours mieux de tous travailler de la même façons.

	\_src/main/java/					<== Contient les sources du projet
	|	\_<main_package>				<== Point d'entrée de l'application et classes d'initialisation (en générale on y touche pas sauf exception).
	|		\_APIConnector/				<== Contient les controleurs responsablent de la comunication avec des API tiers.
	|		|	\_DTO/				<== Structure de données compatible avec les API tiers.
	|		|	\_exceptions/			<== Toutes les exceptions liées aux APIConnector
	|		|	\_mapper/			<== Contient des mapper qui mettent en relation les données reçues des tiers avec la couche domain (model).
	|		|	|	\_AbstractMapper.java	<== Sert d'interface, définit ce que doit faire un mapper et met à disposition des classes l'implémentant, la méthode autoMap(Object, Object), qui va mapper automatiquement les membres identiques entre les deux objets passés en paramètre.
	|		|	\_AbstractAPIConnector.java	<== Met à la disposition des classes héritantes, une méthode get() permettant d'effectuer des requêtes de type HTTP GET facilement.
	|		\_config				<== Contient tous les fichiers de configuration (chargement des données de configuration des fichiers properties).
	|		|	\_BeansFactory.java		<==	Définie tous les beans (au sens de Spring). Les classes définies comme étant des beans sont dans le package specializationBeans.
	|		|	\_ConfigFactory.java		<== Références tous les fichiers de configuration présents dans ce package.
	|		\_controller/				<== Ici se trouvent les controleur de l'API (RestAPI).
	|		|	\_DTO/				<== Contient les structures de données particulières utilisée par les controleurs dans les rares cas où la structure de données du domain n'est pas suffisant.
	|		|_DAO/					<== Contient les DAO (couche de percistance) responsablent de la communication avec la/les BDD.
	|		|_domain/				<== Les java bean (le modèle de données).
	|		|_service/				<== La couche service contient toutes les classes renfermant la logique métier et applicative.
	|		|	\_exceptions/			<== Contient toutes les exceptions concernant les services.
	|		|	\_ResourceLoaderService.java	<== Met à disposition la méthode getResourceAbsolutePathString(String) qui fonctionne comme  la méthode getResource(String) de la classe ResourceLoader de Spring mais prend en compte les contraintes du système de fichier pour les application packagée en .jar. En d'autre terme, c'est un wrapper de ResourceLoader.
	|		\_specializationBeans/			<== Contient tous les beans (au sens de Spring). C'est à dire les classes devant être injectées via le système IoC de Spring et n'étant pas singletonnés comme le sont les services ou les components Spring. Voir beanFactory.
	\_src/mains/resources/
	|	\_static/					<== Contient une page d'accueil static (un bouton pour rediriger sur la page swagger-ui.html)
	|	\_assets/					<== Contient toutes les ressources (css, images, police d'écriture, etc....)
	|	\_templates/					<== Contient tous les templates HTML pour la génération des PDF
	|	|	\_fragments/				<== Contient les fragments (fichiers à inclures) de templates HTML pour la génération des PDF
	|	\_application.properties			<== Configuration global du projet
	|	\_application-dev.properties			<== Configuration pour l'environnement de DEV
	|	\_application-rc.properties			<== Configuration pour l'environnement de RECETTE
	|	\_application-prod.properties			<== Configuration pour l'environnement de PROD
	\_src/test/java/					<== Contient tous les tests unitaires
	|	\_<packages>					<== Regrouper à convenance les tests par theme
	\_ci/                          				<== Contient les DockerFile pour le build des instances à déployer
	\_registry/                     			<== Contient les DockerFile pour builder les images qui seront utilisées par les GitLab runners pour l'intégration continue

# Les diagrammes UML

* [Voir la rubrique architecture du WIKI](https://gitlab.com/mealsready/mealsready-backend/-/wikis/MealsReady-WIKI/Architecture)

# Documentation
Ci-dessous sont listés les documentations officielles des FrameWorks utilisés pour le projet :

### Document de référence
Liens vers les documents de références officiels de Maven et de Spring Boot :

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.7.RELEASE/maven-plugin/)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.2.7.RELEASE/reference/htmlsingle/#using-boot-devtools)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.2.7.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)
* [Swagger2 annotations](https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Annotations#apiresponse)
* [Reference Maven](https://maven.apache.org/pom.html)

### Guides
Quelques exemples concrets :

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)

