package fr.cnam.mealsready.recipes.APIConnector.DTO;

import java.io.Serializable;
import java.util.ArrayList;

import fr.cnam.mealsready.core.APIConnector.APIConnectorDTO;

public class InstructionsDTO implements APIConnectorDTO, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private ArrayList<InstructionStepDTO> steps = new ArrayList<InstructionStepDTO>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<InstructionStepDTO> getSteps() {
		return steps;
	}
	public void setSteps(ArrayList<InstructionStepDTO> steps) {
		this.steps = steps;
	}
}
