package fr.cnam.mealsready.recipes.service;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.cnam.mealsready.core.domain.Day;
import fr.cnam.mealsready.core.domain.Recipe;
import fr.cnam.mealsready.core.domain.RecipeType;
import fr.cnam.mealsready.recipes.APIConnector.RecipesConnector;
import fr.cnam.mealsready.recipes.APIConnector.exceptions.APIConnectorNotAvailableException;
import fr.cnam.mealsready.recipes.APIConnector.exceptions.RecipeConnectorException;
import fr.cnam.mealsready.recipes.APIConnector.exceptions.RecipeConnectorNotFoundException;
import fr.cnam.mealsready.recipes.service.exceptions.RecipesServiceException;
import fr.cnam.mealsready.recipes.service.exceptions.RecipesServiceNotFoundException;
import fr.cnam.mealsready.recipes.service.exceptions.UnvailableServiceException;

@Component
public class RecipesService {

    /** Number of round to find different recipes */
    private final int MAX_TRY = 10;

    /** Logger */
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /** Logger error messages */
    private final String TOO_MUCH_ROUND = "Too much round to find response";

    @Autowired
    RecipesConnector recipesConnetor;

    /**
     * It gets random recipes per number of recipes
     * 
     *
     * @return ArrayList<Recipe> recipes
     * @throws RecipesServiceNotFoundException
     * @throws RecipesServiceException
     * @throws UnvailableServiceException
     */
    public ArrayList<Recipe> getRandomRecipesByNumber(int number)
	    throws RecipesServiceException, RecipesServiceNotFoundException, UnvailableServiceException {
	try {
	    return recipesConnetor.getRandomRecipesByNumber(number);
	} catch (RecipeConnectorNotFoundException e) {
	    throw new RecipesServiceNotFoundException();
	} catch (APIConnectorNotAvailableException e) {
	    throw new UnvailableServiceException("Service unvailable : " + e.getMessage());
	} catch (RecipeConnectorException e) {
	    throw new RecipesServiceException();
	}
    }

    /**
     * It gets random recipes per number of recipes and per tag dish type
     * {"appetizer", "main course", "dessert"}
     * 
     * @param number of recipes
     * @param tag    dish type {"appetizer", "main course", "dessert"}
     * @return Recipes recipes as requested
     * @throws RecipesServiceNotFoundException
     * @throws RecipesServiceException
     * @throws UnvailableServiceException
     */

    public ArrayList<Recipe> getRandomRecipesByNumberAndTagDishType(int number, RecipeType tag)
	    throws RecipesServiceException, RecipesServiceNotFoundException, UnvailableServiceException {
	try {
	    return recipesConnetor.getRandomRecipesByNumberAndTagDishType(number, tag.toValue());
	} catch (RecipeConnectorNotFoundException e) {
	    throw new RecipesServiceNotFoundException();
	} catch (APIConnectorNotAvailableException e) {
	    throw new UnvailableServiceException("Service unvailable : " + e.getMessage());
	} catch (RecipeConnectorException e) {
	    throw new RecipesServiceException();
	}
    }

    /**
     * It gets one week meal plan
     * 
     * @return ArrayList<Day> days with menu list (Lunch + Dinner)
     * @throws RecipesServiceNotFoundException
     * @throws RecipesServiceException
     * @throws UnvailableServiceException
     */

    public ArrayList<Day> getRandomOneWeekPlan()
	    throws RecipesServiceException, RecipesServiceNotFoundException, UnvailableServiceException {
	try {
	    return recipesConnetor.getRandomMealPlanForOneWeek();
	} catch (RecipeConnectorNotFoundException e) {
	    throw new RecipesServiceNotFoundException();
	} catch (APIConnectorNotAvailableException e) {
	    throw new UnvailableServiceException("Service unvailable : " + e.getMessage());
	} catch (RecipeConnectorException e) {
	    throw new RecipesServiceException();
	}
    }

    /**
     * It gets one week meal plan
     * 
     * @return ArrayList<Day> days with menu list (Lunch + Dinner)
     * @throws RecipesServiceNotFoundException
     * @throws RecipesServiceException
     * @throws UnvailableServiceException
     */

    public ArrayList<Day> getRandomMealPlanByDayNumber(int number)
	    throws RecipesServiceException, RecipesServiceNotFoundException, UnvailableServiceException {
	try {
	    return recipesConnetor.getRandomMealPlanByDayNumber(number);
	} catch (RecipeConnectorNotFoundException e) {
	    throw new RecipesServiceNotFoundException();
	} catch (APIConnectorNotAvailableException e) {
	    throw new UnvailableServiceException("Service unvailable : " + e.getMessage());
	} catch (RecipeConnectorException e) {
	    throw new RecipesServiceException();
	}
    }

    /**
     * Exchange recipes for new non duplicated
     * 
     * @param recipesToExchange
     * @return
     * @throws UnvailableServiceException : Too much rounds to find a response
     * @throws RecipesServiceException
     * @throws RecipesServiceNotFoundException
     */
    public ArrayList<Recipe> exchangeRecipes(ArrayList<Recipe> recipesToExchange)
	    throws RecipesServiceNotFoundException, RecipesServiceException, UnvailableServiceException {
	// Initialize arrays of recipes in and out
	ArrayList<Integer> starters = new ArrayList<Integer>();
	ArrayList<Recipe> newStarters = new ArrayList<Recipe>();
	ArrayList<Integer> maincourses = new ArrayList<Integer>();
	ArrayList<Recipe> newMaincourses = new ArrayList<Recipe>();
	ArrayList<Integer> desserts = new ArrayList<Integer>();
	ArrayList<Recipe> newDesserts = new ArrayList<Recipe>();
	// Separate types of recipes
	for (Recipe recipeToExchange : recipesToExchange) {
	    if (recipeToExchange.getRecipeType() == null) {
		continue;
	    }
	    switch (recipeToExchange.getRecipeType()) {
	    case STARTER:
		starters.add(recipeToExchange.getId().intValue());
		break;
	    case MAINCOURSE:
		maincourses.add(recipeToExchange.getId().intValue());
		break;
	    case DESSERT:
		desserts.add(recipeToExchange.getId().intValue());
		break;
	    default:
		break;
	    }
	}
	// Initialize numbers of recipes to change by type
	int nbStartersToExchange = starters.size();
	int nbMaincoursesToExchange = maincourses.size();
	int nbDessertsToExchange = desserts.size();
	// Starters recipes to change
	if (nbStartersToExchange > 0) {
	    int roundCount = 0;
	    while (nbStartersToExchange > 0) {
		// Ask for new recipes...
		newStarters = getRandomRecipesByNumberAndTagDishType(newStarters.size(), RecipeType.STARTER);
		// ... until all recipes are different from origin
		nbStartersToExchange = getNumberOfDuplicatedRecipes(starters, newStarters);
		// Stop loop
		if (roundCount > MAX_TRY) {
		    logger.error(TOO_MUCH_ROUND);
		    throw new UnvailableServiceException();
		}
	    }
	}
	// Maincourses to change
	if (nbMaincoursesToExchange > 0) {
	    while (nbMaincoursesToExchange > 0) {
		int roundCount = 0;
		// Ask for new recipes...
		newMaincourses = getRandomRecipesByNumberAndTagDishType(newMaincourses.size(), RecipeType.MAINCOURSE);
		// ... until all recipes are different from origin
		nbMaincoursesToExchange = getNumberOfDuplicatedRecipes(maincourses, newMaincourses);
		// Stop loop
		if (roundCount > MAX_TRY) {
		    logger.error(TOO_MUCH_ROUND);
		    throw new UnvailableServiceException();
		}
	    }
	}
	// Desserts to change
	if (nbDessertsToExchange > 0) {
	    while (nbDessertsToExchange > 0) {
		int roundCount = 0;
		// Ask for new recipes...
		newDesserts = getRandomRecipesByNumberAndTagDishType(newDesserts.size(), RecipeType.DESSERT);
		// ... until all recipes are different from origin
		nbDessertsToExchange = getNumberOfDuplicatedRecipes(desserts, newDesserts);
		// Stop loop
		if (roundCount > MAX_TRY) {
		    logger.error(TOO_MUCH_ROUND);
		    throw new UnvailableServiceException();
		}
	    }
	}
	// Put starters, maincourses and dessert in a same ArrayList
	recipesToExchange = new ArrayList<Recipe>();
	recipesToExchange.addAll(newStarters);
	recipesToExchange.addAll(newMaincourses);
	recipesToExchange.addAll(newDesserts);

	return recipesToExchange;
    }

    /**
     * Count the number of duplicated recipes
     * 
     * @param recipesIn
     * @param recipesOut
     * @return
     */
    private int getNumberOfDuplicatedRecipes(ArrayList<Integer> recipesIn, ArrayList<Recipe> recipesOut) {
	int count = 0;
	for (Recipe recipeExchanged : recipesOut) {
	    if (recipesIn.contains(recipeExchanged.getId())) {
		count++;
	    }
	}
	return count;
    }
}
