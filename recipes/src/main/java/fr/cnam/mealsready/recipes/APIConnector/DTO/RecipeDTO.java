package fr.cnam.mealsready.recipes.APIConnector.DTO;

import java.io.Serializable;
import java.util.ArrayList;

import fr.cnam.mealsready.core.APIConnector.APIConnectorDTO;

public class RecipeDTO implements APIConnectorDTO, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String title;
	private int servings;
	private String readyInMinutes;
	private String summary;
	private ArrayList<String> dishTypes = new ArrayList<String>();
	private String image;	
	private String imageType;
	private ArrayList<IngredientDTO> extendedIngredients = new ArrayList<IngredientDTO>();
	private ArrayList<InstructionsDTO> analyzedInstructions = new ArrayList<InstructionsDTO>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getServings() {
		return servings;
	}
	public void setServings(int servings) {
		this.servings = servings;
	}
	public String getReadyInMinutes() {
		return readyInMinutes;
	}
	public void setReadyInMinutes(String readyInMinutes) {
		this.readyInMinutes = readyInMinutes;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public ArrayList<String> getDishTypes() {
		return dishTypes;
	}
	public void setDishTypes(ArrayList<String> dishTypes) {
		this.dishTypes = dishTypes;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getImageType() {
		return imageType;
	}
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}
	public ArrayList<IngredientDTO> getExtendedIngredients() {
		return extendedIngredients;
	}
	public void setExtendedIngredients(ArrayList<IngredientDTO> extendedIngredients) {
		this.extendedIngredients = extendedIngredients;
	}
	public ArrayList<InstructionsDTO> getAnalyzedInstructions() {
		return analyzedInstructions;
	}
	public void setAnalyzedInstructions(ArrayList<InstructionsDTO> analyzedInstructions) {
		this.analyzedInstructions = analyzedInstructions;
	}
}



