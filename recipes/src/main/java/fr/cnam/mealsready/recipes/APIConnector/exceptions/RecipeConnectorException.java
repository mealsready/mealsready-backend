package fr.cnam.mealsready.recipes.APIConnector.exceptions;

public class RecipeConnectorException extends Exception {
    private static final long serialVersionUID = 1L;

    public RecipeConnectorException(String message) {
	super(message);
    }
}
