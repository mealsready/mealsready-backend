package fr.cnam.mealsready.recipes.APIConnector.exceptions;

public class RecipeConnectorNotFoundException extends Exception {
    private static final long serialVersionUID = 1L;

    public RecipeConnectorNotFoundException(String message) {
	super(message);
    }
}
