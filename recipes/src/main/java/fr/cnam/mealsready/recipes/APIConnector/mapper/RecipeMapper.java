package fr.cnam.mealsready.recipes.APIConnector.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.cnam.mealsready.core.APIConnector.AbstractMapper;
import fr.cnam.mealsready.core.domain.Ingredient;
import fr.cnam.mealsready.core.domain.InstructionStep;
import fr.cnam.mealsready.core.domain.Recipe;
import fr.cnam.mealsready.core.domain.RecipeType;
import fr.cnam.mealsready.recipes.APIConnector.DTO.IngredientDTO;
import fr.cnam.mealsready.recipes.APIConnector.DTO.InstructionsDTO;
import fr.cnam.mealsready.recipes.APIConnector.DTO.RecipeDTO;

import java.util.ArrayList;
import java.util.Objects;

@Component
public class RecipeMapper extends AbstractMapper<RecipeDTO, Recipe> {
    @Autowired
    IngredientMapper ingredientMapper;
    @Autowired
    InstructionsMapper instructionsMapper;

    @Override
    public Recipe makeObjectFromDTO(RecipeDTO sourceDTO) {
	Objects.requireNonNull(sourceDTO);
	Recipe recipe = new Recipe();
	autoMap(sourceDTO, recipe);
	ArrayList<Ingredient> ingredientList = new ArrayList<Ingredient>();
	ArrayList<InstructionStep> instructionsSteps = new ArrayList<InstructionStep>();

	for (IngredientDTO ingredientDto : sourceDTO.getExtendedIngredients()) {
	    ingredientList.add(ingredientMapper.makeObjectFromDTO(ingredientDto));
	}

	for (InstructionsDTO instructionsDTO : sourceDTO.getAnalyzedInstructions()) {
	    instructionsSteps = instructionsMapper.makeObjectFromDTO(instructionsDTO);
	}

	recipe.setIngredientsList(ingredientList);
	recipe.setName(sourceDTO.getTitle());
	recipe.setDuration(sourceDTO.getReadyInMinutes());
	recipe.setImageURL(sourceDTO.getImage());
	recipe.setDescription(sourceDTO.getSummary());
	recipe.setInstructions(instructionsSteps);
	this.setRecipeType(recipe, sourceDTO);
	return recipe;
    }

    /**
     * It maps the recipe type to RecipeType Enum
     * 
     * @param recipe
     * @param sourceDTO
     */

    private void setRecipeType(Recipe recipe, RecipeDTO sourceDTO) {
	Objects.requireNonNull(sourceDTO);
	ArrayList<String> dishTypes = sourceDTO.getDishTypes();
	if (dishTypes.contains("appetizer") || dishTypes.contains("starter")) {
	    recipe.setRecipeType(RecipeType.STARTER);
	} else if (dishTypes.contains("main course")) {
	    recipe.setRecipeType(RecipeType.MAINCOURSE);
	} else if (dishTypes.contains("dessert")) {
	    recipe.setRecipeType(RecipeType.DESSERT);
	} else
	    recipe.setRecipeType(null);
    }

    @Override
    public RecipeDTO makeDTOFromObject(Recipe sourceObject) {
	RecipeDTO recipeDTO = new RecipeDTO();
	Recipe recipe = (Recipe) sourceObject;
	autoMap(recipe, recipeDTO);
	return recipeDTO;
    }
}
