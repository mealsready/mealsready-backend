package fr.cnam.mealsready.recipes.APIConnector.mapper;

import java.util.Objects;

import org.springframework.stereotype.Component;

import fr.cnam.mealsready.core.APIConnector.AbstractMapper;
import fr.cnam.mealsready.core.domain.Menu;
import fr.cnam.mealsready.recipes.APIConnector.DTO.MenuDTO;

@Component
public class MenuMapper extends AbstractMapper<MenuDTO, Menu> {

	@Override
	public Menu makeObjectFromDTO(MenuDTO sourceDTO) {
		Objects.requireNonNull(sourceDTO);
		Menu menu = new Menu();
		menu.setStarter(sourceDTO.getStarterRecipe());
		menu.setMainCourse(sourceDTO.getMainCourseRecipe());
		menu.setDessert(sourceDTO.getDessertRecipe());
		return menu;
	}

	@Override
	public MenuDTO makeDTOFromObject(Menu sourceObject) {
		// TODO Auto-generated method stub
		return null;
	}
}
