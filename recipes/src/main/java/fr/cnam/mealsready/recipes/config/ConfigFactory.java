package fr.cnam.mealsready.recipes.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConfigFactory {
    private ExternalDataSourceConfiguration externalDataSourceConfiguration;

    @Autowired
    public ConfigFactory(ExternalDataSourceConfiguration externalDataSourceConfiguration) {
	this.externalDataSourceConfiguration = externalDataSourceConfiguration;
    }

    public ExternalDataSourceConfiguration getExternalDataSourceConfiguration() {
	return this.externalDataSourceConfiguration;
    }
}
