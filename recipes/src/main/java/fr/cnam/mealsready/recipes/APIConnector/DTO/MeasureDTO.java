package fr.cnam.mealsready.recipes.APIConnector.DTO;

import java.io.Serializable;

import fr.cnam.mealsready.core.APIConnector.APIConnectorDTO;

public class MeasureDTO implements Serializable, APIConnectorDTO {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private MetricDTO metric;

    public MetricDTO getMetric() {
	return metric;
    }

    public void setMetric(MetricDTO metric) {
	this.metric = metric;
    }
}
