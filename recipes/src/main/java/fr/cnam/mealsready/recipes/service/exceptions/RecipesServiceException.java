package fr.cnam.mealsready.recipes.service.exceptions;

public class RecipesServiceException extends ServiceException {
    private static final long serialVersionUID = 1L;

    public RecipesServiceException() {
	super("Recipes not found");
    }

    public RecipesServiceException(String message) {
	super(message);
    }
}
