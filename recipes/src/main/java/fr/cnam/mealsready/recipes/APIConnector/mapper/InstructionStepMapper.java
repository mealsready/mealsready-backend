package fr.cnam.mealsready.recipes.APIConnector.mapper;

import java.util.Objects;

import org.springframework.stereotype.Component;

import fr.cnam.mealsready.core.APIConnector.AbstractMapper;
import fr.cnam.mealsready.core.domain.InstructionStep;
import fr.cnam.mealsready.recipes.APIConnector.DTO.InstructionStepDTO;

@Component
public class InstructionStepMapper extends AbstractMapper<InstructionStepDTO, InstructionStep>{

	@Override
	public InstructionStep makeObjectFromDTO(InstructionStepDTO sourceDTO) {
		Objects.requireNonNull(sourceDTO);
		InstructionStep instructionStep = new InstructionStep();
		autoMap(sourceDTO, instructionStep);
		instructionStep.setInstruction(sourceDTO.getStep());
		return instructionStep;
	}

	@Override
	public InstructionStepDTO makeDTOFromObject(InstructionStep sourceObject) {
		// TODO Auto-generated method stub
		return null;
	}
}
