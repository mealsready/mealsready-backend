package fr.cnam.mealsready.recipes.APIConnector.exceptions;

public class APIConnectorNotAvailableException extends Exception {
    private static final long serialVersionUID = 1L;

    public APIConnectorNotAvailableException() {
	super("The API is not able to response properly.");
    }

    public APIConnectorNotAvailableException(String message) {
	super(message);
    }
}
