package fr.cnam.mealsready.recipes.APIConnector.DTO;

import java.io.Serializable;

import fr.cnam.mealsready.core.APIConnector.APIConnectorDTO;

public class InstructionStepDTO implements APIConnectorDTO, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String number;
	private String step;
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getStep() {
		return step;
	}
	public void setStep(String step) {
		this.step = step;
	}
}

