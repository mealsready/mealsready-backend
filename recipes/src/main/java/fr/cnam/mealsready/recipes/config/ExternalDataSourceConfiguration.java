package fr.cnam.mealsready.recipes.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("external-datasource")
public class ExternalDataSourceConfiguration {
    private String APIBaseURL;
    private String APIToken;
    private String ImageSpoonacularURL;

    public String getAPIBaseURL() {
	return APIBaseURL;
    }

    public void setAPIBaseURL(String aPIBaseURL) {
	APIBaseURL = aPIBaseURL;
    }

    public String getAPIToken() {
	return APIToken;
    }

    public void setAPIToken(String aPIToken) {
	APIToken = aPIToken;
    }

    public String getImageSpoonacularURL() {
	return ImageSpoonacularURL;
    }

    public void setImageSpoonacularURL(String imageSpoonacularURL) {
	ImageSpoonacularURL = imageSpoonacularURL;
    }
}
