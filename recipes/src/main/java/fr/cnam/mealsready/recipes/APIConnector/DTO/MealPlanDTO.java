package fr.cnam.mealsready.recipes.APIConnector.DTO;

import java.io.Serializable;
import java.util.ArrayList;

import fr.cnam.mealsready.core.APIConnector.APIConnectorDTO;
import fr.cnam.mealsready.core.domain.Recipe;

public class MealPlanDTO implements Serializable, APIConnectorDTO {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private ArrayList<Recipe> starterRecipes;
    private ArrayList<Recipe> mainCourseRecipes;
    private ArrayList<Recipe> dessertRecipes;

    public MealPlanDTO() {
	super();
	this.starterRecipes = new ArrayList<Recipe>();
	this.mainCourseRecipes = new ArrayList<Recipe>();
	this.dessertRecipes = new ArrayList<Recipe>();
    }

    public ArrayList<Recipe> getStarterRecipes() {
	return starterRecipes;
    }

    public void setStarterRecipes(ArrayList<Recipe> starterRecipes) {
	this.starterRecipes = starterRecipes;
    }

    public ArrayList<Recipe> getMainCourseRecipes() {
	return mainCourseRecipes;
    }

    public void setMainCourseRecipes(ArrayList<Recipe> mainCourseRecipes) {
	this.mainCourseRecipes = mainCourseRecipes;
    }

    public ArrayList<Recipe> getDessertRecipes() {
	return dessertRecipes;
    }

    public void setDessertRecipes(ArrayList<Recipe> dessertRecipes) {
	this.dessertRecipes = dessertRecipes;
    }
}
