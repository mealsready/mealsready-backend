package fr.cnam.mealsready.recipes.service.exceptions;

public class UnvailableServiceException extends ServiceException {
    private static final long serialVersionUID = 1L;

    public UnvailableServiceException() {
	super("Service is unvailable for some reasons.");
    }

    public UnvailableServiceException(String message) {
	super(message);
    }
}
