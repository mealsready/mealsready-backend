package fr.cnam.mealsready.recipes.service.exceptions;

public class RecipeServiceException extends ServiceException {
    private static final long serialVersionUID = 1L;

    public RecipeServiceException() {
	super("Recipe not found");
    }

    public RecipeServiceException(String message) {
	super(message);
    }
}
