package fr.cnam.mealsready.recipes.APIConnector.DTO;

import java.io.Serializable;

import fr.cnam.mealsready.core.APIConnector.APIConnectorDTO;
import fr.cnam.mealsready.core.domain.Recipe;

public class MenuDTO implements Serializable,APIConnectorDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Recipe starterRecipe;
	private Recipe mainCourseRecipe;
	private Recipe dessertRecipe ;
	
	public Recipe getStarterRecipe() {
		return starterRecipe;
	}
	public void setStarterRecipe(Recipe starterRecipe) {
		this.starterRecipe = starterRecipe;
	}
	public Recipe getMainCourseRecipe() {
		return mainCourseRecipe;
	}
	public void setMainCourseRecipe(Recipe mainCourseRecipe) {
		this.mainCourseRecipe = mainCourseRecipe;
	}
	public Recipe getDessertRecipe() {
		return dessertRecipe;
	}
	public void setDessertRecipe(Recipe dessertRecipe) {
		this.dessertRecipe = dessertRecipe;
	}
}
