package fr.cnam.mealsready.recipes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.cnam.mealsready.core.domain.Recipe;
import fr.cnam.mealsready.recipes.service.RecipeService;
import fr.cnam.mealsready.recipes.service.exceptions.RecipeServiceException;
import fr.cnam.mealsready.recipes.service.exceptions.RecipeServiceNotFoundException;
import fr.cnam.mealsready.recipes.service.exceptions.UnvailableServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Api(tags = { "/recipes" }, description = "API for CRUD operations on a Recipe")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/recipe")
public class RecipeController {
    @Autowired
    RecipeService recipeService;

    @ApiOperation(value = "Retrieve a recipe by id.")
    @ApiResponses(value = { //
	    @ApiResponse(code = 404, message = "Recipe does not exist"), //
	    @ApiResponse(code = 503, message = "Service temporarily unvailable") //
    })
    @GetMapping(value = "/recipe/{id}")
    public ResponseEntity<Recipe> getRecipeById(
	    @ApiParam(name = "id", type = "Integer", value = "Id of the recipe.", example = "640141", required = true) @PathVariable("id") Integer id) {
	try {
	    return new ResponseEntity<Recipe>(recipeService.getRecipeById(id), HttpStatus.OK);
	} catch (RecipeServiceNotFoundException e) {
	    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	} catch (UnvailableServiceException e) {
	    return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
	} catch (RecipeServiceException e) {
	    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
    }
}
