package fr.cnam.mealsready.recipes.APIConnector.mapper;

import java.util.ArrayList;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.cnam.mealsready.core.APIConnector.AbstractMapper;
import fr.cnam.mealsready.core.domain.InstructionStep;
import fr.cnam.mealsready.recipes.APIConnector.DTO.InstructionStepDTO;
import fr.cnam.mealsready.recipes.APIConnector.DTO.InstructionsDTO;

@Component
public class InstructionsMapper extends AbstractMapper<InstructionsDTO , ArrayList<InstructionStep>> {

	@Autowired
	InstructionStepMapper instructionStepMapper;

	@Override
	public ArrayList<InstructionStep> makeObjectFromDTO(InstructionsDTO sourceDTO) {
		Objects.requireNonNull(sourceDTO);
		ArrayList<InstructionStep>  instructionsSteps = new ArrayList<InstructionStep>();
		ArrayList<InstructionStepDTO>  instructionsStepsDTO = new ArrayList<InstructionStepDTO>();
		instructionsStepsDTO = sourceDTO.getSteps();

		for ( InstructionStepDTO  instructionStepDTO : instructionsStepsDTO){
			instructionsSteps.add(instructionStepMapper.makeObjectFromDTO(instructionStepDTO));
		}
		return instructionsSteps;
	}

	@Override
	public InstructionsDTO makeDTOFromObject(ArrayList<InstructionStep> sourceObject) {
		// TODO Auto-generated method stub
		return null;
	}
}
