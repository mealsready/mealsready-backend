package fr.cnam.mealsready.recipes.APIConnector.DTO;

import java.io.Serializable;
import java.util.ArrayList;

import fr.cnam.mealsready.core.APIConnector.APIConnectorDTO;

public class IngredientDTO implements Serializable, APIConnectorDTO {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private int id;
    private String name;
    private String image;
    private String amount;
    private String unit;
    private String consistency;
    private ArrayList<String> possibleUnits;
    private MeasureDTO measures;

    public MeasureDTO getMeasures() {
	return measures;
    }

    public void setMeasures(MeasureDTO measures) {
	this.measures = measures;
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getImage() {
	return image;
    }

    public void setImage(String image) {
	this.image = image;
    }

    public String getAmount() {
	return amount;
    }

    public void setAmount(String amount) {
	this.amount = amount;
    }

    public String getUnit() {
	return unit;
    }

    public void setUnit(String unit) {
	this.unit = unit;
    }

    public String getConsistency() {
	return consistency;
    }

    public void setConsistency(String consistency) {
	this.consistency = consistency;
    }

    public ArrayList<String> getPossibleUnits() {
	return possibleUnits;
    }

    public void setPossibleUnits(ArrayList<String> possibleUnits) {
	this.possibleUnits = possibleUnits;
    }
}
