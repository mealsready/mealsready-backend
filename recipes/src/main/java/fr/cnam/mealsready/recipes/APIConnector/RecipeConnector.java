package fr.cnam.mealsready.recipes.APIConnector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.cnam.mealsready.core.APIConnector.APIConnectorException;
import fr.cnam.mealsready.core.APIConnector.AbstractAPIConnector;
import fr.cnam.mealsready.core.domain.Recipe;
import fr.cnam.mealsready.recipes.APIConnector.DTO.RecipeDTO;
import fr.cnam.mealsready.recipes.APIConnector.exceptions.APIConnectorNotAvailableException;
import fr.cnam.mealsready.recipes.APIConnector.exceptions.RecipeConnectorException;
import fr.cnam.mealsready.recipes.APIConnector.exceptions.RecipeConnectorNotFoundException;
import fr.cnam.mealsready.recipes.APIConnector.mapper.RecipeMapper;

@Component
public class RecipeConnector extends AbstractAPIConnector<RecipeDTO> {
    private final String URL_GET_RECIPE_BY_ID_URL = "/recipes/{id}/information";
    
    @Autowired
    private RecipeMapper recipeMapper;

    /**
     * 
     * @param id recipe id spoonacular api
     * @return recipe
     * @throws RecipeConnectorNotFoundException  case item not found
     * @throws RecipeConnectorException          default case
     * @throws APIConnectorNotAvailableException
     */
    public Recipe getRecipeById(int id)
	    throws RecipeConnectorNotFoundException, RecipeConnectorException, APIConnectorNotAvailableException {
	Recipe recipe = new Recipe();

	RecipeDTO recipeDTO;
	try {
	    recipeDTO = getAPI(URL_GET_RECIPE_BY_ID_URL, RecipeDTO.class, id);
	    if (recipeDTO != null) {
		recipe = (Recipe) recipeMapper.makeObjectFromDTO(recipeDTO);
	    }
	} catch (APIConnectorException e) {
	    int status = e.getStatusCode().value();

	    switch (status) {
	    case 404:
		throw new RecipeConnectorNotFoundException("recipe with id " + id + "not found");
	    case 402:
		throw new APIConnectorNotAvailableException("the Spoonacular quotas is used up for today !");
	    default:
		throw new RecipeConnectorException(e.getMessage());
	    }
	}
	return recipe;
    }
}
