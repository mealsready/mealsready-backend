package fr.cnam.mealsready.recipes.APIConnector;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.cnam.mealsready.core.APIConnector.APIConnectorException;
import fr.cnam.mealsready.core.APIConnector.AbstractAPIConnector;
import fr.cnam.mealsready.core.domain.Day;
import fr.cnam.mealsready.core.domain.Recipe;
import fr.cnam.mealsready.recipes.APIConnector.DTO.MealPlanDTO;
import fr.cnam.mealsready.recipes.APIConnector.DTO.RecipeDTO;
import fr.cnam.mealsready.recipes.APIConnector.DTO.RecipesDTO;
import fr.cnam.mealsready.recipes.APIConnector.exceptions.APIConnectorNotAvailableException;
import fr.cnam.mealsready.recipes.APIConnector.exceptions.RecipeConnectorException;
import fr.cnam.mealsready.recipes.APIConnector.exceptions.RecipeConnectorNotFoundException;
import fr.cnam.mealsready.recipes.APIConnector.mapper.MealPlanMapper;
import fr.cnam.mealsready.recipes.APIConnector.mapper.RecipeMapper;

@Component
public class RecipesConnector extends AbstractAPIConnector<RecipesDTO> {
    private final String URL_GET_RECIPE_BY_NUMBER_AND_TAG_STARTER = "recipes/random?number={number}&tags=appetizer";
    private final String URL_GET_RECIPE_BY_NUMBER_AND_TAG_MAINCOURE = "recipes/random?number={number}&tags=main course";
    private final String URL_GET_RECIPE_BY_NUMBER_AND_TAG_DESSERT = "recipes/random?number={number}&tags=dessert";
    private final String URL_GET_RECIPE_BY__NUMBER = "recipes/random?number={number}";
    private final int ONEWEEK = 7;

    @Autowired
    private RecipeMapper recipeMapper;
    @Autowired
    private MealPlanMapper oneWeekMealMapper;

    /**
     * It generates random recipes by number of recipes
     * 
     * @param number of recipes
     * @return random ArrayList<Recipe> recipes
     * @throws RecipeConnectorNotFoundException  case item not found
     * @throws RecipeConnectorException          default case
     * @throws APIConnectorNotAvailableException
     */
    public ArrayList<Recipe> getRandomRecipesByNumber(int number)
	    throws RecipeConnectorNotFoundException, RecipeConnectorException, APIConnectorNotAvailableException {
	ArrayList<Recipe> recipes = new ArrayList<Recipe>();
	String url = URL_GET_RECIPE_BY__NUMBER;
	RecipesDTO recipesDTO;
	try {
	    recipesDTO = getAPI(url, RecipesDTO.class, number);

	    if (recipesDTO != null) {
		for (RecipeDTO recipeDTO : recipesDTO.getRecipes()) {
		    Recipe recipe = recipeMapper.makeObjectFromDTO(recipeDTO);
		    recipes.add(recipe);
		}
	    }

	} catch (APIConnectorException e) {
	    int status = e.getStatusCode().value();

	    switch (status) {
	    case 404:
		throw new RecipeConnectorNotFoundException("recipes not found ");
	    case 402:
		throw new APIConnectorNotAvailableException("the Spoonacular quotas is used up for today !");
	    default:
		throw new RecipeConnectorException(e.getMessage());
	    }
	}
	return recipes;
    }

    /**
     * It generates random recipes by number of recipes and a dish type
     * {"appetizer", "main course", "dessert"}
     * 
     * @param number of recipes + dish type {"appetizer", "main course", "dessert"}
     * @return random ArrayList<Recipe> recipes
     * @throws RecipeConnectorNotFoundException  case item not found
     * @throws RecipeConnectorException          default case
     * @throws APIConnectorNotAvailableException
     */
    public ArrayList<Recipe> getRandomRecipesByNumberAndTagDishType(int number, int tag)
	    throws RecipeConnectorNotFoundException, RecipeConnectorException, APIConnectorNotAvailableException {
	ArrayList<Recipe> recipes = new ArrayList<Recipe>();
	String url = this.getRequestUrlByTag(tag);

	RecipesDTO recipesDTO;
	try {
	    recipesDTO = getAPI(url, RecipesDTO.class, number);

	    if (recipesDTO != null) {
		for (RecipeDTO recipeDTO : recipesDTO.getRecipes()) {
		    Recipe recipe = recipeMapper.makeObjectFromDTO(recipeDTO);
		    recipes.add(recipe);
		}
	    }

	} catch (APIConnectorException e) {
	    int status = e.getStatusCode().value();

	    switch (status) {
	    case 404:
		throw new RecipeConnectorNotFoundException("recipe not found");
	    case 402:
		throw new APIConnectorNotAvailableException("the Spoonacular quotas is used up for today !");
	    default:
		throw new RecipeConnectorException(e.getMessage());
	    }
	}
	return recipes;
    }

    /**
     * It generates one week meal plan randomly
     * 
     * @return ArrayList<Day> OneWeekMealPlan
     * @throws RecipeConnectorNotFoundException
     * @throws RecipeConnectorException
     * @throws APIConnectorNotAvailableException
     */
    public ArrayList<Day> getRandomMealPlanForOneWeek()
	    throws RecipeConnectorNotFoundException, RecipeConnectorException, APIConnectorNotAvailableException {
	ArrayList<Day> oneWeekMenus = oneWeekMealMapper.makeObjectFromDTO(this.generateRecipesByDayNumber(ONEWEEK));
	return oneWeekMenus;
    }

    /**
     * generates meal plan randomly for a certain day number
     * 
     * @param number of days
     * @return
     * @throws RecipeConnectorNotFoundException
     * @throws RecipeConnectorException
     * @throws APIConnectorNotAvailableException
     */
    public ArrayList<Day> getRandomMealPlanByDayNumber(int number)
	    throws RecipeConnectorNotFoundException, RecipeConnectorException, APIConnectorNotAvailableException {
	ArrayList<Day> oneWeekMenus = oneWeekMealMapper.makeObjectFromDTO(this.generateRecipesByDayNumber(number));
	return oneWeekMenus;
    }

    /**
     * It generates recipes randomly for a certain number of days
     * 
     * @return MealPlanDTO
     * @throws RecipeConnectorNotFoundException
     * @throws RecipeConnectorException
     * @throws APIConnectorNotAvailableException
     */
    private MealPlanDTO generateRecipesByDayNumber(int number)
	    throws RecipeConnectorNotFoundException, RecipeConnectorException, APIConnectorNotAvailableException {
	MealPlanDTO oneWeekRecipes = new MealPlanDTO();
	number = number * 2;
	oneWeekRecipes.setStarterRecipes(this.getRandomRecipesByNumberAndTagDishType(number, 0));
	oneWeekRecipes.setMainCourseRecipes(this.getRandomRecipesByNumberAndTagDishType(number, 1));
	oneWeekRecipes.setDessertRecipes(this.getRandomRecipesByNumberAndTagDishType(number, 2));
	return oneWeekRecipes;
    }

    /**
     * It calculates request url by tag dish Type
     * 
     * @param tag meal type {"appetizer", "main course", "dessert"}
     * @return String url
     */
    private String getRequestUrlByTag(int tags) {
	String url = "";

	switch (tags) {
	case 0:
	    url = URL_GET_RECIPE_BY_NUMBER_AND_TAG_STARTER;

	    break;
	case 1:
	    url = URL_GET_RECIPE_BY_NUMBER_AND_TAG_MAINCOURE;
	    break;
	case 2:
	    url = URL_GET_RECIPE_BY_NUMBER_AND_TAG_DESSERT;
	    break;
	default:
	}
	return url;
    }
}