
package fr.cnam.mealsready.recipes.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.cnam.mealsready.core.NeedsAuth;
import fr.cnam.mealsready.core.domain.Day;
import fr.cnam.mealsready.core.domain.MealsReadyRolesEnum;
import fr.cnam.mealsready.core.domain.Recipe;
import fr.cnam.mealsready.core.domain.RecipeType;
import fr.cnam.mealsready.recipes.service.RecipesService;
import fr.cnam.mealsready.recipes.service.exceptions.RecipesServiceException;
import fr.cnam.mealsready.recipes.service.exceptions.RecipesServiceNotFoundException;
import fr.cnam.mealsready.recipes.service.exceptions.UnvailableServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Api(tags = { "/recipes" }, description = "API for CRUD operations on Recipes")
@RestController
@RequestMapping("/recipes")
@CrossOrigin(origins = "*")
public class RecipesController {
    @Autowired
    RecipesService recipesService;

    @ApiOperation(value = "Retrieve random recipes per number of recipes.")
    @ApiResponses(value = { //
	    @ApiResponse(code = 404, message = "Recipes not found"), //
	    @ApiResponse(code = 503, message = "Service temporarily unvailable") //
    })
    @GetMapping(value = "/{number}")
    public ResponseEntity<ArrayList<Recipe>> getRandomRecipesByNumber(
	    @ApiParam(name = "number", type = "Integer", value = "The number of random recipes to be returned, it could be {between 1 and 100}.", example = "2", required = true) //
	    @PathVariable("number") Integer number) {
	ResponseEntity<ArrayList<Recipe>> responseEntity = null;
	try {
	    if (number < 1 || number > 100) {
		responseEntity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	    } else {
		responseEntity = new ResponseEntity<ArrayList<Recipe>>(recipesService.getRandomRecipesByNumber(number),
			HttpStatus.OK);
	    }
	} catch (RecipesServiceNotFoundException e) {
	    responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
	} catch (UnvailableServiceException e) {
	    responseEntity = new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
	} catch (RecipesServiceException e) {
	    responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	return responseEntity;
    }

    @ApiOperation(value = "Retrieve a random recipe per number of recipes and a given dish type {\"appetizer\", \"main course\", \"dessert\"}")
    @ApiResponses(value = { //
	    @ApiResponse(code = 404, message = "Recipes not found"), //
	    @ApiResponse(code = 503, message = "Service temporarily unvailable") //
    })
    @GetMapping(value = "/{number}/{tag}")
    public ResponseEntity<ArrayList<Recipe>> getRandomRecipesByNumberAndTagDishType(
	    @ApiParam(name = "number", type = "Integer", value = "The number of random recipes to be returned, it could be {between 1 and 100}.", example = "2", required = true) @PathVariable Integer number,
	    @ApiParam(name = "tag", type = "RecipeType", value = "The type or recipe, it could be { 0 for starter, 1 for main course and 2 for dessert}.", example = "1", required = true) @PathVariable Integer tag) {
	ResponseEntity<ArrayList<Recipe>> responseEntity = null;
	try {
	    if (number < 1 || number > 100) {
		responseEntity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	    } else {
		responseEntity = new ResponseEntity<ArrayList<Recipe>>(
			recipesService.getRandomRecipesByNumberAndTagDishType(number, RecipeType.forValue(tag)), HttpStatus.OK);
	    }
	} catch (RecipesServiceNotFoundException e) {
	    responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
	} catch (UnvailableServiceException e) {
	    responseEntity = new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
	} catch (RecipesServiceException e) {
	    responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	return responseEntity;
    }

    @ApiOperation(value = "Retrieve a random list of recipes for one week.")
    @ApiResponses(value = { //
	    @ApiResponse(code = 404, message = "Recipes not found"), //
	    @ApiResponse(code = 503, message = "Service temporarily unvailable") //
    })
    @GetMapping(value = "/one-week")
    public ResponseEntity<ArrayList<Day>> getRandomOneWeekPlan() {
	ResponseEntity<ArrayList<Day>> responseEntity = null;
	try {
	    responseEntity = new ResponseEntity<ArrayList<Day>>(recipesService.getRandomOneWeekPlan(), HttpStatus.OK);
	} catch (RecipesServiceNotFoundException e) {
	    responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
	} catch (UnvailableServiceException e) {
	    responseEntity = new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
	} catch (RecipesServiceException e) {
	    responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	return responseEntity;
    }

    @ApiOperation(value = "Generate meal plan for a certain number of days")
    @ApiResponses(value = { //
	    @ApiResponse(code = 404, message = "Recipes not found"), //
	    @ApiResponse(code = 503, message = "Service temporarily unvailable") //
    })
    @GetMapping(value = "/days/{number}")
    public ResponseEntity<ArrayList<Day>> getRandomMealPlanPerDayNumber(
	    @ApiParam(name = "number", type = "Integer", value = "The number of days, it could be {between 1 and 30}.", example = "2", required = true) @PathVariable Integer number) {
	ResponseEntity<ArrayList<Day>> responseEntity = null;
	try {
	    if (number < 1 || number > 30) {
		responseEntity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	    } else {
		responseEntity = new ResponseEntity<ArrayList<Day>>(recipesService.getRandomMealPlanByDayNumber(number),
			HttpStatus.OK);
	    }
	} catch (RecipesServiceNotFoundException e) {
	    responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
	} catch (UnvailableServiceException e) {
	    responseEntity = new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
	} catch (RecipesServiceException e) {
	    responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	return responseEntity;
    }

    @ApiOperation(value = "Exchange all given recipes for new ones.")
    @ApiResponses(value = { //
	    @ApiResponse(code = 404, message = "Recipes not found"), //
	    @ApiResponse(code = 503, message = "Service temporarily unvailable") //
    })
    @PostMapping(value = "/exchange")
    public ResponseEntity<ArrayList<Recipe>> exchangeRecepiesByNumberAndTagDishType(
	    @ApiParam(name = "recipesToExchange", type = "Recipe[]", value = "A list of recipes to be exchanged", required = true) @RequestBody ArrayList<Recipe> listRecipies) {
	ResponseEntity<ArrayList<Recipe>> responseEntity = null;
	try {
	    responseEntity = new ResponseEntity<ArrayList<Recipe>>(recipesService.exchangeRecipes(listRecipies), HttpStatus.OK);
	} catch (RecipesServiceNotFoundException e) {
	    responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
	} catch (RecipesServiceException e) {
	    responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	} catch (UnvailableServiceException e) {
	    responseEntity = new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
	}
	return responseEntity;
    }
}
