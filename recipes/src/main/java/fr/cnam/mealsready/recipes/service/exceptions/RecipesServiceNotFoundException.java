package fr.cnam.mealsready.recipes.service.exceptions;

public class RecipesServiceNotFoundException extends RecipesServiceException {
    private static final long serialVersionUID = 1L;

    public RecipesServiceNotFoundException() {
	super("Recipes not found");
    }

    public RecipesServiceNotFoundException(String message) {
	super(message);
    }
}
