package fr.cnam.mealsready.recipes.APIConnector.mapper;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.cnam.mealsready.core.APIConnector.AbstractMapper;
import fr.cnam.mealsready.core.domain.Ingredient;
import fr.cnam.mealsready.core.domain.Unit;
import fr.cnam.mealsready.recipes.APIConnector.DTO.IngredientDTO;
import fr.cnam.mealsready.recipes.config.ConfigFactory;

@Component
public class IngredientMapper extends AbstractMapper<IngredientDTO, Ingredient> {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ConfigFactory configFactory;

    @Override
    public Ingredient makeObjectFromDTO(IngredientDTO sourceDTO) {
	Objects.requireNonNull(sourceDTO);
	Ingredient ingredient = new Ingredient();
	ingredient.setName(sourceDTO.getName());
	String imageUrl = configFactory.getExternalDataSourceConfiguration().getImageSpoonacularURL()
		+ "/cdn/ingredients_250x250/";
	imageUrl += sourceDTO.getImage();
	ingredient.setImageURL(imageUrl);
	ingredient.setQuantity(Double.parseDouble(sourceDTO.getMeasures().getMetric().getAmount()));
	this.setIngredientUnit(ingredient, sourceDTO);
	return ingredient;
    }

    /**
     * It sets Ingredient Units
     * 
     * @param ingredient
     * @param ingredientDTO
     */
    private void setIngredientUnit(Ingredient ingredient, IngredientDTO ingredientDTO) {
	String unit = ingredientDTO.getMeasures().getMetric().getUnitLong();

	switch (unit) {
	case "tbsp":
	    ingredient.setUnit(Unit.tableSpoon);
	    break;
	case "tablespoon":
	    ingredient.setUnit(Unit.tableSpoon);
	    break;
	case "Tbsp":
	    ingredient.setUnit(Unit.tableSpoon);
	    break;
	case "Tb":
	    ingredient.setUnit(Unit.tableSpoon);
	    break;
	case "teaspoon":
	    ingredient.setUnit(Unit.teaSpoon);
	    break;
	case "Tsp":
	    ingredient.setUnit(Unit.teaSpoon);
	    break;
	case "tsp":
	    ingredient.setUnit(Unit.teaSpoon);
	    break;
	case "g":
	    ingredient.setUnit(Unit.g);
	    break;
	case "grams":
	    ingredient.setUnit(Unit.g);
	    break;
	case "pound":
	    ingredient.setUnit(Unit.g);
	    break;
	case "pounds":
	    ingredient.setUnit(Unit.g);
	    break;
	case "lb":
	    ingredient.setUnit(Unit.g);
	    break;
	case "mg":
	    ingredient.setUnit(Unit.mg);
	    break;
	case "kilograms":
	    ingredient.setUnit(Unit.kg);
	    break;
	case "kilogram":
	    ingredient.setUnit(Unit.mg);
	    break;
	case "pinches":
	    ingredient.setUnit(Unit.pinch);
	    break;
	case "pinch":
	    ingredient.setUnit(Unit.pinch);
	    break;
	case "Dashes":
	    ingredient.setUnit(Unit.pinch);
	    break;
	case "handful":
	    ingredient.setUnit(Unit.pinch);
	    break;
	case "serving":
	    ingredient.setUnit(Unit.piece);
	    break;
	case "servings":
	    ingredient.setUnit(Unit.piece);
	    break;
	case "cloves":
	    ingredient.setUnit(Unit.piece);
	    break;
	case "slice":
	    ingredient.setUnit(Unit.piece);
	    break;
	case "piece":
	    ingredient.setUnit(Unit.piece);
	    break;
	case "pieces":
	    ingredient.setUnit(Unit.piece);
	    break;
	case "large":
	    ingredient.setUnit(Unit.piece);
	    break;
	case "small":
	    ingredient.setUnit(Unit.piece);
	    break;
	case "package":
	    ingredient.setUnit(Unit.piece);
	    break;
	case "box":
	    ingredient.setUnit(Unit.piece);
	    break;
	case "packages":
	    ingredient.setUnit(Unit.piece);
	    break;
	case "bag":
	    ingredient.setUnit(Unit.piece);
	    break;
	case "bunch":
	    ingredient.setUnit(Unit.piece);
	    break;
	case "head":
	    ingredient.setUnit(Unit.piece);
	    break;
	case "cube":
	    ingredient.setUnit(Unit.piece);
	    break;
	case "ml":
	    ingredient.setUnit(Unit.mL);
	    break;
	case "milliliters":
	    ingredient.setUnit(Unit.mL);
	    break;
	case "L":
	    ingredient.setUnit(Unit.L);
	    break;
	case "l":
	    ingredient.setUnit(Unit.L);
	    break;
	case "liters":
	    ingredient.setUnit(Unit.L);
	    break;
	case "ounces":
	    this.setOunceOrCup(ingredient, ingredientDTO);
	    break;
	case "ounce":
	    this.setOunceOrCup(ingredient, ingredientDTO);
	    break;
	case "oz":
	    this.setOunceOrCup(ingredient, ingredientDTO);
	    break;
	case "cup":
	    this.setOunceOrCup(ingredient, ingredientDTO);
	    break;
	case "cups":
	    this.setOunceOrCup(ingredient, ingredientDTO);
	    break;
	default:
	    logger.warn("No obvious match for unit '" + unit + "'. Ingredient unit has been set to 'piece'.");
	    ingredient.setUnit(Unit.piece);
	}
    }

    /**
     * It checks consistency and choose measuring unit accordingly
     * 
     * @param ingredient
     * @param ingredientDTO
     */
    private void setOunceOrCup(Ingredient ingredient, IngredientDTO ingredientDTO) {
	if (ingredientDTO.getConsistency() == "liquid") {
	    ingredient.setUnit(Unit.mL);
	} else if (ingredientDTO.getConsistency() == "solid") {
	    ingredient.setUnit(Unit.g);
	}
    }

    @Override
    public IngredientDTO makeDTOFromObject(Ingredient sourceObject) {
	// TODO Auto-generated method stub
	return null;
    }
}
