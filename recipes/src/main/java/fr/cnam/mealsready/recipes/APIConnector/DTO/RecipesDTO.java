package fr.cnam.mealsready.recipes.APIConnector.DTO;

import java.io.Serializable;
import java.util.ArrayList;

import fr.cnam.mealsready.core.APIConnector.APIConnectorDTO;

public class RecipesDTO implements APIConnectorDTO, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<RecipeDTO> recipes = new ArrayList<RecipeDTO>();
	
	public ArrayList<RecipeDTO> getRecipes() {
		return recipes;
	}
	public void setRecipes(ArrayList<RecipeDTO> recipes) {
		this.recipes = recipes;
	}
}
