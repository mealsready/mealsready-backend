package fr.cnam.mealsready.recipes.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.cnam.mealsready.core.domain.Recipe;
import fr.cnam.mealsready.recipes.APIConnector.RecipeConnector;
import fr.cnam.mealsready.recipes.APIConnector.exceptions.APIConnectorNotAvailableException;
import fr.cnam.mealsready.recipes.APIConnector.exceptions.RecipeConnectorException;
import fr.cnam.mealsready.recipes.APIConnector.exceptions.RecipeConnectorNotFoundException;
import fr.cnam.mealsready.recipes.service.exceptions.RecipeServiceException;
import fr.cnam.mealsready.recipes.service.exceptions.RecipeServiceNotFoundException;
import fr.cnam.mealsready.recipes.service.exceptions.UnvailableServiceException;

@Component
public class RecipeService {

    @Autowired
    RecipeConnector recipeConnetor;

    /**
     * It gets a recipe per its id
     * 
     * @param id
     * @return
     * @throws RecipeServiceException
     * @throws RecipeServiceNotFoundException
     * @throws UnvailableServiceException
     */
    public Recipe getRecipeById(int id)
	    throws RecipeServiceNotFoundException, RecipeServiceException, UnvailableServiceException {
	try {
	    return recipeConnetor.getRecipeById(id);
	} catch (RecipeConnectorNotFoundException e) {
	    throw new RecipeServiceNotFoundException();
	} catch (APIConnectorNotAvailableException e) {
	    throw new UnvailableServiceException("Service is unvailable : " + e.getMessage());
	} catch (RecipeConnectorException e) {
	    throw new RecipeServiceException();
	}
    }
}
