package fr.cnam.mealsready.recipes.APIConnector.DTO;

import java.io.Serializable;

import fr.cnam.mealsready.core.APIConnector.APIConnectorDTO;

public class MetricDTO implements Serializable, APIConnectorDTO {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String amount;
    private String unitShort;
    private String unitLong;

    public String getAmount() {
	return amount;
    }

    public void setAmount(String amount) {
	this.amount = amount;
    }

    public String getUnitShort() {
	return unitShort;
    }

    public void setUnitShort(String unitShort) {
	this.unitShort = unitShort;
    }

    public String getUnitLong() {
	return unitLong;
    }

    public void setUnitLong(String unitLong) {
	this.unitLong = unitLong;
    }
}
