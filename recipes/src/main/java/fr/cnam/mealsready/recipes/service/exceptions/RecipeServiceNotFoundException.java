package fr.cnam.mealsready.recipes.service.exceptions;

public class RecipeServiceNotFoundException extends RecipeServiceException {
    private static final long serialVersionUID = 1L;

    public RecipeServiceNotFoundException() {
	super("Recipe not found");
    }

    public RecipeServiceNotFoundException(String message) {
	super(message);
    }
}
