package fr.cnam.mealsready.recipes.APIConnector.mapper;

import java.util.ArrayList;
import java.util.Objects;

import org.springframework.stereotype.Component;

import fr.cnam.mealsready.core.APIConnector.AbstractMapper;
import fr.cnam.mealsready.core.domain.Day;
import fr.cnam.mealsready.core.domain.Menu;
import fr.cnam.mealsready.core.domain.MenuType;
import fr.cnam.mealsready.core.domain.Recipe;
import fr.cnam.mealsready.recipes.APIConnector.DTO.MealPlanDTO;

@Component
public class MealPlanMapper extends AbstractMapper<MealPlanDTO, ArrayList<Day>> {

    @Override
    public ArrayList<Day> makeObjectFromDTO(MealPlanDTO sourceDTO) {
	Objects.requireNonNull(sourceDTO);
	ArrayList<Menu> menusWithMainCourses = this.setMenusMainCourses(sourceDTO.getMainCourseRecipes());
	ArrayList<Menu> menusWithDesserts = this.setDesserts(sourceDTO.getDessertRecipes());
	ArrayList<Menu> fullmenus = this.setMenusStarters(sourceDTO.getStarterRecipes());
	int i = 0;

	for (Menu menu : fullmenus) {
	    menu.setMainCourse(menusWithMainCourses.get(i).getMainCourse());
	    menu.setDessert(menusWithDesserts.get(i).getDessert());
	    i++;
	}
	return this.makeDaysFromMenus(this.setMenusInformation(fullmenus));
    }

    /**
     * It sets starters recipes on each menu
     * 
     * @param recipes
     * @return ArrayList<Menu> menus with starters
     */
    private ArrayList<Menu> setMenusStarters(ArrayList<Recipe> recipes) {
	ArrayList<Menu> menusWithStarters = new ArrayList<Menu>();

	for (Recipe recipe : recipes) {
	    Menu menu = new Menu();

	    while (menu.getStarter() == null) {
		menu.setStarter(recipe);
		menusWithStarters.add(menu);
	    }
	}
	return menusWithStarters;
    }

    /**
     * It sets main courses recipes on each menu
     * 
     * @param recipes
     * @return ArrayList<Menu> menus with main course
     */
    private ArrayList<Menu> setMenusMainCourses(ArrayList<Recipe> recipes) {
	ArrayList<Menu> menusWithMainCourses = new ArrayList<Menu>();

	for (Recipe recipe : recipes) {
	    Menu menu = new Menu();

	    while (menu.getMainCourse() == null) {
		menu.setMainCourse(recipe);
		menusWithMainCourses.add(menu);
	    }
	}
	return menusWithMainCourses;
    }

    /**
     * It sets desserts recipes on each menu
     * 
     * @param recipes
     * @return ArrayList<Menu> menus with desserts
     */
    private ArrayList<Menu> setDesserts(ArrayList<Recipe> recipes) {
	ArrayList<Menu> menusWithDesserts = new ArrayList<Menu>();

	for (Recipe recipe : recipes) {
	    Menu menu = new Menu();

	    while (menu.getDessert() == null) {
		menu.setDessert(recipe);
		menusWithDesserts.add(menu);
	    }
	}
	return menusWithDesserts;
    }

    /**
     * It sets : menu type {BREAKFAST, LUNCH, DINNE} menu name menu number
     * 
     * @param menus ArrayList<Menu>
     * @return ArrayList<Menu> menus with all information
     */
    private ArrayList<Menu> setMenusInformation(ArrayList<Menu> menus) {
	int i = 0;
	int j = 0;

	for (i = 0; i < menus.size() / 2; i++) {
	    menus.get(i).setMenuType(MenuType.LUNCH);
	    menus.get(i).setName("LUNCH DAY " + (i + 1));
	    menus.get(i).setNumber(i + 1);
	}

	for (i = menus.size() / 2; i < menus.size(); i++) {
	    menus.get(i).setMenuType(MenuType.DINNER);
	    menus.get(i).setName("DINNER DAY " + (j + 1));
	    menus.get(i).setNumber(j + 1);
	    j++;
	}

	return menus;
    }

    /**
     * It sets the days list with their menus list
     * 
     * @param menus ArrayList<Menu>
     * @return ArrayList<Menu> days
     */
    private ArrayList<Day> makeDaysFromMenus(ArrayList<Menu> menus) {
	ArrayList<Day> daysWithLunch = this.setLunch(menus);
	ArrayList<Day> daysWithDinner = this.setDinner(menus);
	ArrayList<Day> oneWeekMealPlan = this.setFullMenuDays(daysWithLunch, daysWithDinner);
	return oneWeekMealPlan;
    }

    /**
     * It sets the day whole menu (lunch +dinner)
     * 
     * @param daysWithLunch
     * @param daysWithDinner
     * @return ArrayList<Day> days with lunch and dinner and day number
     */
    private ArrayList<Day> setFullMenuDays(ArrayList<Day> daysWithLunch, ArrayList<Day> daysWithDinner) {
	Objects.requireNonNull(daysWithDinner);
	Objects.requireNonNull(daysWithLunch);

	ArrayList<Day> days = new ArrayList<Day>();

	if (daysWithDinner.size() == daysWithLunch.size()) {

	    for (int i = 0; i < daysWithLunch.size(); i++) {
		ArrayList<Menu> oneDayMenus = new ArrayList<Menu>();
		ArrayList<Menu> oneDayMenuWithLunch = new ArrayList<Menu>();
		ArrayList<Menu> oneDayMenuWithDinner = new ArrayList<Menu>();

		oneDayMenuWithLunch = daysWithLunch.get(i).getMenusList();
		oneDayMenuWithDinner = daysWithDinner.get(i).getMenusList();

		if ((oneDayMenuWithLunch.get(0).getNumber() == oneDayMenuWithDinner.get(0).getNumber())
			&& (oneDayMenuWithLunch.get(0).getMenuType() != oneDayMenuWithDinner.get(0).getMenuType())) {
		    Day day = new Day();
		    oneDayMenus.add(oneDayMenuWithLunch.get(0));
		    oneDayMenus.add(oneDayMenuWithDinner.get(0));

		    day.setMenusList(oneDayMenus);
		    day.setName("DAY " + (i + 1));
		    day.setNumber(i + 1);
		    days.add(day);

		}
	    }
	}
	return days;
    }

    /**
     * It sets lunch menu on a day menus
     * 
     * @param menus
     * @return ArrayList<Day> days with lunch menus
     */
    private ArrayList<Day> setLunch(ArrayList<Menu> menus) {
	Objects.requireNonNull(menus);
	ArrayList<Day> days = new ArrayList<Day>();

	for (Menu menu : menus) {

	    if (menu.getMenuType() == MenuType.LUNCH) {
		Day day = new Day();
		ArrayList<Menu> oneDayMenus = new ArrayList<Menu>();
		oneDayMenus.add(menu);
		day.setMenusList(oneDayMenus);
		days.add(day);
	    }
	}
	return days;
    }

    /**
     * It sets dinner menu on a day menus
     * 
     * @param menus
     * @return ArrayList<Day> days with dinner menus
     */
    private ArrayList<Day> setDinner(ArrayList<Menu> menus) {
	Objects.requireNonNull(menus);
	ArrayList<Day> days = new ArrayList<Day>();

	for (Menu menu : menus) {

	    if (menu.getMenuType() == MenuType.DINNER) {
		Day day = new Day();
		ArrayList<Menu> oneDayMenus = new ArrayList<Menu>();
		oneDayMenus.add(menu);
		day.setMenusList(oneDayMenus);
		days.add(day);
	    }
	}
	return days;
    }

    @Override
    public MealPlanDTO makeDTOFromObject(ArrayList<Day> sourceObject) {
	// TODO Auto-generated method stub
	return null;
    }
}
