package fr.cnam.mealsready.recipes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@EnableDiscoveryClient
@SpringBootApplication(exclude = ErrorMvcAutoConfiguration.class)
@ComponentScan({"fr.cnam.mealsready.recipes", "fr.cnam.mealsready.core"})
public class RecipesServiceApplication {

    public static void main(String[] args) {
    	SpringApplication.run(RecipesServiceApplication.class, args);
    }

}
