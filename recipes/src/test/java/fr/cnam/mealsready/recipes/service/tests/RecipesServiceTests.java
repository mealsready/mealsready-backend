package fr.cnam.mealsready.recipes.service.tests;

import java.util.ArrayList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import fr.cnam.mealsready.core.domain.Ingredient;
import fr.cnam.mealsready.core.domain.InstructionStep;
import fr.cnam.mealsready.core.domain.Recipe;
import fr.cnam.mealsready.core.domain.RecipeType;
import fr.cnam.mealsready.recipes.service.RecipesService;

@SpringBootTest
public class RecipesServiceTests {

	private ArrayList<Recipe> recipesList;
	private ArrayList<Recipe> starterRecipesList;
	private ArrayList<Recipe> maincourseRecipesList;
	private ArrayList<Recipe> dessertRecipesList;

	@Autowired
	private RecipesService recipesService;

	@BeforeEach
	public void init() {

		this.recipesList = new ArrayList<Recipe>();
		this.starterRecipesList = new ArrayList<Recipe>();
		this.maincourseRecipesList = new ArrayList<Recipe>();
		this.dessertRecipesList = new ArrayList<Recipe>();

		Recipe recipe1 = new Recipe();
		recipe1.setId(1L);
		recipe1.setName("recette1");
		recipe1.setImageURL("image1");
		recipe1.setServings(4);
		recipe1.setDuration("durée");
		recipe1.setDescription("description");
		recipe1.setRecipeType(RecipeType.STARTER);
		recipe1.setInstructions(new ArrayList<InstructionStep>());
		recipe1.setIngredientsList(new ArrayList<Ingredient>());
		this.recipesList.add(recipe1);

		Recipe recipe2 = new Recipe();
		recipe2.setId(2L);
		recipe2.setName("recette2");
		recipe2.setImageURL("image2");
		recipe2.setServings(4);
		recipe2.setDuration("durée");
		recipe2.setDescription("description");
		recipe2.setRecipeType(RecipeType.MAINCOURSE);
		recipe2.setInstructions(new ArrayList<InstructionStep>());
		recipe2.setIngredientsList(new ArrayList<Ingredient>());
		this.recipesList.add(recipe2);

		Recipe recipe3 = new Recipe();
		recipe3.setId(3L);
		recipe3.setName("recette3");
		recipe3.setImageURL("image3");
		recipe3.setServings(4);
		recipe3.setDuration("durée");
		recipe3.setDescription("description");
		recipe3.setRecipeType(RecipeType.DESSERT);
		recipe3.setInstructions(new ArrayList<InstructionStep>());
		recipe3.setIngredientsList(new ArrayList<Ingredient>());
		this.recipesList.add(recipe3);

		this.starterRecipesList.add(recipe1);
		this.maincourseRecipesList.add(recipe2);
		this.dessertRecipesList.add(recipe3);
	}

	@Test
	public void shouldReturnDifferentListRecipes() throws Exception {

		ArrayList<Recipe> newListRecipes = new ArrayList<Recipe>();
		newListRecipes = this.recipesService.exchangeRecipes(this.recipesList);
		Assertions.assertNotNull(newListRecipes);
		// Assertions.assertEquals(1, this.recipe.getId());
		Assertions.assertTrue(!newListRecipes.equals(this.recipesList));
		Assertions.assertEquals(newListRecipes.size(), this.recipesList.size());

	}

	@Test
	public void shouldReturnDifferentIdRecipes() throws Exception {

		ArrayList<Recipe> newListRecipesStarter = new ArrayList<Recipe>();
		newListRecipesStarter = this.recipesService.exchangeRecipes(this.starterRecipesList);
		ArrayList<Recipe> newListRecipesMaincourse = new ArrayList<Recipe>();
		newListRecipesMaincourse = this.recipesService.exchangeRecipes(this.maincourseRecipesList);
		ArrayList<Recipe> newListRecipesDessert = new ArrayList<Recipe>();
		newListRecipesDessert = this.recipesService.exchangeRecipes(this.dessertRecipesList);

		Assertions.assertTrue(newListRecipesStarter.get(0).getId() != this.starterRecipesList.get(0).getId());
		Assertions.assertTrue(newListRecipesMaincourse.get(0).getId() != this.maincourseRecipesList.get(0).getId());
		Assertions.assertTrue(newListRecipesDessert.get(0).getId() != this.dessertRecipesList.get(0).getId());
	}

	@Test
	public void shouldReturnSameTypeRecipes() throws Exception {

		ArrayList<Recipe> newListRecipesStarter = new ArrayList<Recipe>();
		newListRecipesStarter = this.recipesService.exchangeRecipes(this.starterRecipesList);
		ArrayList<Recipe> newListRecipesMaincourse = new ArrayList<Recipe>();
		newListRecipesMaincourse = this.recipesService.exchangeRecipes(this.maincourseRecipesList);
		ArrayList<Recipe> newListRecipesDessert = new ArrayList<Recipe>();
		newListRecipesDessert = this.recipesService.exchangeRecipes(this.dessertRecipesList);

		Assertions.assertEquals(newListRecipesStarter.get(0).getRecipeType(),
				this.starterRecipesList.get(0).getRecipeType());
		Assertions.assertEquals(newListRecipesMaincourse.get(0).getRecipeType(),
				this.maincourseRecipesList.get(0).getRecipeType());
		Assertions.assertEquals(newListRecipesDessert.get(0).getRecipeType(),
				this.dessertRecipesList.get(0).getRecipeType());
	}
}
