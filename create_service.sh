#!/bin/bash

previousLoc=$PWD
goPreviousLoc() {
	cd "$previousLoc"
}

[[ $1 = '-h' ]] && {
	echo
	echo '////// HELP \\\\\\'
	echo "Ce script permet de créer un nouveau micro-service."
	echo "Un projet sera généré incluant tout le nécessaire pour pouvoir utiliser le core"
	echo "et incluant également le dockerfile et le fichier de conf pour le pipeline d'intégration continue."
	echo
	echo "SYNOPSIS :"
	echo "./create_service.sh <serviceName> <portNumber>"
	echo
	echo "PARAMS :"
	echo "* serviceName = le nom du microservice à créer."
	echo
	echo "* portNumber = Le numéo de port à utiliser pour les démarrages en local du micro-service."
	echo
	
	goPreviousLoc
	exit 0
}

[[ -z $1 || -z $2 ]] && {
	echo "ERROR Argument missing, usage : COMMAND <serviceName> <portNumber>"
	exit 1
} || {
	rootPath=$(dirname $0)
	cd "$rootPath/archetypes/service-archetype" || {
		echo "ERROR Maven service-archetype not found !"
		goPreviousLoc
		exit 1
	}
	
	mvn --version && {
		mvn clean install && \
		goPreviousLoc && \
		cd "$rootPath" && \
		mvn archetype:generate -B \
			-DarchetypeGroupId=fr.cnam.mealsready.archetypes \
			-DarchetypeArtifactId=service-archetype \
			-DarchetypeVersion=latest \
			-DgroupId=fr.cnam.mealsready \
			-DartifactId=$1 \
			-DportNumber=$2 \
			-DmainPackage=fr.cnam.mealsready.$1 \
			-Dversion=1.0-SNAPSHOT && {
			    # Update files
			    perl -v && {
			    	perl update_files.pl $1 $2
			    } || {
			    	echo "Procedure ended with WARNING. You have to update .gitlab-ci, docker-compose_local.yml, docker-compose.yml and pom.xml manually if it has not be done. Refer to the WIKI."
				}
			}
	} || {
		echo "ERROR Maven can not be found !"		
	}
	goPreviousLoc
	
	[[ $? -eq 0 ]] && echo 'Done' || echo 'Error'
}
