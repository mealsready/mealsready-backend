package fr.cnam.mealsready.authentification.configuration;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * @author khaou
 *
 */
@Configuration
@ConfigurationProperties(prefix = "keycloak")
public class KeycloakConfiugartion {
    
    @Autowired
    private Environment env;

    private String realm;
    private String serverURL;
    private String grantType;
    private String mealsrearyClientId;
    private String mealsreadyClientSecret;
    private String keycloakAdminClient;
    private String keycloakAdminPassword;
    private String keycloakAdminClientId;
    private String keycloakAdminClientSecret;
    private String tokenURL;
    private String validateTokenURL;
    private String usersURL;
    private String userLogoutURL;
    private String userResetPassword;
    
    @PostConstruct
    private void init() {
	this.setRealm(this.env.getProperty("keycloak.realm"));
	this.setServerURL(this.env.getProperty("keycloak.auth-server-url"));
	this.setGrantType(this.env.getProperty("keycloak.granttype"));
	this.setMealsrearyClientId(this.env.getProperty("keycloak.mealsready.client.id"));
	this.setMealsreadyClientSecret(this.env.getProperty("keycloak.mealsready.client.secret"));
	this.setKeycloakAdminClientId(this.env.getProperty("keycloak.admin.client.id"));
	this.setKeycloakAdminPassword(this.env.getProperty("keycloak.admin.client.password"));
	this.setKeycloakAdminClient(this.env.getProperty("keycloak.admin.client.user.name"));
	this.setKeycloakAdminClientSecret(this.env.getProperty("keycloak.admin.client.secret"));
	this.setTokenURL(this.env.getProperty("keycloak.token-url"));
	this.setValidateTokenURL(this.env.getProperty("keycloak.token.validate"));
	this.setUserResetPassword(this.env.getProperty("keycloak.password.reset"));
	this.setUserLogoutURL(this.env.getProperty("keycloak.logout"));
	this.setUsersURL(this.env.getProperty("keycloak.users.url"));
    }

    public Environment getEnv() {
        return env;
    }

    public void setEnv(Environment env) {
        this.env = env;
    }

    public String getRealm() {
        return realm;
    }

    public void setRealm(String realm) {
        this.realm = realm;
    }

    public String getServerURL() {
        return serverURL;
    }

    public void setServerURL(String serverURL) {
        this.serverURL = serverURL;
    }

    public String getGrantType() {
        return grantType;
    }

    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    public String getMealsrearyClientId() {
        return mealsrearyClientId;
    }

    public void setMealsrearyClientId(String mealsrearyClientId) {
        this.mealsrearyClientId = mealsrearyClientId;
    }

    public String getMealsreadyClientSecret() {
        return mealsreadyClientSecret;
    }

    public void setMealsreadyClientSecret(String mealsreadyClientSecret) {
        this.mealsreadyClientSecret = mealsreadyClientSecret;
    }

    public String getKeycloakAdminClient() {
        return keycloakAdminClient;
    }

    public void setKeycloakAdminClient(String keycloakAdminClient) {
        this.keycloakAdminClient = keycloakAdminClient;
    }

    public String getKeycloakAdminPassword() {
        return keycloakAdminPassword;
    }

    public void setKeycloakAdminPassword(String keycloakAdminPassword) {
        this.keycloakAdminPassword = keycloakAdminPassword;
    }

    public String getKeycloakAdminClientId() {
        return keycloakAdminClientId;
    }

    public void setKeycloakAdminClientId(String keycloakAdminClientId) {
        this.keycloakAdminClientId = keycloakAdminClientId;
    }

    public String getKeycloakAdminClientSecret() {
        return keycloakAdminClientSecret;
    }

    public void setKeycloakAdminClientSecret(String keycloakAdminClientSecret) {
        this.keycloakAdminClientSecret = keycloakAdminClientSecret;
    }

    public String getTokenURL() {
        return tokenURL;
    }

    public void setTokenURL(String tokenURL) {
        this.tokenURL = tokenURL;
    }

    public String getValidateTokenURL() {
        return validateTokenURL;
    }

    public void setValidateTokenURL(String validateTokenURL) {
        this.validateTokenURL = validateTokenURL;
    }

    public String getUsersURL() {
        return usersURL;
    }

    public void setUsersURL(String usersURL) {
        this.usersURL = usersURL;
    }

    public String getUserLogoutURL() {
        return userLogoutURL;
    }

    public void setUserLogoutURL(String userLogoutURL) {
        this.userLogoutURL = userLogoutURL;
    }

    public String getUserResetPassword() {
        return userResetPassword;
    }

    public void setUserResetPassword(String userResetPassword) {
        this.userResetPassword = userResetPassword;
    }
    
}
