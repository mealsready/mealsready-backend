package fr.cnam.mealsready.authentification.APIConnectors.DTO;

import java.util.ArrayList;

import fr.cnam.mealsready.core.APIConnector.APIConnectorDTO;

public class RealmAccess implements APIConnectorDTO{
    
    private ArrayList<String> roles;

    public ArrayList<String> getRoles() {
        return roles;
    }
    
    public void setRoles(ArrayList<String> roles) {
        this.roles = roles;
    }
  
}
