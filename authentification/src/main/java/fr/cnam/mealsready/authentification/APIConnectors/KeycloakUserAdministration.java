package fr.cnam.mealsready.authentification.APIConnectors;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import fr.cnam.mealsready.authentification.APIConnectors.DTO.UserDTO;
import fr.cnam.mealsready.authentification.configuration.ConfigurationFactory;
import fr.cnam.mealsready.authentification.configuration.KeycloakConfiugartion;
import fr.cnam.mealsready.core.APIConnector.APIConnectorException;
import fr.cnam.mealsready.core.APIConnector.AbstractAPIConnector;

@Component
public class KeycloakUserAdministration extends AbstractAPIConnector<UserDTO> {

     @Autowired
     KeycloakAuthentification keycloakAuthentification;

     @Autowired
     ConfigurationFactory configurationFactory;

     KeycloakConfiugartion keycloakConfiugartion;

     @PostConstruct
     private void init(){
	 this.keycloakConfiugartion = this.configurationFactory.getKeycloakConfiguration();
     }

     public int registerUser(UserDTO user) throws APIConnectorException {
	 // Admin token
	 String JWT = keycloakAuthentification.getAdminToken();
	 // Request header 
	 Map<String, String> headers = new HashMap<String, String>();
	 headers.put("Content-Type", "application/json");
	 headers.put(HttpHeaders.AUTHORIZATION, "bearer " +JWT);

	 int status = post(keycloakConfiugartion.getUsersURL(), user, headers);
	 return status;
     }

     public UserDTO getUserById(UUID userId) throws APIConnectorException { 
	 // Admin token
	 String JWT = keycloakAuthentification.getAdminToken();
	 // Request header 
	 Map<String, String> headers = new HashMap<String, String>();
	 headers.put("Content-Type", "application/json");
	 headers.put(HttpHeaders.AUTHORIZATION, "bearer " +JWT);

	 UserDTO user = get(keycloakConfiugartion.getUsersURL() + "/{id}", headers, UserDTO.class, userId.toString());
	 return user;
     }

     public List<UserDTO> getUserByUserName(String username) throws APIConnectorException { 
	 // Admin token
	 String JWT = keycloakAuthentification.getAdminToken();
	 // Request header 
	 Map<String, String> headers = new HashMap<String, String>();
	 headers.put("Content-Type", "application/json");
	 headers.put(HttpHeaders.AUTHORIZATION, "bearer " +JWT);

	 Gson gson = new Gson();
	 String json = (String) get(keycloakConfiugartion.getUsersURL() + "?username={username}", headers, username);
	 List<UserDTO> users = Arrays.asList(gson.fromJson(json, UserDTO[].class));

	 return users;
     }

     public List<UserDTO> getUserByEmail(String email) throws APIConnectorException { 
	 // Admin token
	 String JWT = keycloakAuthentification.getAdminToken();
	 // Request header 
	 Map<String, String> headers = new HashMap<String, String>();
	 headers.put("Content-Type", "application/json");
	 headers.put(HttpHeaders.AUTHORIZATION, "bearer " +JWT);

	 Gson gson = new Gson();
	 String json = (String) get(keycloakConfiugartion.getUsersURL() + "?email={email}", headers, email);
	 List<UserDTO> users = Arrays.asList(gson.fromJson(json, UserDTO[].class));     

	 return users;
     }

     public int updateUser(UUID userId, UserDTO user) throws APIConnectorException {
	 // Admin token
	 String JWT = keycloakAuthentification.getAdminToken();
	 // Request header 
	 Map<String, String> headers = new HashMap<String, String>();
	 headers.put("Content-Type", "application/json");
	 headers.put(HttpHeaders.AUTHORIZATION, "bearer " +JWT);

	 return put(keycloakConfiugartion.getUsersURL() + "/{id}", headers, user, userId.toString());
     }


     public int logoutByUserId(UUID userId) throws APIConnectorException {
	 // Admin token
	 String JWT = keycloakAuthentification.getAdminToken();
	 // Request header 
	 Map<String, String> headers = new HashMap<String, String>();
	 headers.put("Content-Type", "application/json");
	 headers.put(HttpHeaders.AUTHORIZATION, "bearer " +JWT);

	 int status = post(keycloakConfiugartion.getUserLogoutURL(), userId , headers,  userId.toString());
	 return status;
     }
}




