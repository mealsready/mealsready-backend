package fr.cnam.mealsready.authentification.exceptions;

public class UserNotFoundException extends UserServiceException {
    
    private static final long serialVersionUID = 1L;

    public UserNotFoundException() {
	super("User does not exist");
    }
    
    public UserNotFoundException(String message) {
	super(message);
    }
    
}
