package fr.cnam.mealsready.authentification.exceptions;

public class TokenNotValidException extends UserServiceException {
    
    private static final long serialVersionUID = 1L;

    public TokenNotValidException() {
	super("Token not valid");
    }

    public TokenNotValidException(String message) {
	super(message);
    }
    
}
