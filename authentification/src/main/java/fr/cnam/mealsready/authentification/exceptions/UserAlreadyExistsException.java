package fr.cnam.mealsready.authentification.exceptions;

public class UserAlreadyExistsException extends UserServiceException {
    
    private static final long serialVersionUID = 1L;

    public UserAlreadyExistsException() {
	super("User already exists");
    }

    public UserAlreadyExistsException(String message) {
	super(message);
    }
    
}
