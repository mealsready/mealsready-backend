package fr.cnam.mealsready.authentification.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:global-${spring.profiles.active}.properties")
public class ConfigurationFactory {

    private KeycloakConfiugartion keycloakConfiguration;

    @Autowired
    public ConfigurationFactory(KeycloakConfiugartion configuration) {
	this.keycloakConfiguration = configuration;
    }

    public KeycloakConfiugartion getKeycloakConfiguration() {
	return keycloakConfiguration;
    }

}
