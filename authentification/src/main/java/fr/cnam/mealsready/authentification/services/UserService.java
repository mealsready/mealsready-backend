package fr.cnam.mealsready.authentification.services;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.cnam.mealsready.authentification.APIConnectors.KeycloakUserAdministration;
import fr.cnam.mealsready.authentification.APIConnectors.DTO.JWTValidationResponseDTO;
import fr.cnam.mealsready.authentification.APIConnectors.DTO.UserDTO;
import fr.cnam.mealsready.authentification.domain.UserDisplayed;
import fr.cnam.mealsready.authentification.domain.UserSubscribed;
import fr.cnam.mealsready.authentification.domain.UserUpdated;
import fr.cnam.mealsready.authentification.exceptions.OperationNotAllowedException;
import fr.cnam.mealsready.authentification.exceptions.UserAlreadyExistsException;
import fr.cnam.mealsready.authentification.exceptions.UserNotFoundException;
import fr.cnam.mealsready.authentification.exceptions.UserServiceException;
import fr.cnam.mealsready.authentification.services.mapper.UserMapper;
import fr.cnam.mealsready.authentification.services.mapper.UserSubscribedMapper;
import fr.cnam.mealsready.core.APIConnector.APIConnectorException;

@Service
public class UserService {

    @Autowired
    KeycloakUserAdministration keycloakUserAdministration;

    @Autowired
    TokenService tokenService;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserSubscribedMapper userSubscribedMapper;

    public int subscribeUser(UserSubscribed user) throws UserServiceException {
	UserDTO userDTO = userSubscribedMapper.makeDTOFromObject(user);
	try {
	    return keycloakUserAdministration.registerUser(userDTO);
	} catch (APIConnectorException e) {
	    switch (e.getStatusCode().value()) {
	    case 409:		
		throw new UserAlreadyExistsException();
	    default:
		throw new UserServiceException();
	    }
	}
    }

    public UserDisplayed getUserById(UUID userId) throws UserServiceException{
	try {
	    UserDTO userRepresentation = keycloakUserAdministration.getUserById(userId);
	    UserDisplayed user = userMapper.makeObjectFromDTO(userRepresentation);
	    return user;
	} catch (APIConnectorException e) {
	    switch (e.getStatusCode().value()) {
	    case 404:		
		throw new UserNotFoundException();
	    default:
		throw new UserServiceException();
	    }
	}
    }

    public UserDisplayed getUserByUserName(String userName) throws UserServiceException {
	UserDisplayed userDisplayed=  new UserDisplayed();
	try {
	    List<UserDTO>  usersDTO =  keycloakUserAdministration.getUserByUserName(userName);
	    List<UserDisplayed> users = new ArrayList<UserDisplayed>() ;
	    if (usersDTO.size()== 0) {
		return  userDisplayed;
	    }
	    for(UserDTO userDTO:usersDTO) {
		if (userDTO.getUsername().equals(userName)) {
		    users.add(userMapper.makeObjectFromDTO(userDTO));
		    userDisplayed = users.get(0);
		}
	    }
	    return userDisplayed;
	} catch (APIConnectorException e) {
	    switch (e.getStatusCode().value()) {
	    default:
		throw new UserServiceException();
	    }
	}
    }

    public UserDisplayed getUserByEmail(String email) throws UserServiceException{
	UserDisplayed userDisplayed=  new UserDisplayed();
	try {
	    List<UserDTO>  usersDTO =  keycloakUserAdministration.getUserByEmail(email);
	    List<UserDisplayed> users = new ArrayList<UserDisplayed>() ;
	    if (usersDTO.size()== 0) {
		return  userDisplayed;
	    }
	    for(UserDTO userDTO:usersDTO) {
		if (userDTO.getEmail().equals(email)) {
		    users.add(userMapper.makeObjectFromDTO(userDTO));
		    userDisplayed = users.get(0);
		}
	    }
	    return userDisplayed;
	} catch (APIConnectorException e) {
	    switch (e.getStatusCode().value()) {
	    default:
		throw new UserServiceException();
	    }
	}
    }

    public int updateUser(UUID id, UserUpdated userUpdated, String authHeader) throws UserServiceException{
	try {
	    if (!canUpdateUser(id, authHeader)) {
		throw new OperationNotAllowedException("user with id : " + id + " not allowed to do operation");
	    }
	    UserDTO userDTO= new UserDTO();
	    userDTO.setFirstName(userUpdated.getFirstName());
	    userDTO.setLastName(userUpdated.getLastName());
	    userDTO.setEnabled(true);
	    return keycloakUserAdministration.updateUser(id, userDTO);
	} catch (APIConnectorException e) {
	    switch (e.getStatusCode().value()) {
	    case 404:		
		throw new UserNotFoundException();
	    default:
		throw new UserServiceException();
	    }
	}
    }

    public int logoutUser(UUID id, String authHeader) throws UserServiceException {
	try {
	    if (!canUpdateUser(id, authHeader)) {
		throw new OperationNotAllowedException("user with id : " + id + " not allowed to do operation");
	    }
	    return keycloakUserAdministration.logoutByUserId(id);
	} catch (APIConnectorException e) {
	    switch (e.getStatusCode().value()) {
	    case 404:		
		throw new UserNotFoundException();
	    default:
		throw new UserServiceException();
	    }
	}
    }

    /**
     * 
     * @param id
     * @param auth
     * @return true if user is authenticated and can do operation
     * @throws UserServiceException
     */
    private Boolean canUpdateUser(UUID id, String authHeader) throws UserServiceException {
	UserDisplayed userRequester = this.getUserById(id);
	JWTValidationResponseDTO jWTValidationResponseDTO = tokenService.validateToken(authHeader);
	UserDisplayed userAutheticated = this.getUserByUserName(jWTValidationResponseDTO.getUsername());
	if (userRequester.getId().equals(userAutheticated.getId())&& jWTValidationResponseDTO.isActive() == true) {
	    return true; 
	}
	return false; 
    }
}
