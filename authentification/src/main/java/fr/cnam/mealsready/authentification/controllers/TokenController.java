package fr.cnam.mealsready.authentification.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.cnam.mealsready.authentification.APIConnectors.DTO.JWTValidationResponseDTO;
import fr.cnam.mealsready.authentification.domain.UserAuthenticated;
import fr.cnam.mealsready.authentification.exceptions.TokenNotValidException;
import fr.cnam.mealsready.authentification.exceptions.UserServiceException;
import fr.cnam.mealsready.authentification.services.TokenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Api(tags = { "/token" }, description = "Handle token validation")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/token")
public class TokenController {
    
    @Autowired
    TokenService tokenService;

    @ApiOperation(value = "Validate token")
    @ApiResponses(value = { //
	    @ApiResponse(code = 401, message = "The given Credentials do not exist"), //
	    @ApiResponse(code = 500, message = "Something bad happend"), //
	    @ApiResponse(code = 200, message = "Token validated"), //
    })
    @PostMapping(value = "/validate")
    public ResponseEntity<JWTValidationResponseDTO> validateUserToken(@RequestBody String JWT) {
	JWTValidationResponseDTO response; 
	try {
	    response = tokenService.validateToken(JWT); 
	} catch (TokenNotValidException e) 
	{ 
	    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
	} catch (UserServiceException e)
	{
	    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR); 
	} return new
		ResponseEntity<>(response, HttpStatus.OK); 
    }

    @ApiOperation(value = "Refresh token")
    @ApiResponses(value = { //
	    @ApiResponse(code = 401, message = "The given Credentials do not exist"), //
	    @ApiResponse(code = 500, message = "Something bad happend"), //
	    @ApiResponse(code = 200, message = "Token validated"), //
    })
    @PostMapping(value = "/refresh")
    public  ResponseEntity<UserAuthenticated> refreshUserToken(@RequestBody String JWT) {
	UserAuthenticated response; 
	try {
	    response = tokenService.getNewAccessToken(JWT); 
	} catch (TokenNotValidException e) 
	{ 
	    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
	} catch (UserServiceException e)
	{
	    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR); 
	} return new
		ResponseEntity<>(response, HttpStatus.OK); 
    }

}
