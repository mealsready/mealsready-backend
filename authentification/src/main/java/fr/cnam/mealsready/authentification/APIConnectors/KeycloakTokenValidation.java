package fr.cnam.mealsready.authentification.APIConnectors;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.keycloak.common.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.cnam.mealsready.authentification.APIConnectors.DTO.JWTValidationParamsDTO;
import fr.cnam.mealsready.authentification.APIConnectors.DTO.JWTValidationResponseDTO;
import fr.cnam.mealsready.authentification.configuration.ConfigurationFactory;
import fr.cnam.mealsready.authentification.configuration.KeycloakConfiugartion;
import fr.cnam.mealsready.core.APIConnector.APIConnectorException;
import fr.cnam.mealsready.core.APIConnector.AbstractAPIConnector;

@Component
public class KeycloakTokenValidation extends AbstractAPIConnector<JWTValidationResponseDTO> {

    @Autowired
    ConfigurationFactory configurationFactory;

    KeycloakConfiugartion keycloakConfiugartion;

    @PostConstruct
    private void init() {
	this.keycloakConfiugartion = this.configurationFactory.getKeycloakConfiguration();
    }

    public JWTValidationResponseDTO validateToken(String JWT) throws APIConnectorException {
	String clientCredentials = keycloakConfiugartion.getMealsrearyClientId() + ":"
		+ keycloakConfiugartion.getMealsreadyClientSecret();
	String encodedCredentials = new String(Base64.encodeBytes(clientCredentials.getBytes()));

	Map<String, String> headers = new HashMap<String, String>();
	headers.put("Authorization", "Basic " + encodedCredentials);
	headers.put("Content-Type", "application/x-www-form-urlencoded");

	JWTValidationParamsDTO tokenValidationDTO = new JWTValidationParamsDTO();
	tokenValidationDTO.setToken(JWT.toString());

	JWTValidationResponseDTO response = post(keycloakConfiugartion.getValidateTokenURL(),
		JWTValidationResponseDTO.class, tokenValidationDTO, headers, "token=" + JWT);

	return response;
    }

}
