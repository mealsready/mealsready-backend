package fr.cnam.mealsready.authentification.APIConnectors;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import fr.cnam.mealsready.authentification.APIConnectors.DTO.CredentialDTO;
import fr.cnam.mealsready.authentification.configuration.ConfigurationFactory;
import fr.cnam.mealsready.authentification.configuration.KeycloakConfiugartion;
import fr.cnam.mealsready.core.APIConnector.APIConnectorException;
import fr.cnam.mealsready.core.APIConnector.AbstractAPIConnector;

@Component
public class KeycloakUserPasswordAdministration extends AbstractAPIConnector<CredentialDTO> {

    @Autowired
    KeycloakAuthentification keycloakAuthentification;

    @Autowired
    ConfigurationFactory configurationFactory;

    KeycloakConfiugartion keycloakConfiugartion;

    @PostConstruct
    private void init(){
	this.keycloakConfiugartion = this.configurationFactory.getKeycloakConfiguration();
    }

    /**
     * reset password by user id
     * @param id
     * @param credentialDTO
     * @return code status
     * @throws APIConnectorException
     */
    public int resetUserPassword(UUID id, CredentialDTO credentialDTO) throws APIConnectorException {
	// Admin token
	String JWT = keycloakAuthentification.getAdminToken();
	// Request header 
	Map<String, String> headers = new HashMap<String, String>();
	headers.put("Content-Type", "application/json");
	headers.put(HttpHeaders.AUTHORIZATION, "bearer " +JWT);

	int status = put(keycloakConfiugartion.getUserResetPassword(), headers,  credentialDTO, id.toString());
	return status;
    }
}
