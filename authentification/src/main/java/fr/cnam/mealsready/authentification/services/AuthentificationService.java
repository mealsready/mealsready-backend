package fr.cnam.mealsready.authentification.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.cnam.mealsready.authentification.APIConnectors.KeycloakAuthentification;
import fr.cnam.mealsready.authentification.APIConnectors.DTO.JWTDTO;
import fr.cnam.mealsready.authentification.domain.Login;
import fr.cnam.mealsready.authentification.domain.UserAuthenticated;
import fr.cnam.mealsready.authentification.domain.UserDisplayed;
import fr.cnam.mealsready.authentification.exceptions.UserServiceException;
import fr.cnam.mealsready.authentification.exceptions.WrongCredentialsException;
import fr.cnam.mealsready.authentification.services.mapper.CredentialMapper;
import fr.cnam.mealsready.core.APIConnector.APIConnectorException;

@Service
public class AuthentificationService {

    @Autowired 
    KeycloakAuthentification keycloakAuthentification;

    @Autowired 
    UserService userService;

    @Autowired 
    TokenService tokenservice;

    @Autowired 
    CredentialMapper credentialMapper;

    public UserAuthenticated login(Login login) throws UserServiceException {
	UserAuthenticated userAuthenticated = new UserAuthenticated();
	try {
	    login.setUserNameOrEmail(getUserUserName(login.getUserNameOrEmail().trim().toLowerCase()));
	    JWTDTO token=  keycloakAuthentification.getToken(login);
	    userAuthenticated.setUser(userService.getUserByUserName(login.getUserNameOrEmail().toLowerCase()));
	    userAuthenticated.setJwt(token);
	    userAuthenticated.setjWTValidationResponseDTO(tokenservice.validateToken(token.getAccess_token()));
	    return userAuthenticated;
	} catch (APIConnectorException e) {
	    switch (e.getStatusCode().value()) {
	    case 401:		
		throw new WrongCredentialsException();
	    default:
		throw new UserServiceException();
	    }
	}
    }

    public String getRealmAdminToken() throws UserServiceException {
	try {
	    return keycloakAuthentification.getAdminToken();
	} catch (APIConnectorException e) {
	    switch (e.getStatusCode().value()) {
	    case 401:		
		throw new WrongCredentialsException();
	    default:
		throw new UserServiceException();
	    }
	}
    }

    /**
     * check if user exists 
     * @param userNameOrEmail
     * @return username
     * @throws UserServiceException
     */
    private String getUserUserName(String userNameOrEmail) throws UserServiceException  {
	String userName= "";
	try {
	    UserDisplayed userByEmail = userService.getUserByEmail(userNameOrEmail);
	    UserDisplayed userByUserName = userService.getUserByUserName(userNameOrEmail);
	    if (!(userByEmail.getUserName() == null)) {
		userName =  userByEmail.getUserName();
		return userName;
	    }else
		if (!(userByUserName.getUserName() == null)) {
		    userName =  userByUserName.getUserName();
		    return userName;
		}
	} catch (UserServiceException e) {
	    throw new UserServiceException();
	}
	return userName;
    }

}
