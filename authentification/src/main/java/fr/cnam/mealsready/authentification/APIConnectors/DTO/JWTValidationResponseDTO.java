package fr.cnam.mealsready.authentification.APIConnectors.DTO;

import fr.cnam.mealsready.core.APIConnector.APIConnectorDTO;

public class JWTValidationResponseDTO implements APIConnectorDTO{
    private boolean active;
    private String username;
    private String session_state;
    private RealmAccess realm_access;
  
    public boolean isActive() {
        return active;
    }
    
    public void setActive(boolean active) {
        this.active = active;
    }
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getSession_state() {
        return session_state;
    }
    
    public void setSession_state(String session_state) {
        this.session_state = session_state;
    }

    public RealmAccess getRealm_access() {
        return realm_access;
    }

    public void setRealm_access(RealmAccess realm_access) {
        this.realm_access = realm_access;
    }

}
