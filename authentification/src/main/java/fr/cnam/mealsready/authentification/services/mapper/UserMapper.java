package fr.cnam.mealsready.authentification.services.mapper;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import fr.cnam.mealsready.authentification.APIConnectors.DTO.CredentialDTO;
import fr.cnam.mealsready.authentification.APIConnectors.DTO.UserDTO;
import fr.cnam.mealsready.authentification.domain.UserDisplayed;
import fr.cnam.mealsready.core.APIConnector.AbstractMapper;

@Component
public class UserMapper extends AbstractMapper<UserDTO , UserDisplayed> {

    @Override
    public UserDisplayed makeObjectFromDTO(UserDTO sourceDTO) {
	UserDisplayed user = new UserDisplayed();
	user.setId(sourceDTO.getId());
	user.setEmail(sourceDTO.getEmail());
	user.setFirstName(sourceDTO.getFirstName());
	user.setLastName(sourceDTO.getLastName());
	user.setUserName(sourceDTO.getUsername());
	return user;
    }

    @Override
    public UserDTO makeDTOFromObject(UserDisplayed user) {
	UserDTO userRepresentation = new UserDTO();
	CredentialDTO credentialRepresentation = new CredentialDTO();
	credentialRepresentation.setType("password");
	credentialRepresentation.setTemporary(false);
	ArrayList<CredentialDTO> credentials = new  ArrayList<CredentialDTO> ();
	credentials.add(credentialRepresentation);
	userRepresentation.setUsername(user.getUserName());
	userRepresentation.setFirstName(user.getFirstName());
	userRepresentation.setLastName(user.getLastName());
	userRepresentation.setEmail(user.getEmail());
	userRepresentation.setEnabled(true);
	userRepresentation.setCredentials(credentials);
	return userRepresentation;
    }

}
