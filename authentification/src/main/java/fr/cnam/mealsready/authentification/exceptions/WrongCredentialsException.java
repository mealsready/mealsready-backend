package fr.cnam.mealsready.authentification.exceptions;

public class WrongCredentialsException extends UserServiceException {
    
    private static final long serialVersionUID = 1L;

    public WrongCredentialsException() {
	super("Credentials are not correct error");
    }
    
    public WrongCredentialsException(String message) {
	super(message);
    }
    
}
