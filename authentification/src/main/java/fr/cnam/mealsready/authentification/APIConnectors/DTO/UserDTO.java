package fr.cnam.mealsready.authentification.APIConnectors.DTO;

import java.util.ArrayList;
import java.util.UUID;

import fr.cnam.mealsready.core.APIConnector.APIConnectorDTO;

public class UserDTO implements APIConnectorDTO{
    private UUID id;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private ArrayList<CredentialDTO> credentials;
    private boolean enabled;
    
    public UUID getId() {
        return id;
    }
    
    public void setId(UUID id) {
        this.id = id;
    }
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getFirstName() {
        return firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public String getLastName() {
        return lastName;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public ArrayList<CredentialDTO> getCredentials() {
        return credentials;
    }
    
    public void setCredentials(ArrayList<CredentialDTO> credentials) {
        this.credentials = credentials;
    }
    
    public boolean isEnabled() {
        return enabled;
    }
    
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    
}
