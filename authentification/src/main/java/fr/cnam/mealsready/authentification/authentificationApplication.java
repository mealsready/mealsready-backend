package fr.cnam.mealsready.authentification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@EnableDiscoveryClient
@SpringBootApplication(exclude = ErrorMvcAutoConfiguration.class)
@ComponentScan({"fr.cnam.mealsready.authentification", "fr.cnam.mealsready.core"})
public class authentificationApplication {

    public static void main(String[] args) {
    	SpringApplication.run(authentificationApplication.class, args);
    }

}
