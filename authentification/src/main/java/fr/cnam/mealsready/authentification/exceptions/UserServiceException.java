package fr.cnam.mealsready.authentification.exceptions;

public class UserServiceException extends Exception {
    
    private static final long serialVersionUID = 1L;
    
    public UserServiceException() {
	super("Error when using the service");
    }
    
    public UserServiceException(String message) {
	super(message);
    }
    
}
