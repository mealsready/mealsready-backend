package fr.cnam.mealsready.authentification.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.cnam.mealsready.authentification.exceptions.UserServiceException;
import fr.cnam.mealsready.authentification.exceptions.WrongCredentialsException;
import fr.cnam.mealsready.authentification.services.AuthentificationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Api(tags = { "/authentification" }, description = "Provides and handle JSON Web tokens")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/jwt")
public class AdminClientController {

    @Autowired
    AuthentificationService authentificationService;
    /*
    @ApiOperation(value = "Provide a JWT for realm admin ")
    @ApiResponses(value = { //
	    @ApiResponse(code = 401, message = "The given Credentials do not exist"), //
	    @ApiResponse(code = 500, message = "Something bad happend"), //
	    @ApiResponse(code = 200, message = "Login successful, the JWT is return as text/plain"), //
    })
    @PostMapping(value = "/admin")
     */
    public ResponseEntity<String> getRealmAdminToken() {
	String token;
	try {
	    token = authentificationService.getRealmAdminToken();
	} catch (WrongCredentialsException e) {
	    return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	} catch (UserServiceException e) {
	    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	return new ResponseEntity<>(token, HttpStatus.OK);
    }

}
