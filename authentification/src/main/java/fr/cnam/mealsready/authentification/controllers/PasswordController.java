package fr.cnam.mealsready.authentification.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.cnam.mealsready.authentification.domain.PasswordUpdated;
import fr.cnam.mealsready.authentification.exceptions.OperationNotAllowedException;
import fr.cnam.mealsready.authentification.exceptions.UserNotFoundException;
import fr.cnam.mealsready.authentification.exceptions.UserServiceException;
import fr.cnam.mealsready.authentification.exceptions.WrongCredentialsException;
import fr.cnam.mealsready.authentification.services.UserPasswordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Api(tags = { "/user/password" }, description = "Manage user password")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/user/password")
public class PasswordController {
    @Autowired
    UserPasswordService userPasswordService;

    /**
     * change authenticated user password
     * @param passwordUpdated
     * @return status code 204 
     * @throws UserServiceException
     */
    @ApiOperation(value = "Change password providing old password")
    @ApiResponses(value = { //
	    @ApiResponse(code = 200, message = "Password upadted 204"),//
	    @ApiResponse(code = 401, message = "User not allowed to do operation"), //
	    @ApiResponse(code = 404, message = "User not found"), //
	    @ApiResponse(code = 500, message = "Something bad happend") //
    })
    @PostMapping(value = "/update")
    public ResponseEntity<String> resetAuthenticatedUserPassword(@RequestBody PasswordUpdated passwordUpdated,String authHeader) {
	int status;
	String response = "";
	try {
	    status = userPasswordService.resetUserPassword(passwordUpdated, authHeader);
	    response = String.valueOf(status) + " : Password updated";
	} catch (WrongCredentialsException e) { 
	    return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	} catch (OperationNotAllowedException e) { 
	    return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	} catch (UserNotFoundException e) {
	    return new ResponseEntity<>(HttpStatus.NOT_FOUND);  
	} catch (UserServiceException e) {
	    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
}

