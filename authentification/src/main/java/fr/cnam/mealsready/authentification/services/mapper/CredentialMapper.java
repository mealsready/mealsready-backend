package fr.cnam.mealsready.authentification.services.mapper;

import org.springframework.stereotype.Component;

import fr.cnam.mealsready.authentification.APIConnectors.DTO.CredentialDTO;
import fr.cnam.mealsready.authentification.domain.Credential;
import fr.cnam.mealsready.core.APIConnector.AbstractMapper;

@Component
public class CredentialMapper extends AbstractMapper<CredentialDTO , Credential> {

    @Override public Credential makeObjectFromDTO(CredentialDTO sourceDTO) { 
	Credential credential= new Credential();
	return credential; 
    }

    @Override public CredentialDTO makeDTOFromObject(Credential sourceObject) {
	CredentialDTO credentialRepresentation = new CredentialDTO();
	credentialRepresentation.setType("password");
	credentialRepresentation.setTemporary(false);
	credentialRepresentation.setValue(sourceObject.getPassword());
	return credentialRepresentation; 
    } 
    
}
