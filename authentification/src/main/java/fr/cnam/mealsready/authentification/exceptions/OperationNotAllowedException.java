package fr.cnam.mealsready.authentification.exceptions;

public class OperationNotAllowedException extends UserServiceException {

    private static final long serialVersionUID = -3336703757825557516L;

    public OperationNotAllowedException() {
	super("User can not do operation");
    }

    public OperationNotAllowedException(String message) {
	super(message);
    }

}
