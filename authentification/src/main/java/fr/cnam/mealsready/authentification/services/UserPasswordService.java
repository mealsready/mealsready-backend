package fr.cnam.mealsready.authentification.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.cnam.mealsready.authentification.APIConnectors.KeycloakUserPasswordAdministration;
import fr.cnam.mealsready.authentification.APIConnectors.DTO.CredentialDTO;
import fr.cnam.mealsready.authentification.APIConnectors.DTO.JWTValidationResponseDTO;
import fr.cnam.mealsready.authentification.domain.Login;
import fr.cnam.mealsready.authentification.domain.PasswordUpdated;
import fr.cnam.mealsready.authentification.domain.UserAuthenticated;
import fr.cnam.mealsready.authentification.domain.UserDisplayed;
import fr.cnam.mealsready.authentification.exceptions.OperationNotAllowedException;
import fr.cnam.mealsready.authentification.exceptions.UserNotFoundException;
import fr.cnam.mealsready.authentification.exceptions.UserServiceException;
import fr.cnam.mealsready.authentification.exceptions.WrongCredentialsException;
import fr.cnam.mealsready.core.APIConnector.APIConnectorException;

@Service
public class UserPasswordService {

    @Autowired
    KeycloakUserPasswordAdministration keycloakUserPasswordAdministration;

    @Autowired
    UserService subscriptionService;

    @Autowired
    AuthentificationService authentificationService;

    @Autowired
    UserService userService;

    @Autowired
    TokenService tokenService;

    /**
     * 
     * @param passwordUpdated
     * @return status code 200 if password has been changer or 403 if forbidden
     * @throws UserServiceException
     */
    public int resetUserPassword(PasswordUpdated passwordUpdated, String authHeader) throws UserServiceException {
	int status = 401;
	CredentialDTO credentialDTO = new CredentialDTO();
	credentialDTO.setTemporary(false);
	credentialDTO.setType("password");
	credentialDTO.setValue(passwordUpdated.getNewPassword());
	UserDisplayed user = userService.getUserByEmail(passwordUpdated.getEmail());
	try {
	    if(!canResetPassword(user, passwordUpdated, authHeader)) {
		throw new OperationNotAllowedException("user with id : " + user.getId() + " not allowed to do operation");
	    }
	    status= keycloakUserPasswordAdministration.resetUserPassword(user.getId(), credentialDTO);

	    return status;
	} catch (APIConnectorException e) {
	    switch (e.getStatusCode().value()) {
	    case 401:		
		throw new WrongCredentialsException();
	    case 404:		
		throw new UserNotFoundException();
	    default:
		throw new UserServiceException();
	    }
	}
    }

    /**
     * 
     * @param user
     * @param passwordUpdated
     * @return true if user is autheticated and can change password
     * @throws UserServiceException
     */
    private Boolean canResetPassword(UserDisplayed user, PasswordUpdated passwordUpdated, String authHeader) throws UserServiceException {
	if(user.getEmail() != null) {
	    if (user.getEmail().equals(passwordUpdated.getEmail())) { 
		Login login = new Login(); 
		login.setUserNameOrEmail(user.getUserName());
		login.setPassword(passwordUpdated.getOldPassword()); //
		UserAuthenticated userAuthenticated = authentificationService.login(login);
		JWTValidationResponseDTO jWTValidationResponseDTO = tokenService.validateToken(authHeader);
		if (userAuthenticated.getjWTValidationResponseDTO().isActive() && jWTValidationResponseDTO.isActive() //
			&& jWTValidationResponseDTO.getUsername().equals(userAuthenticated.getUser().getUserName())) {
		    return true; 
		}
	    }
	} return false; 
    }
}
