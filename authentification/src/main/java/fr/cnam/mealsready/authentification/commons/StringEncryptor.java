package fr.cnam.mealsready.authentification.commons;

import java.nio.charset.StandardCharsets;

import com.google.common.hash.Hashing;

public class StringEncryptor {

    public static String getSha256(String password) {
	String sha256hpassword = Hashing.sha256() //
		.hashString(password, StandardCharsets.UTF_8) //
		.toString();
	return sha256hpassword;
    }
    
}
