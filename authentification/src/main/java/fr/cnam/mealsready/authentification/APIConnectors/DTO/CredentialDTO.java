package fr.cnam.mealsready.authentification.APIConnectors.DTO;

import fr.cnam.mealsready.core.APIConnector.APIConnectorDTO;

public class CredentialDTO implements APIConnectorDTO {
    private String type;
    private String value;
    private boolean temporary;
    
    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    public String getValue() {
        return value;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
    
    public boolean isTemporary() {
        return temporary;
    }
    
    public void setTemporary(boolean temporary) {
        this.temporary = temporary;
    }
    
}
