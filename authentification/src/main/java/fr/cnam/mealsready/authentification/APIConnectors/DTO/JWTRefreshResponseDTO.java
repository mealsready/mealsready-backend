package fr.cnam.mealsready.authentification.APIConnectors.DTO;

import fr.cnam.mealsready.core.APIConnector.APIConnectorDTO;

public class JWTRefreshResponseDTO implements APIConnectorDTO{
    private Integer exp;
    private Integer session_state;
    private String client_id;
    private String username;
    private String email;
    private String scope;
    
    public Integer getExp() {
        return exp;
    }
    
    public void setExp(Integer exp) {
        this.exp = exp;
    }
    
    public Integer getSession_state() {
        return session_state;
    }
    
    public void setSession_state(Integer session_state) {
        this.session_state = session_state;
    }
    
    public String getClient_id() {
        return client_id;
    }
    
    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getScope() {
        return scope;
    }
    
    public void setScope(String scope) {
        this.scope = scope;
    }
  
}
