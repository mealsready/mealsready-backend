package fr.cnam.mealsready.authentification.domain;

import fr.cnam.mealsready.authentification.APIConnectors.DTO.JWTDTO;
import fr.cnam.mealsready.authentification.APIConnectors.DTO.JWTValidationResponseDTO;

public class UserAuthenticated {
    private JWTDTO jwt;
    private UserDisplayed user;
    private JWTValidationResponseDTO jWTValidationResponseDTO;
    
    public JWTDTO getJwt() {
        return jwt;
    }
    
    public void setJwt(JWTDTO jwt) {
        this.jwt = jwt;
    }
    
    public UserDisplayed getUser() {
        return user;
    }
    
    public void setUser(UserDisplayed user) {
        this.user = user;
    }
    
    public JWTValidationResponseDTO getjWTValidationResponseDTO() {
        return jWTValidationResponseDTO;
    }
    
    public void setjWTValidationResponseDTO(JWTValidationResponseDTO jWTValidationResponseDTO) {
        this.jWTValidationResponseDTO = jWTValidationResponseDTO;
    }

}
