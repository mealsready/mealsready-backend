package fr.cnam.mealsready.authentification.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.cnam.mealsready.authentification.APIConnectors.KeycloakAuthentification;
import fr.cnam.mealsready.authentification.APIConnectors.KeycloakTokenValidation;
import fr.cnam.mealsready.authentification.APIConnectors.DTO.JWTDTO;
import fr.cnam.mealsready.authentification.APIConnectors.DTO.JWTValidationResponseDTO;

import fr.cnam.mealsready.authentification.domain.UserAuthenticated;
import fr.cnam.mealsready.authentification.exceptions.TokenNotValidException;
import fr.cnam.mealsready.authentification.exceptions.UserNotFoundException;
import fr.cnam.mealsready.authentification.exceptions.UserServiceException;
import fr.cnam.mealsready.core.APIConnector.APIConnectorException;
import fr.cnam.mealsready.core.domain.MealsReadyRolesEnum;

@Service
public class TokenService {
    
    @Autowired 
    KeycloakTokenValidation keycloakTokenValidation;

    @Autowired
    KeycloakAuthentification keycloakAuthentification;

    @Autowired
    UserService userService;

    /**
     * validate token 
     * @param JWT
     * @return JWTValidationResponseDTO
     * @throws UserServiceException
     */
    public JWTValidationResponseDTO validateToken(String JWT) throws UserServiceException {
	int statusCode=0;
	try {
	    if (keycloakTokenValidation.validateToken(JWT).isActive() == false) {
		throw new TokenNotValidException();
	    }
	    return keycloakTokenValidation.validateToken(JWT);
	} catch (APIConnectorException e) {
	    statusCode = e.getStatusCode().value();
	    switch (statusCode) {
	    case 403:		
		throw new TokenNotValidException();
	    default:
		throw new UserServiceException();
	    }
	}
    }

    /**
     *  get a new access token by refresh token
     * @param refreshToken
     * @return
     * @throws UserServiceException
     */
    public UserAuthenticated getNewAccessToken(String refreshToken) throws UserServiceException {
	int statusCode=0;
	UserAuthenticated userAuthenticated = new UserAuthenticated();
	try {
	    JWTDTO JWTDTO =  keycloakAuthentification.getNewAccessToken(refreshToken);
	    JWTValidationResponseDTO JWTValidationResponseDTO = this.validateToken(JWTDTO.getAccess_token());
	    userAuthenticated.setUser(userService.getUserByUserName(JWTValidationResponseDTO.getUsername()));
	    userAuthenticated.setJwt(JWTDTO);
	    userAuthenticated.setjWTValidationResponseDTO(JWTValidationResponseDTO);
	    return userAuthenticated;
	} catch (APIConnectorException e) {
	    statusCode = e.getStatusCode().value();
	    switch (statusCode) {
	    case 403:		
		throw new TokenNotValidException();
	    default:
		throw new UserServiceException();
	    }
	}
    }
    
    /**
     * Get User Role
     * @param JWT
     * @return Role
     * @throws UserServiceException
     */
    public MealsReadyRolesEnum getUserRole(String JWT) throws UserServiceException {
	int statusCode=0;
	try {
	    if (keycloakTokenValidation.validateToken(JWT).isActive() == false) {
		throw new TokenNotValidException();
	    }
	
	    return getRoleFromJWTValidationResponseDTO(keycloakTokenValidation.validateToken(JWT));
	} catch (APIConnectorException e) {
	    statusCode = e.getStatusCode().value();
	    switch (statusCode) {
	    case 403:		
		throw new TokenNotValidException();
	    default:
		throw new UserServiceException();
	    }
	}
    }
    
    private MealsReadyRolesEnum getRoleFromJWTValidationResponseDTO(JWTValidationResponseDTO jWTValidationResponseDTO) throws UserNotFoundException {

	if (jWTValidationResponseDTO.getRealm_access().getRoles().contains("USER_ACCESS")) {
	    return MealsReadyRolesEnum.USER;
	}
	else 
		throw new UserNotFoundException();
    }

}
