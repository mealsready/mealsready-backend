package fr.cnam.mealsready.authentification.APIConnectors.DTO;

import fr.cnam.mealsready.core.APIConnector.APIConnectorDTO;

public class JWTValidationParamsDTO implements APIConnectorDTO{
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
 
}
