package fr.cnam.mealsready.authentification.APIConnectors;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.keycloak.common.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.cnam.mealsready.authentification.APIConnectors.DTO.JWTDTO;
import fr.cnam.mealsready.authentification.APIConnectors.DTO.JWTRefreshParamsDTO;
import fr.cnam.mealsready.authentification.APIConnectors.DTO.KeycloakAuthParamsDTO;
import fr.cnam.mealsready.authentification.configuration.ConfigurationFactory;
import fr.cnam.mealsready.authentification.configuration.KeycloakConfiugartion;
import fr.cnam.mealsready.authentification.domain.Login;
import fr.cnam.mealsready.core.APIConnector.APIConnectorException;
import fr.cnam.mealsready.core.APIConnector.AbstractAPIConnector;

@Component
public class KeycloakAuthentification extends AbstractAPIConnector<JWTDTO> {

    @Autowired
    KeycloakTokenValidation keycloakTokenValidation;

    @Autowired
    ConfigurationFactory configurationFactory;

    KeycloakConfiugartion keycloakConfiugartion;

    @PostConstruct
    private void init(){
	this.keycloakConfiugartion =this.configurationFactory.getKeycloakConfiguration();
    }

    public JWTDTO getToken(Login login) throws APIConnectorException {
	KeycloakAuthParamsDTO keycloakAuthParamsDTO = this.getKeycloakAuthParamsDTO(keycloakConfiugartion.getMealsrearyClientId(), keycloakConfiugartion.getMealsreadyClientSecret(), keycloakConfiugartion.getGrantType());
	keycloakAuthParamsDTO.setUsername(login.getUserNameOrEmail());
	keycloakAuthParamsDTO.setPassword(login.getPassword());

	Map<String, String> headers = new HashMap<String, String>();
	headers.put("Content-Type", "application/x-www-form-urlencoded");

	JWTDTO response = post(keycloakConfiugartion.getTokenURL(), JWTDTO.class, keycloakAuthParamsDTO, headers);
	keycloakTokenValidation.validateToken(response.getAccess_token());
	return response;
    }

    public String getAdminToken() throws APIConnectorException {
	KeycloakAuthParamsDTO keycloakAuthParamsDTO = this.getKeycloakAuthParamsDTO(keycloakConfiugartion.getKeycloakAdminClientId(),keycloakConfiugartion.getKeycloakAdminClientSecret() , keycloakConfiugartion.getGrantType());
	keycloakAuthParamsDTO.setUsername(keycloakConfiugartion.getKeycloakAdminClient());
	keycloakAuthParamsDTO.setPassword(keycloakConfiugartion.getKeycloakAdminPassword());

	Map<String, String> headers = new HashMap<String, String>();
	headers.put("Content-Type", "application/x-www-form-urlencoded");

	String JWT;
	JWTDTO resp = post(keycloakConfiugartion.getTokenURL(), JWTDTO.class, keycloakAuthParamsDTO, headers);
	JWT = resp.getAccess_token();

	return JWT;
    }

    public JWTDTO getNewAccessToken(String refreshToken) throws APIConnectorException {
	String clientCredentials = keycloakConfiugartion.getMealsrearyClientId() + ":" + keycloakConfiugartion.getMealsreadyClientSecret();
	String encodedCredentials = new String(Base64.encodeBytes(clientCredentials.getBytes()));

	Map<String, String> headers = new HashMap<String, String>();
	headers.put("Authorization", "Basic " + encodedCredentials);
	headers.put("Content-Type", "application/x-www-form-urlencoded");

	JWTRefreshParamsDTO jWTRefreshParamsDTO = new JWTRefreshParamsDTO();
	jWTRefreshParamsDTO.setRefresh_token(refreshToken.toString());
	jWTRefreshParamsDTO.setClient_id(keycloakConfiugartion.getMealsrearyClientId());
	jWTRefreshParamsDTO.setGrant_type("refresh_token");

	JWTDTO response = post(keycloakConfiugartion.getTokenURL(), JWTDTO.class, jWTRefreshParamsDTO, headers);
	keycloakTokenValidation.validateToken(response.getAccess_token());
	return response;
    }

    private KeycloakAuthParamsDTO getKeycloakAuthParamsDTO(String clientId, String clientSecret, String grantType) {
	KeycloakAuthParamsDTO keycloakAuthParamsDTO = new KeycloakAuthParamsDTO();
	keycloakAuthParamsDTO.setClient_id(clientId);
	keycloakAuthParamsDTO.setClient_secret(clientSecret);
	keycloakAuthParamsDTO.setGrant_type(grantType);
	return keycloakAuthParamsDTO;
    }

}

