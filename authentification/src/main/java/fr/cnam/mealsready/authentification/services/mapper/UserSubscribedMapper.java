package fr.cnam.mealsready.authentification.services.mapper;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import fr.cnam.mealsready.authentification.APIConnectors.DTO.CredentialDTO;
import fr.cnam.mealsready.authentification.APIConnectors.DTO.UserDTO;
import fr.cnam.mealsready.authentification.domain.UserSubscribed;
import fr.cnam.mealsready.core.APIConnector.AbstractMapper;

@Component
public class UserSubscribedMapper  extends AbstractMapper<UserDTO, UserSubscribed> {

    @Override
    public UserSubscribed makeObjectFromDTO(UserDTO userDTO) {
	UserSubscribed userSubscribed = new UserSubscribed();
	userSubscribed.setEmail(userDTO.getEmail());
	userSubscribed.setFirstName(userDTO.getFirstName());
	userSubscribed.setLastName(userDTO.getLastName());
	userSubscribed.setUserName(userDTO.getUsername());
	return userSubscribed;
    }

    @Override
    public UserDTO makeDTOFromObject(UserSubscribed usersubscribed) {
	UserDTO userDTO = new UserDTO();
	CredentialDTO credentialRepresentation = new CredentialDTO();
	credentialRepresentation.setType("password");
	credentialRepresentation.setValue(usersubscribed.getPassword());
	credentialRepresentation.setTemporary(false);
	ArrayList<CredentialDTO> credentials = new  ArrayList<CredentialDTO> ();
	credentials.add(credentialRepresentation);
	userDTO.setUsername(usersubscribed.getUserName());
	userDTO.setFirstName(usersubscribed.getFirstName());
	userDTO.setLastName(usersubscribed.getLastName());
	userDTO.setEmail(usersubscribed.getEmail());
	userDTO.setEnabled(true);
	userDTO.setCredentials(credentials);
	return userDTO;
    }

}
