package fr.cnam.mealsready.authentification.controllers;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.cnam.mealsready.authentification.domain.Login;
import fr.cnam.mealsready.authentification.domain.UserAuthenticated;
import fr.cnam.mealsready.authentification.domain.UserDisplayed;
import fr.cnam.mealsready.authentification.domain.UserSubscribed;
import fr.cnam.mealsready.authentification.domain.UserUpdated;
import fr.cnam.mealsready.authentification.exceptions.OperationNotAllowedException;
import fr.cnam.mealsready.authentification.exceptions.TokenNotValidException;
import fr.cnam.mealsready.authentification.exceptions.UserAlreadyExistsException;
import fr.cnam.mealsready.authentification.exceptions.UserNotFoundException;
import fr.cnam.mealsready.authentification.exceptions.UserServiceException;
import fr.cnam.mealsready.authentification.exceptions.WrongCredentialsException;
import fr.cnam.mealsready.authentification.services.AuthentificationService;
import fr.cnam.mealsready.authentification.services.TokenService;
import fr.cnam.mealsready.authentification.services.UserService;
import fr.cnam.mealsready.core.domain.MealsReadyRolesEnum;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Api(tags = {"/users"}, description = "Manage Keycloak users")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    AuthentificationService authentificationService;
    
    @Autowired
    TokenService tokenService;
    
    @ApiOperation(value = "Subscribe user on mealsreadyrealm")
    @ApiResponses(value = { //
	    @ApiResponse(code = 409, message = "User exists with same username"), //
	    @ApiResponse(code = 500, message = "Something bad happend"), //
	    @ApiResponse(code = 201, message = "User created, return the user"), //
    })
    @PostMapping(value = "user/subscribe")
    public ResponseEntity<UserDisplayed> addUser(@RequestBody UserSubscribed user) {
	int status;
	UserDisplayed response = new UserDisplayed();
	try {
	    status = userService.subscribeUser(user);
	} 
	catch (UserAlreadyExistsException e) {
	    return new ResponseEntity<>(HttpStatus.CONFLICT);  
	} catch (UserServiceException e) {
	    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	} 
	if(status == 201) {
	    response.setFirstName(user.getFirstName());
	    response.setLastName(user.getLastName());
	    response.setEmail(user.getEmail());
	    response.setUserName(user.getUserName());
	}
	return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Retrieve user by id")
    @ApiResponses(value = { //
	    @ApiResponse(code = 200, message = "Return the user"),//
	    @ApiResponse(code = 404, message = "User not found"), //
	    @ApiResponse(code = 500, message = "Something bad happend") //    
    })
    @GetMapping(value = "/user/{id}")
    public ResponseEntity<UserDisplayed> getUserById
    (@ApiParam(name = "id", type = "UUID", value = "User id", example ="3ebbd846-04ce-44a3-bbe3-f992c9360d00", required = true) //
    @PathVariable("id") UUID id)  {
	UserDisplayed response = new UserDisplayed();
	try {
	    response =userService.getUserById(id);
	} 
	catch (UserNotFoundException e) {
	    return new ResponseEntity<>(HttpStatus.NOT_FOUND);  
	} catch (UserServiceException e) {
	    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	} 
	return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Retrieve user by username")
    @ApiResponses(value = { //
	    @ApiResponse(code = 200, message = "Return the user"),//
	    @ApiResponse(code = 404, message = "User not found"), //
	    @ApiResponse(code = 500, message = "Something bad happend") //    
    })
    @GetMapping(value = "/user/username/{username}")
    public ResponseEntity<UserDisplayed> getUserByUserName
    (@ApiParam(name = "username", type = "String", value = "username", example ="string", required = true) //
    @PathVariable("username") String username) {
	UserDisplayed response = new UserDisplayed();
	try {
	    response = userService.getUserByUserName(username.trim().toLowerCase());
	    if (response.getUserName().equals(null)) {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND); 
	    }
	} catch (UserServiceException e) {
	    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	} 
	catch (Exception e) {
	    return new ResponseEntity<>(HttpStatus.NOT_FOUND);  
	}
	return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Retrieve user by email")
    @ApiResponses(value = { //
	    @ApiResponse(code = 200, message = "Return the user"),//
	    @ApiResponse(code = 404, message = "User not found"), //
	    @ApiResponse(code = 500, message = "Something bad happend") //    
    })
    @GetMapping(value = "/user/email/{email}")
    public ResponseEntity<UserDisplayed> getUserByEmail
    (@ApiParam(name = "email", type = "String", value = "email", example ="string", required = true) //
    @PathVariable("email") String email) {
	UserDisplayed response = new UserDisplayed();
	try {
	    response = userService.getUserByEmail(email.trim().toLowerCase());
	    if (response.getEmail().equals(null)) {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND); 
	    }
	} 
	catch (UserNotFoundException e) {
	    return new ResponseEntity<>(HttpStatus.NOT_FOUND);  
	} catch (UserServiceException e) {
	    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	} 
	catch (Exception e) {
	    return new ResponseEntity<>(HttpStatus.NOT_FOUND);  
	}
	return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Update user firstName and lastName")
    @ApiResponses(value = { //
	    @ApiResponse(code = 200, message = "User upadted, return the user"),//
	    @ApiResponse(code = 404, message = "user not found"), //
	    @ApiResponse(code = 401, message = "User not allowed to do operation"), //
	    @ApiResponse(code = 500, message = "Something bad happend") //
    })
    @PutMapping(value = "/user/{id}/update")
    public ResponseEntity<UserDisplayed> UpdateUser
    (@ApiParam(name = "id", type = "UUID", value = "User id", example ="3ebbd846-04ce-44a3-bbe3-f992c9360d00", required = true) //
    @PathVariable("id") UUID id, @RequestBody UserUpdated userUpdated, @RequestHeader("Authorization") String authHeader) {
	UserDisplayed response = new UserDisplayed();
	try {
	    int status = userService.updateUser(id, userUpdated, authHeader);
	    if (status == 204) {
		response = userService.getUserById(id);
	    }
	} 
	catch (UserNotFoundException e) {
	    return new ResponseEntity<>(HttpStatus.NOT_FOUND);  
	}  catch (OperationNotAllowedException e) {
	    return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	} catch (UserServiceException e) {
	    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	} 
	return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Provide a JWT if given credentials were authenticated ")
    @ApiResponses(value = { //
	    @ApiResponse(code = 401, message = "The given Credentials do not exist"), //
	    @ApiResponse(code = 500, message = "Something bad happend"), //
	    @ApiResponse(code = 200, message = "Login successful, the JWT is return as text/plain"), //
    })
    @PostMapping(value = "user/login")
    public ResponseEntity<UserAuthenticated> login(@RequestBody Login login) {
	UserAuthenticated userAuthenticated;
	try {
	    userAuthenticated = authentificationService.login(login);
	} catch (WrongCredentialsException e) {
	    return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	} catch (UserServiceException e) {
	    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	return new ResponseEntity<>(userAuthenticated, HttpStatus.OK);
    }

    @ApiOperation(value = "Remove all user sessions associated with the user Also send notification to all clients that have an admin URL to invalidate the sessions for the particular user.\r\n"
	    + "\r\n"
	    + "")
    @ApiResponses(value = { //
	    @ApiResponse(code = 409, message = "User exists with same username"), //
	    @ApiResponse(code = 500, message = "Something bad happend"), //
	    @ApiResponse(code = 401, message = "User not allowed to do operation"), //
	    @ApiResponse(code = 200, message = "User logged out"), //
    })
    @PostMapping(value = "/user/{id}/logout")
    public ResponseEntity<String> logout
    (@ApiParam(name = "id", type = "UUID", value = "User id", example ="3ebbd846-04ce-44a3-bbe3-f992c9360d00", required = true) 
    @PathVariable("id") UUID id , @RequestHeader("Authorization") String authHeader) {
	String response = "";
	try {
	    int status = userService.logoutUser(id , authHeader);
	    response = String.valueOf(status) + " : All sessions are disconnected for user : " + id ;
	}
	catch (UserNotFoundException e) {
	    return new ResponseEntity<>(HttpStatus.NOT_FOUND);  
	} catch (OperationNotAllowedException e) {
	    return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	} catch (UserServiceException e) {
	    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @ApiOperation(value = "get user role")
    @ApiResponses(value = { //
	    @ApiResponse(code = 404, message = "No such role for this user"), //
	    @ApiResponse(code = 500, message = "Something bad happend"), //
	    @ApiResponse(code = 200, message = "Token validated"), //
    })
    @GetMapping(value = "/role")
    public ResponseEntity<MealsReadyRolesEnum> getUserRole(@RequestHeader("Authorization") String authHeader)  {
	MealsReadyRolesEnum response;
	try {
	    response = tokenService.getUserRole(authHeader);
	} catch (UserNotFoundException e) {
	    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}  catch (TokenNotValidException e) {
	    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
	}catch (UserServiceException e) {
	    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
