package fr.cnam.mealsready.pdf.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.FileTemplateResolver;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;

import fr.cnam.mealsready.core.services.ResourceLoaderService;

/**
 * PDF generation service. This service uses Thymeleaf as HTML template engine
 * 
 * @author christopher MILAZZO
 * @since v1.0
 *
 */
@Service
public class PDFGenerator {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private BeanFactory beanFactory;

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    ResourceLoaderService resourceLoader;

    private Path outputFolderPath;

    private final int MAX_PDF_AGING_MILLIS = 300000; // 5 minutes

    @PostConstruct
    private void init() throws Exception {
	FileTemplateResolver fileTemplateResolver = new FileTemplateResolver();
	fileTemplateResolver.setCharacterEncoding("UTF-8");
	fileTemplateResolver.setTemplateMode(TemplateMode.HTML);
	templateEngine.setTemplateResolver(fileTemplateResolver);

	this.outputFolderPath = Paths.get(resourceLoader.getResourceAbsolutePathString("file:outpdf"));
    }

    /**
     * Generate a PDF from the given HTML template file without any specific context
     * 
     * @param pdfTemplate    - The HTML template file located in the
     *                       <b>/resources/templates/ folder</b>
     * @param outputFileName - The name of the generated PDF file
     * 
     * @return the relative URI to the generated PDF file from the
     *         <b>/resources/public/output/</b>
     * @throws Exception
     */
    public String generatePDFFromHTML(final PDFTemplates pdfTemplate, String outputFileName) throws Exception {
	return this.generatePDFFromHTML(pdfTemplate, outputFileName, new EmptyContextBuilder());
    }

    /**
     * Generate a PDF from the given HTML template file using the given context
     * 
     * @param pdfTemplate    - The HTML template file located in the
     *                       <b>/resources/templates/ folder</b>
     * @param outputFileName - The name of the generated PDF file
     * @param contextBuilder - Construct a context to bind variables in the template
     * 
     * @return the relative URI to the generated PDF file from the
     *         <b>/resources/public/output/</b>
     * @throws Exception
     */
    public String generatePDFFromHTML(final PDFTemplates pdfTemplate, String outputFileName,
	    final AbstractPDFContextBuilder contextBuilder) throws Exception {
	String uniqueDirName = "";
	try {
	    // Clean output folder
	    deleteOldGeneratedPDF();

	    // Prepare IO
	    String templateAbsolutePathString = resourceLoader
		    .getResourceAbsolutePathString("classpath:templates/" + pdfTemplate.getTemplateName());
	    uniqueDirName = this.generateUniqueDirNameFor(this.outputFolderPath);
	    String outputFileURI = Paths.get(this.outputFolderPath.toString(), uniqueDirName, outputFileName)
		    .toString();

	    Path pathToCreateIfNotExists = Paths.get(this.outputFolderPath.toString(), uniqueDirName);
	    if (Files.notExists(pathToCreateIfNotExists)) {
		new File(pathToCreateIfNotExists.toString()).mkdirs();
	    }

	    File pdfDestFile = new File(outputFileURI);

	    // pdfHTML specific code
	    ConverterProperties converterProperties = new ConverterProperties();

	    // Build context
	    PDFTemplateContext context = contextBuilder.buildContext(beanFactory);

	    try {
		String HTMLGeneratedString = templateEngine.process(templateAbsolutePathString, context);
		logger.debug("Generated HTML file :\n" + HTMLGeneratedString);
		HtmlConverter.convertToPdf(HTMLGeneratedString, new FileOutputStream(pdfDestFile), converterProperties);
	    } catch (IOException e) {
		logger.error("Error while trying to generatePDF using template " + pdfTemplate.getTemplateName()
			+ " caused by : " + e.getMessage());
		throw new Exception();
	    }
	    uniqueDirName = uniqueDirName.replaceAll("-", "/");
	} catch (Exception e) {
	    logger.error(e.getMessage());
	    throw new Exception();
	}

	return "/pdf/get/" + uniqueDirName + "/" + outputFileName;
    }

    /**
     * Generate a random dirName for the given destination folder by concatenating a
     * timestamp and a random int like so : &#60;timestamp&#62;-&#60;random int&#62;
     * 
     * @param destFolderPath the folder where the dirName should be unique
     * @return the dirName
     */
    private String generateUniqueDirNameFor(final Path destFolderPath) {
	long timestamp = new Date().getTime();
	String dirName;

	do {
	    int rdm = (int) (Math.random() * 1000);
	    StringBuilder stringBuilder = new StringBuilder();
	    stringBuilder.append(timestamp);
	    stringBuilder.append("-");
	    stringBuilder.append(rdm);

	    dirName = stringBuilder.toString();
	} while (Files.exists(Path.of(destFolderPath.toString(), dirName)));

	return dirName;
    }

    private void deleteOldGeneratedPDF() {
	ArrayList<File> foldersToDelete = getAllDirectoriesThatNeedsToBeDeleted(this.outputFolderPath);
	int folderDeletedQuantity = 0;
	for (File folder : foldersToDelete) {
	    if (deleteDirectoryAndAllContents(folder)) {
		folderDeletedQuantity++;
	    } else {
		System.err.println("Problem trying to delete folder " + folder.getName());
	    }
	}
	logger.info(
		"Checked old generated folder. " + folderDeletedQuantity + " folder(s) have been deleted successfuly.");
    }

    private ArrayList<File> getAllDirectoriesThatNeedsToBeDeleted(final Path folder) {
	File file = new File(folder.toString());
	File[] files = file.listFiles();
	ArrayList<File> foldersToDelete = new ArrayList<File>();
	int folderDeletedQuantity = 0;

	if (files != null) {
	    for (int i = 0; i < files.length; i++) {
		if (files[i].isDirectory() == true && isFolderTooOld(files[i])) {
		    folderDeletedQuantity++;
		    foldersToDelete.add(files[i]);
		}
	    }
	}
	logger.info(folderDeletedQuantity + " folder(s) have to be deleted.");
	return foldersToDelete;
    }

    private boolean isFolderTooOld(final File folder) {
	boolean isTooOld = false;
	String folderName = folder.getName();
	int index = folderName.lastIndexOf("-");

	if (index > 0) {
	    long folderTimestamp = Long.parseLong(folderName.substring(0, index));
	    if (new Date().getTime() - folderTimestamp > this.MAX_PDF_AGING_MILLIS) {
		isTooOld = true;
	    }
	}
	return isTooOld;
    }

    private boolean deleteDirectoryAndAllContents(final File directoryToBeDeleted) {
	File[] allContents = directoryToBeDeleted.listFiles();
	if (allContents != null) {
	    for (File file : allContents) {
		deleteDirectoryAndAllContents(file);
	    }
	}
	return directoryToBeDeleted.delete();
    }
}
