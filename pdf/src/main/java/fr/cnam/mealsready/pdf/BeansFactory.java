package fr.cnam.mealsready.pdf;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import fr.cnam.mealsready.core.domain.Day;
import fr.cnam.mealsready.core.domain.Ingredient;
import fr.cnam.mealsready.core.domain.Recipe;
import fr.cnam.mealsready.pdf.services.PDFTemplateContext;
import fr.cnam.mealsready.pdf.services.RecipeContextBuilder;
import fr.cnam.mealsready.pdf.services.RecipesShoppinglistContextBuilder;

@Configuration
public class BeansFactory {

    @Bean
    @Scope(value = "prototype")
    public PDFTemplateContext makePDFTemplateContext(Map<String, Object> objectsToMap, String... fragments) {
	return new PDFTemplateContext(objectsToMap, fragments);
    }

    @Bean
    @Scope(value = "prototype")
    public RecipesShoppinglistContextBuilder makeRecipesShoppinglistContextBuilder(ArrayList<Day> daysList,
	    ArrayList<Ingredient> ingredientsList) {
	return new RecipesShoppinglistContextBuilder(daysList, ingredientsList);
    }

    @Bean
    @Scope(value = "prototype")
    public RecipeContextBuilder makeRecipeContextBuilder(Recipe recipe) {
	return new RecipeContextBuilder(recipe);
    }
}
