package fr.cnam.mealsready.pdf.exceptions;

public class ServiceException extends Exception {
    private static final long serialVersionUID = 1L;

    public ServiceException() {
	super("Service error");
    }

    public ServiceException(String message) {
	super(message);
    }
}
