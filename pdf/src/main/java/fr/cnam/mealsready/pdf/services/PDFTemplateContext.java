package fr.cnam.mealsready.pdf.services;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.thymeleaf.context.IContext;

import fr.cnam.mealsready.core.services.ResourceLoaderService;
import fr.cnam.mealsready.pdf.exceptions.PDFTemplateContextException;


public class PDFTemplateContext implements IContext {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ResourceLoaderService resourceLoader;

    private Locale locale;
    private HashSet<String> variablesNames;
    private Map<String, Object> mappedVariables;
    private String[] resourcesFiles;

    /**
     * Build a context for HTML template
     * 
     * @param objectsToMap   provide a Map with all variables to be binded. The keys
     *                       are variables names used inside the template and
     *                       Objects are the data to be binded.
     * 
     * @param resourcesFiles (Optional) Fragments names
     * 
     */
    public PDFTemplateContext(final Map<String, Object> objectsToMap, final String... resourcesFiles) {
	this.locale = new Locale("french");
	this.variablesNames = new HashSet<String>();
	this.mappedVariables = objectsToMap;
	this.resourcesFiles = resourcesFiles;

	Iterator<Entry<String, Object>> iterator = mappedVariables.entrySet().iterator();

	while (iterator.hasNext()) {
	    Entry<String, Object> entry = iterator.next();
	    this.variablesNames.add(entry.getKey());
	}
    }

    @PostConstruct
    private void init() {
	// Load resources
	for (String resourcesFile : this.resourcesFiles) {
	    this.variablesNames.add(this.replaceSlashesWithUnderscores(this.stripExtentionFile(resourcesFile)));
	    try {
		Object resourcesFilePath = this.resourceLoader
			.getResourceAbsolutePathString("classpath:" + resourcesFile);
		this.mappedVariables.put(this.replaceSlashesWithUnderscores(this.stripExtentionFile(resourcesFile)),
			resourcesFilePath);
	    } catch (Exception e) {
		logger.error(
			"Error while loading template's resource file '" + resourcesFile + "' : " + e.getMessage());
		throw new PDFTemplateContextException();
	    }
	}
    }

    private String stripExtentionFile(String str) {
	return str.replaceFirst("\\.\\w+$", "");
    }

    private String replaceSlashesWithUnderscores(String str) {
	return str.replaceAll("\\/", "_");
    }

    @Override
    public Set<String> getVariableNames() {
	return this.variablesNames;
    }

    @Override
    public Object getVariable(String name) {
	return this.mappedVariables.get(name);
    }

    @Override
    public Locale getLocale() {
	return this.locale;
    }

    @Override
    public boolean containsVariable(String name) {
	return variablesNames.contains(name);
    }

}
