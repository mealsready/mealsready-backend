package fr.cnam.mealsready.pdf.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.cnam.mealsready.core.domain.Day;
import fr.cnam.mealsready.core.domain.Ingredient;
import fr.cnam.mealsready.core.domain.Recipe;
import fr.cnam.mealsready.core.services.ResourceLoaderService;
import fr.cnam.mealsready.pdf.BeansFactory;
import fr.cnam.mealsready.pdf.controllers.DTO.ShoppingListAndDaysListDTO;
import fr.cnam.mealsready.pdf.services.PDFGenerator;
import fr.cnam.mealsready.pdf.services.PDFTemplates;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Api(tags = { "/pdf" }, description = "API PDF generation and retrieval")
@CrossOrigin
@RestController
@RequestMapping("/")
public class PDFGeneratorResources {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    PDFGenerator pdfService;

    @Autowired
    ResourceLoaderService resourceLoader;

    @Autowired
    BeansFactory beansFactory;

    @PostMapping("/generate-recipes-shoppinglist")
    @ApiOperation(value = "Generate a PDF containing the shopping list and the recipes list.", produces = "text/plain")
    public ResponseEntity<String> generateRecipesAndShoppingListPDF(
	    @ApiParam(value = "A JSON containing an ingredient list and a day list", required = true) @RequestBody ShoppingListAndDaysListDTO shoppingListAndDaysListDTO) {
	ResponseEntity<String> responseEntity = null;
	String generatedPDFFileURL = "";

	try {
	    generatedPDFFileURL = pdfService.generatePDFFromHTML(PDFTemplates.RecipesAndShoppingList,
		    "RecipesAndShoppingList.pdf", beansFactory.makeRecipesShoppinglistContextBuilder(
			    shoppingListAndDaysListDTO.getDaysList(), shoppingListAndDaysListDTO.getShoppingList()));

	    responseEntity = ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.TEXT_PLAIN)
		    .body(generatedPDFFileURL);
	} catch (Exception e) {
	    responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	return responseEntity;
    }

    @PostMapping("/generate-recipe")
    @ApiOperation(value = "Generate a PDF containing the given recipe", produces = "text/plain")
    public ResponseEntity<String> generateRecipePDF(
	    @ApiParam(value = "A JSON containing a recipe", required = true) @RequestBody Recipe recipe) {
	ResponseEntity<String> responseEntity = null;
	String generatedPDFFileURL = "";

	try {
	    generatedPDFFileURL = pdfService.generatePDFFromHTML(PDFTemplates.Recipe, "Recipe.pdf",
		    beansFactory.makeRecipeContextBuilder(recipe));

	    responseEntity = ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.TEXT_PLAIN)
		    .body(generatedPDFFileURL);
	} catch (Exception e) {
	    responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	return responseEntity;
    }

    @PostMapping("/generate-recipes")
    @ApiOperation(value = "Generate a PDF containing the recipes list.", produces = "text/plain")
    public ResponseEntity<String> generateRecipesPDF(
	    @ApiParam(value = "The list of model object 'Day'", required = true) @RequestBody ArrayList<Day> daysList) {
	ResponseEntity<String> responseEntity = null;
	String generatedPDFFileURL = "";

	try {
	    generatedPDFFileURL = pdfService.generatePDFFromHTML(PDFTemplates.RecipesList, "recipesList.pdf",
		    beansFactory.makeRecipesShoppinglistContextBuilder(daysList, new ArrayList<Ingredient>()));

	    responseEntity = ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.TEXT_PLAIN)
		    .body(generatedPDFFileURL);
	} catch (Exception e) {
	    responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	return responseEntity;
    }

    @PostMapping("/generate-shoppinglist")
    @ApiOperation(value = "Generate a PDF containing the shopping list.", produces = "text/plain")
    public ResponseEntity<String> generateShoppingListPDF(
	    @ApiParam(value = "The list of model object 'Ingredient'", required = true) @RequestBody ArrayList<Ingredient> ingredientsList) {
	ResponseEntity<String> responseEntity = null;
	String generatedPDFFileURL = "";

	try {
	    generatedPDFFileURL = pdfService.generatePDFFromHTML(PDFTemplates.ShoppingList, "shoppingList.pdf",
		    beansFactory.makeRecipesShoppinglistContextBuilder(new ArrayList<Day>(), ingredientsList));

	    responseEntity = ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.TEXT_PLAIN)
		    .body(generatedPDFFileURL);
	} catch (Exception e) {
	    responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	return responseEntity;
    }

    @GetMapping("/get/{timeStamp}/{randomNumber}/{fileName}")
    @ApiOperation(value = "Retrieve a PDF file using the relative URL given by the PDF generator.", produces = "application/pdf")
    public ResponseEntity<InputStreamResource> getPDFFile(@PathVariable String timeStamp,
	    @PathVariable String randomNumber, @PathVariable String fileName) {
	ResponseEntity<InputStreamResource> responseEntity = null;
	HttpHeaders headers = new HttpHeaders();
	FileInputStream inputStream = null;

	File pdfFile = null;
	try {
	    pdfFile = new File(resourceLoader
		    .getResourceAbsolutePathString("file:outpdf/" + timeStamp + "-" + randomNumber + "/" + fileName));
	} catch (Exception e) {
	    logger.error(e.getMessage());
	    responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	headers.add("Content-Disposition", "inline; filename=" + pdfFile.getName());

	try {
	    inputStream = new FileInputStream(pdfFile);

	    responseEntity = ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
		    .body(new InputStreamResource(inputStream));
	} catch (FileNotFoundException e) {
	    logger.info("The resource \"" + timeStamp + "-" + randomNumber + "/" + fileName + "\" does not exist");
	    responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	return responseEntity;
    }
}
