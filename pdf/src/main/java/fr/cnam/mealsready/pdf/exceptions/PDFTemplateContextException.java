package fr.cnam.mealsready.pdf.exceptions;

public class PDFTemplateContextException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public PDFTemplateContextException() {
	super("Error while creating PDF template context.");
    }

    public PDFTemplateContextException(String message) {
	super(message);
    }
}
