package fr.cnam.mealsready.pdf.services;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.cnam.mealsready.core.domain.Day;
import fr.cnam.mealsready.core.domain.Ingredient;
import fr.cnam.mealsready.core.domain.Unit;
import fr.cnam.mealsready.core.services.ResourceLoaderService;
import fr.cnam.mealsready.pdf.DTO.IngredientDTO;
import fr.cnam.mealsready.pdf.DTO.ShoppingListDTO;

@Component
public class RecipesShoppinglistContextBuilder extends AbstractPDFContextBuilder {
    @Autowired
    ResourceLoaderService resourceLoader;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String KEY_DAYS_LIST = "daysList";
    private static final String KEY_SHOPPING_LIST = "shoppinglist";
    private static final String KEY_FONTS_DECLARATION = "fontsDeclaration";

    private ArrayList<Day> daysList;
    private ArrayList<Ingredient> ingredientsList;

    public RecipesShoppinglistContextBuilder(ArrayList<Day> daysList, ArrayList<Ingredient> ingredientsList) {
	this.daysList = daysList;
	this.ingredientsList = ingredientsList;
    }

    @Override
    protected BindingContent prepareContent() {
	HashMap<String, Object> objectsToMap = new HashMap<String, Object>();

	objectsToMap.put(KEY_DAYS_LIST, daysList);
	objectsToMap.put(KEY_SHOPPING_LIST, this.buildShoppingListFromIngredientsList(ingredientsList));
	String fontsDeclarationString = "";

	try {
	    String dinproFontFilePath = resourceLoader
		    .getResourceAbsolutePathString("classpath:assets/font/DINPro_Light_tr.woff");
	    String montserratFontFilePath = resourceLoader
		    .getResourceAbsolutePathString("classpath:assets/font/Montserrat_Light.woff");
	    fontsDeclarationString = "@font-face {\r\n" + //
		    "	    font-family: \"Montserrat\";\r\n" + //
		    "	    src: url(\"" + montserratFontFilePath + "\") format(\"woff\");\r\n" + //
		    "	}\r\n" + //
		    "	@font-face {\r\n" + //
		    "	    font-family: \"DINpro\";\r\n" + //
		    "	    src: url(\"" + dinproFontFilePath + "\") format(\"woff\");\r\n" + //
		    "	}"; //
	} catch (Exception e) {
	    logger.warn("Error while building the PDF template context. A font file could not be loaded : "
		    + e.getMessage());
	}

	objectsToMap.put(KEY_FONTS_DECLARATION, fontsDeclarationString);

	return new BindingContent(objectsToMap, //
		"templates/fragments/recipiesShoppinglistInc.html", //
		"assets/css/PDFStyle.css", //
		"assets/img/head.png", //
		"assets/img/duration_icon.png", //
		"assets/img/servings_icon.png");
    }

    /**
     * Concat all similar ingredients
     * 
     * @param ingredientsList
     * @return
     */
    private ShoppingListDTO buildShoppingListFromIngredientsList(ArrayList<Ingredient> ingredientsList) {
	HashMap<String, HashMap<Unit, Double>> ingredientsMap = new HashMap<String, HashMap<Unit, Double>>();

	for (Ingredient ingredient : ingredientsList) {
	    if (ingredientsMap.containsKey(ingredient.getName())) {
		HashMap<Unit, Double> quantityMap = ingredientsMap.get(ingredient.getName());
		if (!quantityMap.containsKey(ingredient.getUnit())) {
		    quantityMap.put(ingredient.getUnit(), 0.0);
		}
		Double quantity = quantityMap.get(ingredient.getUnit());
		quantity += ingredient.getQuantity();
		quantityMap.put(ingredient.getUnit(), quantity);
	    } else {
		HashMap<Unit, Double> map = new HashMap<Unit, Double>();
		map.put(ingredient.getUnit(), ingredient.getQuantity());
		ingredientsMap.put(ingredient.getName(), map);
	    }
	}

	ArrayList<IngredientDTO> ingredientsDTOList = new ArrayList<IngredientDTO>();

	Iterator<Entry<String, HashMap<Unit, Double>>> it = ingredientsMap.entrySet().iterator();
	while (it.hasNext()) {
	    Entry<String, HashMap<Unit, Double>> entry = it.next();
	    String ingredientName = entry.getKey();
	    HashMap<Unit, Double> quantityMap = entry.getValue();

	    StringBuilder strBuilder = new StringBuilder();

	    DecimalFormat df = new DecimalFormat("#.#");

	    String lastValue = "";
	    Iterator<Entry<Unit, Double>> quantitiesIterator = quantityMap.entrySet().iterator();
	    while (quantitiesIterator.hasNext()) {
		Entry<Unit, Double> qtyEntry = quantitiesIterator.next();
		if (qtyEntry.getKey() != null) {
		    String unit = qtyEntry.getKey().getText();
		    Double value = qtyEntry.getValue();

		    if (quantityMap.size() > 1 && unit.length() == 0) {
			lastValue = " + " + df.format(value);
		    } else {
			strBuilder.append(df.format(value) + unit);

			if (quantitiesIterator.hasNext()) {
			    strBuilder.append(" + ");
			}
		    }
		}
	    }
	    strBuilder.append(lastValue + " ");

	    IngredientDTO ingredientDTO = new IngredientDTO();
	    ingredientDTO.setName(ingredientName);
	    ingredientDTO.setQuantities(strBuilder.toString());

	    ingredientsDTOList.add(ingredientDTO);
	}

	ShoppingListDTO shoppingListDTO = new ShoppingListDTO();
	shoppingListDTO.setIngredients(ingredientsDTOList);

	return shoppingListDTO;
    }
}
