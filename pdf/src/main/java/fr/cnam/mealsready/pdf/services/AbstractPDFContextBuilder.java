package fr.cnam.mealsready.pdf.services;

import java.util.Map;

import org.springframework.beans.factory.BeanFactory;

/**
 * This abstract class is a helper for PDF template context builder
 * implementation classes. If you want to implement a context builder for the
 * PDF generator you have to extend from this class and implement the
 * prepareContent method.
 * 
 * @author christopher MILAZZO
 * @since V1.0
 *
 */
public abstract class AbstractPDFContextBuilder {
    public final PDFTemplateContext buildContext(BeanFactory beanFactory) {
	BindingContent content = prepareContent();
	PDFTemplateContext pdfTemplateContext = beanFactory.getBean(PDFTemplateContext.class, content.getObjectsToMap(),
		content.getResourcesFiles());
	return pdfTemplateContext;
    }

    /**
     * This method should be overridden in the child class. It should return a
     * valued BindingContent instance.
     * 
     * @return a BindingContent instance, representing the context for the PDF
     *         document
     * 
     * @see BindingContent
     */
    abstract protected BindingContent prepareContent();

    /**
     * This internal class is used to contain the binding data.
     * 
     * @see BindingContent#BindingContent(Map, String[])
     *      "the constructor for details"
     * 
     * @author christopher MILAZZO
     * @since V1.0
     */
    public static class BindingContent {
	private Map<String, Object> objectsToMap;
	private String[] resourcesFiles;

	/**
	 * 
	 * @param objectsToMap   This map contains all the variables to be linked in the
	 *                       PDF model, the keys being the name of the variable
	 *                       associated with its value. <br>
	 *                       <br>
	 * @param resourcesFiles This array is optional and must contain the relative
	 *                       path for the file to be included in the template. The
	 *                       path is relative to the project's <b>resources</b>
	 *                       folder. The name of the variable to be included in the
	 *                       template should be the same as the path with
	 *                       <b>underscore</b> instead of <b>slashes</b> and no file
	 *                       extension. <br>
	 *                       <b>EXAMPLE : </b><br>
	 *                       <br>
	 *                       This example is for a template using <b>Thymeleaf</b>
	 *                       template engine. You want to include the CSS file
	 *                       located at "src/main/resources/assets/css/myStyle.css"
	 *                       in the template, you should provide the relative path :
	 * 
	 *                       <pre>
	 *                       assets / css / myStyle.css
	 *                       </pre>
	 * 
	 *                       <br>
	 *                       and use the following tag in the template file (see the
	 *                       <b>th:href</b> attribut) :
	 * 
	 *                       <pre>
	 *                       &lt;link rel="stylesheet" type="text/css"
					th:href="${assets_css_myStyle}"&gt;
	 *                       </pre>
	 * 
	 *                       <b>NOTE : </b><br>
	 *                       No preprocessing will be done. So Sass files or
	 *                       TypeScript files won't work.
	 * 
	 * @author Christopher MILAZZO
	 * @since V1.0
	 */
	public BindingContent(Map<String, Object> objectsToMap, String... resourcesFiles) {
	    this.objectsToMap = objectsToMap;
	    this.resourcesFiles = resourcesFiles;
	}

	public Map<String, Object> getObjectsToMap() {
	    return objectsToMap;
	}

	public String[] getResourcesFiles() {
	    return resourcesFiles;
	}
    }
}
