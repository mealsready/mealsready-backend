package fr.cnam.mealsready.pdf.DTO;

import java.util.ArrayList;

public class ShoppingListDTO {
    private ArrayList<IngredientDTO> ingredients;

    public ArrayList<IngredientDTO> getIngredients() {
	return ingredients;
    }

    public void setIngredients(ArrayList<IngredientDTO> ingredients) {
	this.ingredients = ingredients;
    }
}
