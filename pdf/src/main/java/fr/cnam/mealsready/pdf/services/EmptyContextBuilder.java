package fr.cnam.mealsready.pdf.services;

import java.util.HashMap;

/**
 * Build an empty context for the PDF service
 * 
 * @author christopher MILAZZO
 * @since V1.0
 *
 */
public final class EmptyContextBuilder extends AbstractPDFContextBuilder {
    @Override
    protected BindingContent prepareContent() {
	return new BindingContent(new HashMap<String, Object>());
    }
}
