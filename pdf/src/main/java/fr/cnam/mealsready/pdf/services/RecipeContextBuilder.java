package fr.cnam.mealsready.pdf.services;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.cnam.mealsready.core.domain.Recipe;
import fr.cnam.mealsready.core.services.ResourceLoaderService;

@Component
public class RecipeContextBuilder extends AbstractPDFContextBuilder {
    @Autowired
    ResourceLoaderService resourceLoader;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String KEY_RECIPE = "recipe";
    private static final String KEY_FONTS_DECLARATION = "fontsDeclaration";

    private Recipe recipe;

    public RecipeContextBuilder() {
	this.recipe = null;
    }

    public RecipeContextBuilder(Recipe recipe) {
	this.recipe = recipe;
    }

    @Override
    protected BindingContent prepareContent() {
	HashMap<String, Object> objectsToMap = new HashMap<String, Object>();

	objectsToMap.put(KEY_RECIPE, recipe);
	String fontsDeclarationString = "";

	try {
	    String dinproFontFilePath = resourceLoader
		    .getResourceAbsolutePathString("classpath:assets/font/DINPro_Light_tr.woff");
	    String montserratFontFilePath = resourceLoader
		    .getResourceAbsolutePathString("classpath:assets/font/Montserrat_Light.woff");
	    fontsDeclarationString = "@font-face {\r\n" + //
		    "	    font-family: \"Montserrat\";\r\n" + //
		    "	    src: url(\"" + montserratFontFilePath + "\") format(\"woff\");\r\n" + //
		    "	}\r\n" + //
		    "	@font-face {\r\n" + //
		    "	    font-family: \"DINpro\";\r\n" + //
		    "	    src: url(\"" + dinproFontFilePath + "\") format(\"woff\");\r\n" + //
		    "	}"; //
	} catch (Exception e) {
	    logger.warn("Error while building the PDF template context. A font file could not be loaded : "
		    + e.getMessage());
	}

	objectsToMap.put(KEY_FONTS_DECLARATION, fontsDeclarationString);

	return new BindingContent(objectsToMap, //
		"templates/fragments/recipiesShoppinglistInc.html", //
		"assets/css/PDFStyle.css", //
		"assets/img/head.png", //
		"assets/img/duration_icon.png", //
		"assets/img/servings_icon.png");
    }
}
