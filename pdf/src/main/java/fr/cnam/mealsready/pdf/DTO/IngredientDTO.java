package fr.cnam.mealsready.pdf.DTO;

public class IngredientDTO {
    private String name;
    private String quantities;

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getQuantities() {
	return quantities;
    }

    public void setQuantities(String quantities) {
	this.quantities = quantities;
    }
}
