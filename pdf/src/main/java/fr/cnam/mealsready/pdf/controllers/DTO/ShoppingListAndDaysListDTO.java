package fr.cnam.mealsready.pdf.controllers.DTO;

import java.util.ArrayList;

import fr.cnam.mealsready.core.domain.Day;
import fr.cnam.mealsready.core.domain.Ingredient;

public class ShoppingListAndDaysListDTO implements ControllerDTO {
    private ArrayList<Ingredient> shoppingList;
    private ArrayList<Day> daysList;

    public ArrayList<Ingredient> getShoppingList() {
	return shoppingList;
    }

    public void setShoppingList(ArrayList<Ingredient> shoppingList) {
	this.shoppingList = shoppingList;
    }

    public ArrayList<Day> getDaysList() {
	return daysList;
    }

    public void setDaysList(ArrayList<Day> daysList) {
	this.daysList = daysList;
    }
}
