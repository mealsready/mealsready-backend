package fr.cnam.mealsready.pdf.services;

/**
 * Contain the template file name and the associated context builder class
 * 
 * @author christopher MILAZZO
 *
 */
public enum PDFTemplates {
    RecipesAndShoppingList("recipesShoppingListPDFTemplate.html"), //
    RecipesList("recipesListPDFTemplate.html"), //
    ShoppingList("shoppingListPDFTemplate.html"), //
    Recipe("recipePDFTemplate.html");

    private String templateName;

    PDFTemplates(String templateName) {
	this.templateName = templateName;
    }

    public String getTemplateName() {
	return templateName;
    }

    public void setTemplateName(String templateName) {
	this.templateName = templateName;
    }
}
