package fr.cnam.mealsready.pdf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Paths;
import java.util.ArrayList;

import com.google.gson.Gson;

import fr.cnam.mealsready.core.domain.Day;
import fr.cnam.mealsready.core.domain.Ingredient;
import fr.cnam.mealsready.core.domain.Recipe;

public class MockFactory {
    private static Gson gson = new Gson();

    public static ArrayList<Day> generateDaysListWithRecipies() throws Exception {
	MockFactory.DaysList daysList = null;
	File file = new File(Paths.get("src", "test", "java", "fr", "cnam", "mealsready", "pdf",
		"recipies_mock.json").toString());

	try {
	    BufferedReader buff = new BufferedReader(new FileReader(file));
	    StringWriter stringWriter = new StringWriter();
	    buff.transferTo(stringWriter);
	    daysList = gson.fromJson(stringWriter.toString(), MockFactory.DaysList.class);
	    buff.close();
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new Exception();
	}

	return daysList.days;
    }

    public static ArrayList<Ingredient> generateIngredientsList() throws Exception {
	MockFactory.IngredientsList ingredientsList = null;
	File file = new File(Paths.get("src", "test", "java", "fr", "cnam", "mealsready", "pdf",
		"shoppinglist_mock.json").toString());

	try {
	    BufferedReader buff = new BufferedReader(new FileReader(file));
	    StringWriter stringWriter = new StringWriter();
	    buff.transferTo(stringWriter);
	    ingredientsList = gson.fromJson(stringWriter.toString(), MockFactory.IngredientsList.class);
	    buff.close();
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new Exception();
	}

	return ingredientsList.ingredients;
    }

    public static Recipe generateOneRecipe() throws Exception {
	ArrayList<Day> days = MockFactory.generateDaysListWithRecipies();
	return days.get(0).getMenusList().get(0).getMainCourse();
    }

    private static class DaysList {
	public ArrayList<Day> days;
    }

    private static class IngredientsList {
	public ArrayList<Ingredient> ingredients;
    }
}
