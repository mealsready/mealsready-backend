package fr.cnam.mealsready.pdf;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import fr.cnam.mealsready.core.domain.Day;
import fr.cnam.mealsready.core.services.ResourceLoaderService;
import fr.cnam.mealsready.pdf.services.PDFGenerator;
import fr.cnam.mealsready.pdf.services.PDFTemplates;
import fr.cnam.mealsready.pdf.services.RecipeContextBuilder;
import fr.cnam.mealsready.pdf.services.RecipesShoppinglistContextBuilder;

@SpringBootTest
public class PDFServiceTest {
	@Autowired
	PDFGenerator pdfService;

	@Autowired
	ResourceLoaderService resourceLoader;

	@Autowired
	BeansFactory beansFactory;

	ArrayList<Day> daysList;
	RecipesShoppinglistContextBuilder contextBuilder;
	RecipeContextBuilder contextBuilderForOneRecipe;
	private static final String OUTPUT_FILE_NAME = "test_pdf_generator.pdf";

	@BeforeEach
	private void setUp() throws Exception {
		this.daysList = MockFactory.generateDaysListWithRecipies();
		this.contextBuilder = beansFactory.makeRecipesShoppinglistContextBuilder(
				MockFactory.generateDaysListWithRecipies(), MockFactory.generateIngredientsList());
		this.contextBuilderForOneRecipe = beansFactory.makeRecipeContextBuilder(MockFactory.generateOneRecipe());
	}

	@Test
	void recipesAndShoppingListGeneration() throws Exception {
		this.pdfService.generatePDFFromHTML(PDFTemplates.RecipesAndShoppingList, OUTPUT_FILE_NAME, this.contextBuilder);
	}

	@Test
	void recipesListGeneration() throws Exception {
		this.pdfService.generatePDFFromHTML(PDFTemplates.RecipesList, OUTPUT_FILE_NAME, this.contextBuilder);
	}

	@Test
	void shoppingListGeneration() throws Exception {
		this.pdfService.generatePDFFromHTML(PDFTemplates.ShoppingList, OUTPUT_FILE_NAME, this.contextBuilder);
	}

	@Test
	void recipeGeneration() throws Exception {
		this.pdfService.generatePDFFromHTML(PDFTemplates.Recipe, OUTPUT_FILE_NAME, this.contextBuilderForOneRecipe);
	}
}
