package gateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder.Builder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import fr.cnam.mealsready.core.domain.ServicesEnum;

@SpringBootApplication
@EnableEurekaClient
public class ApiGatewayApplication {
    public static void main(String[] args) {
	SpringApplication.run(ApiGatewayApplication.class, args);
    }
}

@Configuration
class GatewayRouter {
    
    @Autowired
    private JWTFilter jwtFilter;
    
    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
	Builder routeBuilder = builder.routes();
	
	for (ServicesEnum servicePair : ServicesEnum.values()) {
	    String serviceName = servicePair.getServiceName();
	    String serviceRoute = servicePair.getRoute();
	    
	    routeBuilder.route(serviceName, r -> r.path("/" + serviceRoute + "/**")
		    .filters(c -> 
		    	c.filter(jwtFilter)
		    	.rewritePath("/" + serviceRoute + "(?<segment>/?.*)", "$\\{segment}")
		    ).uri("lb://" + serviceName.toUpperCase()));
	}
	return routeBuilder.build();
    }
}
