package gateway;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConf {

  @Bean
  public RestTemplateBuilder restTemplateBuilder() {
    // Need to provide a rest template builder because
    // @RestTemplateAutoConfiguration does not work with webflux
    return new RestTemplateBuilder();
  }

}
