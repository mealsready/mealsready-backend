package gateway;

import javax.annotation.PostConstruct;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import fr.cnam.mealsready.core.services.JWTService;
import reactor.core.publisher.Mono;

@Component
public class JWTFilter implements GatewayFilter {

    private JWTService jwtService;
    
    @PostConstruct
    public void init() {
	this.jwtService = new JWTService();
    }
    
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
	// Internal JWT Authentification
	String token = jwtService.generateJWT();
	
	exchange.getRequest().mutate().header("Authorization_internal", token).build();
	return chain.filter(exchange);
    }

}
