#!/bin/bash

servicesLines=""
global_rematch() { 
    local file=$1 end=false
    while IFS= read -r line; do
        [[ $start == true && $end == false ]] && {
        	part2=`echo -n $line | sed -rn 's/^.+?\(\"(.+)\".*$/\1/p'`
        	part2=`echo -n $part2 | sed -rn 's/^.+?,\s*\"(.+)$/\1/p'`
        	newLine="`echo -n $line | sed -rn 's/^(\w+)\(.*$/\1/p'`|$part2"

            servicesLines="$servicesLines $newLine"
        }
        [[ $line =~ 'public enum' ]] && start=true
        [[ $line =~ ';' && $start == true ]] && end=true
    done < $file
}

cd ./src/main/resources/static/
fileContent="\
<!DOCTYPE html>\
<html lang=\"fr\">\
	<head>\
		<meta charset=\"UTF-8\">\
		<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\
		<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">\
		<title>Meal's ready Gateway</title>\
	</head>\
\
	<body>\
		<header>\
			<img src=\"logo-empty.png\">\
		</header>\
		<section id=\"main\">\
			<h2 id=\"welcome-title\">Declared MealsReady micro-services</h2>\
			<div id=\"button-container\">\
"

global_rematch '../../../../../core/src/main/java/fr/cnam/mealsready/core/domain/ServicesEnum.java'
for line in $servicesLines; do
    serviceName=`echo -n $line | sed -rn 's/^(.+)\|.*$/\1/p'`
    serviceUrl=`echo -n $line | sed -rn 's/^.+\|(.+)$/\1/p'`
    fileContent="$fileContent \
        <button class=\"link\" onClick=\"onLinkClicked('/$serviceUrl/')\">\
		    <span>$serviceName (/$serviceUrl/)</span>\
		</button>\
    "
done

fileContent="$fileContent\
            </div>\
		</section>\
		<hr>\
		<script src=\"./link.js\"></script>\
	</body>\
</html>\
"

echo "$fileContent" > index.html
