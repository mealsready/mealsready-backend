#!/bin/bash

docker-compose --version && {
	docker-compose -f docker-compose_local.yml down
} || {
	echo "Error : docker-compose could not be found"
	exit 1
}
